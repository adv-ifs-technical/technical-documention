# iFactory+ GraphQL API Schema

<!-- markdownlint-disable MD033 -->
<!--- spell-checker:disable -->
<!--markdownlint-disable ol-prefix-->

## Table of Contents

<details>
  <summary><strong>Table of Contents</strong></summary>

- [前言](#前言)
- [GraphQL playground](#graphql-playground)
- [Group/Machine/Parameter](#groupmachineparameter)
- [Enabler](#enabler)
- [Profile Management](#profile-management)
- [Machine Status](#machine-status)
- [Rule Management](#rule-management)
- [Command Center](#command-center)
- [Production KPI](#production-kpi)
- [Data Gateway](#data-gateway)
- [Event/Notification](#eventnotification)
- [Calculator](#calculator)
- [Generic KPI](#generic-kpi)
- [EHS](#ehs)
- [Objects](#objects)
  - [AddAlarmCodeEventPayload](#addalarmcodeeventpayload)
  - [AddAlarmLevelPayload](#addalarmlevelpayload)
  - [AddClientPayload](#addclientpayload)
  - [AddCodeRulePayload](#addcoderulepayload)
  - [AddEhsUsagePayload](#addehsusagepayload)
  - [AddEnergyDeviceKindPayload](#addenergydevicekindpayload)
  - [AddEnergyUsageInterfacePayload](#addenergyusageinterfacepayload)
  - [AddEnergyUsagePayload](#addenergyusagepayload)
  - [AddFormulasPayload](#addformulaspayload)
  - [AddGatewayPayload](#addgatewaypayload)
  - [AddGroupPayload](#addgrouppayload)
  - [AddHighLowEventPayload](#addhighloweventpayload)
  - [AddIAppGroupGroupPayload](#addiappgroupgrouppayload)
  - [AddIAppMenuItemPayload](#addiappmenuitempayload)
  - [AddIAppPayload](#addiapppayload)
  - [AddKpiMetricPayload](#addkpimetricpayload)
  - [AddKpiParameterUsagePayload](#addkpiparameterusagepayload)
  - [AddKpiParameterUsageRelationPayload](#addkpiparameterusagerelationpayload)
  - [AddLicensePayload](#addlicensepayload)
  - [AddMachinePayload](#addmachinepayload)
  - [AddMachineStatusPayload](#addmachinestatuspayload)
  - [AddMachineTypeCategoryPayload](#addmachinetypecategorypayload)
  - [AddMachineTypePayload](#addmachinetypepayload)
  - [AddMarqueeActionPayload](#addmarqueeactionpayload)
  - [AddNotificationActionPayload](#addnotificationactionpayload)
  - [AddOutboundPayload](#addoutboundpayload)
  - [AddParameterInterfacePayload](#addparameterinterfacepayload)
  - [AddParameterMappingCodesPayload](#addparametermappingcodespayload)
  - [AddParameterMappingRulePayload](#addparametermappingrulepayload)
  - [AddParameterPayload](#addparameterpayload)
  - [AddProfileMachinePayload](#addprofilemachinepayload)
  - [AddProfileParameterPayload](#addprofileparameterpayload)
  - [AddRolePayload](#addrolepayload)
  - [AddThresholdRulePayload](#addthresholdrulepayload)
  - [AddTimeOfUsePeriodPayload](#addtimeofuseperiodpayload)
  - [AddTranslationLangPayload](#addtranslationlangpayload)
  - [AddUserPayload](#adduserpayload)
  - [AgentInfo](#agentinfo)
  - [AlarmCodeEvent](#alarmcodeevent)
  - [AlarmCodeEventLog](#alarmcodeeventlog)
  - [AlarmCodeEventLogAnalysisItem](#alarmcodeeventloganalysisitem)
  - [AlarmCodeEventLogCountAndElapsedByAlarmLevelItem](#alarmcodeeventlogcountandelapsedbyalarmlevelitem)
  - [AlarmLevel](#alarmlevel)
  - [ApiInfo](#apiinfo)
  - [AppIFactory](#appifactory)
  - [AppMaxnerva](#appmaxnerva)
  - [ApplyParameterMappingToParametersPayload](#applyparametermappingtoparameterspayload)
  - [AssociateInboundGroupPayload](#associateinboundgrouppayload)
  - [Author](#author)
  - [BadTagValue](#badtagvalue)
  - [BulkSetGenericKpiListPayload](#bulksetgenerickpilistpayload)
  - [CalculateFormulaPayload](#calculateformulapayload)
  - [CalculationPoint](#calculationpoint)
  - [CalculationPointOperand](#calculationpointoperand)
  - [CalculationSetting](#calculationsetting)
  - [CalculatorApiInfo](#calculatorapiinfo)
  - [ChangeMyPasswordPayload](#changemypasswordpayload)
  - [Client](#client)
  - [CodeRule](#coderule)
  - [ConnectionStatus](#connectionstatus)
  - [Coordinate](#coordinate)
  - [CountryCallingCodeInfo](#countrycallingcodeinfo)
  - [CreateFirstAdminPayload](#createfirstadminpayload)
  - [CurrencyInfo](#currencyinfo)
  - [DHAcl](#dhacl)
  - [DHAclEntry](#dhaclentry)
  - [DataHubApiInfo](#datahubapiinfo)
  - [DataHubAppSetting](#datahubappsetting)
  - [DeleteValuesInRangePayload](#deletevaluesinrangepayload)
  - [DeviceStatus](#devicestatus)
  - [DisableAlertsPayload](#disablealertspayload)
  - [DownSampledModbusValue](#downsampledmodbusvalue)
  - [DownSampledOpcUaValue](#downsampledopcuavalue)
  - [DownSampledTagValue](#downsampledtagvalue)
  - [DryRunTransformationPointPayload](#dryruntransformationpointpayload)
  - [EhsApiInfo](#ehsapiinfo)
  - [EhsTranslation](#ehstranslation)
  - [EhsUsage](#ehsusage)
  - [ElectricityRateContract](#electricityratecontract)
  - [ElectricityRateContractedCapacity](#electricityratecontractedcapacity)
  - [ElectricityRateContractedCapacityPayload](#electricityratecontractedcapacitypayload)
  - [ElectricityRatePriceComparison](#electricityratepricecomparison)
  - [ElectricityRatePriceComparisonData](#electricityratepricecomparisondata)
  - [ElectricityRatePriceComparisonPayload](#electricityratepricecomparisonpayload)
  - [EnableAlertsPayload](#enablealertspayload)
  - [EnergyAlertTotalCount](#energyalerttotalcount)
  - [EnergyAlertTotalCountListItem](#energyalerttotalcountlistitem)
  - [EnergyAlertTotalListItem](#energyalerttotallistitem)
  - [EnergyAlertTypeCountListItem](#energyalerttypecountlistitem)
  - [EnergyAlertValue](#energyalertvalue)
  - [EnergyConnectAnalysis](#energyconnectanalysis)
  - [EnergyConnectInformation](#energyconnectinformation)
  - [EnergyConnectMachineValue](#energyconnectmachinevalue)
  - [EnergyConnectStatus](#energyconnectstatus)
  - [EnergyConnectValue](#energyconnectvalue)
  - [EnergyConsumptionValue](#energyconsumptionvalue)
  - [EnergyDemandMachineValue](#energydemandmachinevalue)
  - [EnergyDemandValue](#energydemandvalue)
  - [EnergyDevice](#energydevice)
  - [EnergyDeviceKind](#energydevicekind)
  - [EnergyDisconnectTime](#energydisconnecttime)
  - [EnergyKpi](#energykpi)
  - [EnergyKpiAlert](#energykpialert)
  - [EnergyKpiAlertMachineValue](#energykpialertmachinevalue)
  - [EnergyKpiAlertValue](#energykpialertvalue)
  - [EnergyKpiDataOfUsage](#energykpidataofusage)
  - [EnergyKpiReportByUsage](#energykpireportbyusage)
  - [EnergyKpiSavingRate](#energykpisavingrate)
  - [EnergyKpiSavingRateValue](#energykpisavingratevalue)
  - [EnergyKpiSavingReport](#energykpisavingreport)
  - [EnergyKpiSavingReportByUsage](#energykpisavingreportbyusage)
  - [EnergyLastListByUsage](#energylastlistbyusage)
  - [EnergyLastRate](#energylastrate)
  - [EnergyLastValue](#energylastvalue)
  - [EnergyLastValueWithPeak](#energylastvaluewithpeak)
  - [EnergyMachineTotalListValue](#energymachinetotallistvalue)
  - [EnergyMachineValue](#energymachinevalue)
  - [EnergyPeakValue](#energypeakvalue)
  - [EnergyShareTimelineSum](#energysharetimelinesum)
  - [EnergyShareUsageSum](#energyshareusagesum)
  - [EnergyShareUsageTimeline](#energyshareusagetimeline)
  - [EnergyShareValue](#energysharevalue)
  - [EnergySplit](#energysplit)
  - [EnergySplitOperand](#energysplitoperand)
  - [EnergyTotalListItemByPeak](#energytotallistitembypeak)
  - [EnergyTotalValue](#energytotalvalue)
  - [EnergyUsage](#energyusage)
  - [EnergyUsageInterface](#energyusageinterface)
  - [EnergyUsageInterfaceUsage](#energyusageinterfaceusage)
  - [EnergyValue](#energyvalue)
  - [EventConnection](#eventconnection)
  - [Formula](#formula)
  - [FormulaDetail](#formuladetail)
  - [FormulasList](#formulaslist)
  - [FullOutboundIfpCfgPayload](#fulloutboundifpcfgpayload)
  - [Gateway](#gateway)
  - [GenerateEHSKpiConfigPayload](#generateehskpiconfigpayload)
  - [GenericKpi](#generickpi)
  - [GenericKpiAlert](#generickpialert)
  - [GenericKpiHoliday](#generickpiholiday)
  - [Group](#group)
  - [GroupEnergyUsageInterfaceRelation](#groupenergyusageinterfacerelation)
  - [GroupParameterInterfaceRelation](#groupparameterinterfacerelation)
  - [GroupUser](#groupuser)
  - [HighLowEvent](#highlowevent)
  - [HighLowEventLog](#highloweventlog)
  - [IApp](#iapp)
  - [IAppGroup](#iappgroup)
  - [IAppMenuItem](#iappmenuitem)
  - [IfpProduct](#ifpproduct)
  - [InboundProtocol](#inboundprotocol)
  - [InfluxInfo](#influxinfo)
  - [KpiMetric](#kpimetric)
  - [KpiParameterUsage](#kpiparameterusage)
  - [KpiParameterUsageRelation](#kpiparameterusagerelation)
  - [Language](#language)
  - [License](#license)
  - [LicenseData](#licensedata)
  - [Machine](#machine)
  - [MachineStatus](#machinestatus)
  - [MachineType](#machinetype)
  - [MachineTypeCategory](#machinetypecategory)
  - [MachineTypeField](#machinetypefield)
  - [MarqueeAction](#marqueeaction)
  - [Me](#me)
  - [ModbusTag](#modbustag)
  - [ModbusValue](#modbusvalue)
  - [MoveMachinePayload](#movemachinepayload)
  - [NotificationAction](#notificationaction)
  - [NotificationGroup](#notificationgroup)
  - [OpcUaTag](#opcuatag)
  - [OpcUaValue](#opcuavalue)
  - [OrgAcl](#orgacl)
  - [OrganizerApiInfo](#organizerapiinfo)
  - [Outbound](#outbound)
  - [PageInfo](#pageinfo)
  - [Parameter](#parameter)
  - [ParameterConnection](#parameterconnection)
  - [ParameterEdge](#parameteredge)
  - [ParameterInterface](#parameterinterface)
  - [ParameterMappingCode](#parametermappingcode)
  - [ParameterMappingRule](#parametermappingrule)
  - [PeakValueData](#peakvaluedata)
  - [PeriodTypeInfo](#periodtypeinfo)
  - [PointValue](#pointvalue)
  - [ProductionKpi](#productionkpi)
  - [ProfileMachine](#profilemachine)
  - [ProfileParameter](#profileparameter)
  - [PublicUser](#publicuser)
  - [REAcl](#reacl)
  - [REAclEntry](#reaclentry)
  - [RETranslation](#retranslation)
  - [RegisterInfo](#registerinfo)
  - [RegisterInfoMetadata](#registerinfometadata)
  - [RemoveAlarmCodeEventPayload](#removealarmcodeeventpayload)
  - [RemoveAlarmLevelPayload](#removealarmlevelpayload)
  - [RemoveAlarmLevelTranslationPayload](#removealarmleveltranslationpayload)
  - [RemoveCalculationPointPayload](#removecalculationpointpayload)
  - [RemoveClientPayload](#removeclientpayload)
  - [RemoveCodeRulePayload](#removecoderulepayload)
  - [RemoveCodeRulesByCodeIdsPayload](#removecoderulesbycodeidspayload)
  - [RemoveEhsUsagePayload](#removeehsusagepayload)
  - [RemoveEnergyDeviceKindPayload](#removeenergydevicekindpayload)
  - [RemoveEnergyDeviceKindTranslationPayload](#removeenergydevicekindtranslationpayload)
  - [RemoveEnergyUsageInterfacePayload](#removeenergyusageinterfacepayload)
  - [RemoveEnergyUsagePayload](#removeenergyusagepayload)
  - [RemoveEnergyUsageTranslationPayload](#removeenergyusagetranslationpayload)
  - [RemoveEventByParameterPayload](#removeeventbyparameterpayload)
  - [RemoveFormulaPayload](#removeformulapayload)
  - [RemoveGatewayPayload](#removegatewaypayload)
  - [RemoveGenericKpiListPayload](#removegenerickpilistpayload)
  - [RemoveGroupPayload](#removegrouppayload)
  - [RemoveGroupTranslationPayload](#removegrouptranslationpayload)
  - [RemoveHighLowEventPayload](#removehighloweventpayload)
  - [RemoveHolidaysPayload](#removeholidayspayload)
  - [RemoveIAppGroupPayload](#removeiappgrouppayload)
  - [RemoveIAppGroupTranslationPayload](#removeiappgrouptranslationpayload)
  - [RemoveIAppMenuItemPayload](#removeiappmenuitempayload)
  - [RemoveIAppMenuItemTranslationPayload](#removeiappmenuitemtranslationpayload)
  - [RemoveIAppPayload](#removeiapppayload)
  - [RemoveIAppTranslationPayload](#removeiapptranslationpayload)
  - [RemoveKpiMetricPayload](#removekpimetricpayload)
  - [RemoveKpiMetricTranslationPayload](#removekpimetrictranslationpayload)
  - [RemoveKpiParameterUsagePayload](#removekpiparameterusagepayload)
  - [RemoveKpiParameterUsageRelationPayload](#removekpiparameterusagerelationpayload)
  - [RemoveLicensePayload](#removelicensepayload)
  - [RemoveMachinePayload](#removemachinepayload)
  - [RemoveMachineStatusPayload](#removemachinestatuspayload)
  - [RemoveMachineTranslationPayload](#removemachinetranslationpayload)
  - [RemoveMachineTypeCategoryPayload](#removemachinetypecategorypayload)
  - [RemoveMachineTypePayload](#removemachinetypepayload)
  - [RemoveMarqueeActionPayload](#removemarqueeactionpayload)
  - [RemoveNotificationActionPayload](#removenotificationactionpayload)
  - [RemoveOutboundPayload](#removeoutboundpayload)
  - [RemoveParameterInterfacePayload](#removeparameterinterfacepayload)
  - [RemoveParameterMappingCodePayload](#removeparametermappingcodepayload)
  - [RemoveParameterMappingRulePayload](#removeparametermappingrulepayload)
  - [RemoveParameterPayload](#removeparameterpayload)
  - [RemoveParameterTranslationPayload](#removeparametertranslationpayload)
  - [RemoveRolePayload](#removerolepayload)
  - [RemoveThresholdRulePayload](#removethresholdrulepayload)
  - [RemoveThresholdRuleTranslationPayload](#removethresholdruletranslationpayload)
  - [RemoveTimeOfUsePeriodPayload](#removetimeofuseperiodpayload)
  - [RemoveTransformationPointPayload](#removetransformationpointpayload)
  - [RemoveTranslationLangPayload](#removetranslationlangpayload)
  - [RemoveTranslationsByLangPayload](#removetranslationsbylangpayload)
  - [Role](#role)
  - [RuleEngineApiInfo](#ruleengineapiinfo)
  - [SetAsHolidaysPayload](#setasholidayspayload)
  - [SetCalculationPointPayload](#setcalculationpointpayload)
  - [SetConstantParameterValuesPayload](#setconstantparametervaluespayload)
  - [SetDHAclPayload](#setdhaclpayload)
  - [SetElectricityRateContractPayload](#setelectricityratecontractpayload)
  - [SetEnergyDevicePayload](#setenergydevicepayload)
  - [SetEnergyKpiPayload](#setenergykpipayload)
  - [SetEnergySplitPayload](#setenergysplitpayload)
  - [SetGenericKpiListPayload](#setgenerickpilistpayload)
  - [SetGroupEnergyUsageInterfaceRelationPayload](#setgroupenergyusageinterfacerelationpayload)
  - [SetGroupParameterInterfaceRelationPayload](#setgroupparameterinterfacerelationpayload)
  - [SetGroupUserPayload](#setgroupuserpayload)
  - [SetI18nLangPayload](#seti18nlangpayload)
  - [SetMyTenantPayload](#setmytenantpayload)
  - [SetOrgAclPayload](#setorgaclpayload)
  - [SetProductionKpiPayload](#setproductionkpipayload)
  - [SetREAclPayload](#setreaclpayload)
  - [SetStatusExpirationPeriodPayload](#setstatusexpirationperiodpayload)
  - [SetTenantQuotaPayload](#settenantquotapayload)
  - [SetTransformationPointPayload](#settransformationpointpayload)
  - [SetUserPasswordPayload](#setuserpasswordpayload)
  - [SignInPayload](#signinpayload)
  - [Tag](#tag)
  - [TagValue](#tagvalue)
  - [TenantQuota](#tenantquota)
  - [ThresholdRule](#thresholdrule)
  - [TimeOfUsePeriod](#timeofuseperiod)
  - [TimeZoneInfo](#timezoneinfo)
  - [TopicLevel](#topiclevel)
  - [TransformationDiffRule](#transformationdiffrule)
  - [TransformationPoint](#transformationpoint)
  - [TranslateAlarmLevelPayload](#translatealarmlevelpayload)
  - [TranslateEnergyDeviceKindPayload](#translateenergydevicekindpayload)
  - [TranslateEnergyUsagePayload](#translateenergyusagepayload)
  - [TranslateGroupPayload](#translategrouppayload)
  - [TranslateIAppGroupPayload](#translateiappgrouppayload)
  - [TranslateIAppMenuItemPayload](#translateiappmenuitempayload)
  - [TranslateIAppPayload](#translateiapppayload)
  - [TranslateKpiMetricPayload](#translatekpimetricpayload)
  - [TranslateMachinePayload](#translatemachinepayload)
  - [TranslateMachineStatusPayload](#translatemachinestatuspayload)
  - [TranslateParameterMappingCodePayload](#translateparametermappingcodepayload)
  - [TranslateParameterPayload](#translateparameterpayload)
  - [TranslatePeriodTypeInfoPayload](#translateperiodtypeinfopayload)
  - [TranslateProfileMachinePayload](#translateprofilemachinepayload)
  - [TranslateProfileParameterPayload](#translateprofileparameterpayload)
  - [TranslateThresholdRulePayload](#translatethresholdrulepayload)
  - [Translation](#translation)
  - [TranslationLang](#translationlang)
  - [TurnOffAcquiringPayload](#turnoffacquiringpayload)
  - [TurnOffSaveToHistoryPayload](#turnoffsavetohistorypayload)
  - [TurnOnAcquiringPayload](#turnonacquiringpayload)
  - [UnbindGatewayPayload](#unbindgatewaypayload)
  - [UnbindModbusParameterDataSourcePayload](#unbindmodbusparameterdatasourcepayload)
  - [UnbindOpcUaParameterDataSourceByServerPayload](#unbindopcuaparameterdatasourcebyserverpayload)
  - [UnbindOpcUaParameterDataSourceBySubscriptionTagPayload](#unbindopcuaparameterdatasourcebysubscriptiontagpayload)
  - [UnbindParametersByTagPayload](#unbindparametersbytagpayload)
  - [UnbindParametersMappingPayload](#unbindparametersmappingpayload)
  - [UpdateAlarmCodeEventPayload](#updatealarmcodeeventpayload)
  - [UpdateAlarmLevelPayload](#updatealarmlevelpayload)
  - [UpdateClientPayload](#updateclientpayload)
  - [UpdateCodeRulePayload](#updatecoderulepayload)
  - [UpdateEnergyDeviceKindPayload](#updateenergydevicekindpayload)
  - [UpdateEnergyUsageInterfaceNamePayload](#updateenergyusageinterfacenamepayload)
  - [UpdateEnergyUsageInterfaceUsagesPayload](#updateenergyusageinterfaceusagespayload)
  - [UpdateEnergyUsagePayload](#updateenergyusagepayload)
  - [UpdateGatewayPayload](#updategatewaypayload)
  - [UpdateGroupPayload](#updategrouppayload)
  - [UpdateHighLowEventPayload](#updatehighloweventpayload)
  - [UpdateIAppGroupIndexesPayload](#updateiappgroupindexespayload)
  - [UpdateIAppGroupPayload](#updateiappgrouppayload)
  - [UpdateIAppIndexesPayload](#updateiappindexespayload)
  - [UpdateIAppMenuItemIndexesPayload](#updateiappmenuitemindexespayload)
  - [UpdateIAppMenuItemPayload](#updateiappmenuitempayload)
  - [UpdateIAppPayload](#updateiapppayload)
  - [UpdateKpiMetricPayload](#updatekpimetricpayload)
  - [UpdateKpiParameterUsagePayload](#updatekpiparameterusagepayload)
  - [UpdateMachinePayload](#updatemachinepayload)
  - [UpdateMachineStatusPayload](#updatemachinestatuspayload)
  - [UpdateMachineTypeCategoryPayload](#updatemachinetypecategorypayload)
  - [UpdateMachineTypePayload](#updatemachinetypepayload)
  - [UpdateMarqueeActionPayload](#updatemarqueeactionpayload)
  - [UpdateMePayload](#updatemepayload)
  - [UpdateNotificationActionPayload](#updatenotificationactionpayload)
  - [UpdateOutboundPayload](#updateoutboundpayload)
  - [UpdateParameterDataSourcePayload](#updateparameterdatasourcepayload)
  - [UpdateParameterInterfaceCalculationSettingsPayload](#updateparameterinterfacecalculationsettingspayload)
  - [UpdateParameterInterfaceNamePayload](#updateparameterinterfacenamepayload)
  - [UpdateParameterInterfaceParameterNamesAndKeyPayload](#updateparameterinterfaceparameternamesandkeypayload)
  - [UpdateParameterMappingCodePayload](#updateparametermappingcodepayload)
  - [UpdateParameterMappingRulePayload](#updateparametermappingrulepayload)
  - [UpdateParameterPayload](#updateparameterpayload)
  - [UpdateProfileMachinePayload](#updateprofilemachinepayload)
  - [UpdateProfileParameterPayload](#updateprofileparameterpayload)
  - [UpdateRolePayload](#updaterolepayload)
  - [UpdateThresholdRulePayload](#updatethresholdrulepayload)
  - [UpdateTimeOfUsePeriodPayload](#updatetimeofuseperiodpayload)
  - [UpdateTranslationLangPayload](#updatetranslationlangpayload)
  - [UpdateUserPayload](#updateuserpayload)
  - [User](#user)
- [Inputs](#inputs)
  - [AddAlarmCodeEventInput](#addalarmcodeeventinput)
  - [AddAlarmLevelInput](#addalarmlevelinput)
  - [AddClientInput](#addclientinput)
  - [AddCodeRuleInput](#addcoderuleinput)
  - [AddEhsUsageInput](#addehsusageinput)
  - [AddEnergyDeviceKindInput](#addenergydevicekindinput)
  - [AddEnergyUsageInput](#addenergyusageinput)
  - [AddEnergyUsageInterfaceInput](#addenergyusageinterfaceinput)
  - [AddFormulaEntry](#addformulaentry)
  - [AddFormulasInput](#addformulasinput)
  - [AddGatewayInput](#addgatewayinput)
  - [AddGroupInput](#addgroupinput)
  - [AddHighLowEventInput](#addhighloweventinput)
  - [AddIAppGroupInput](#addiappgroupinput)
  - [AddIAppInput](#addiappinput)
  - [AddIAppMenuItemInput](#addiappmenuiteminput)
  - [AddKpiMetricInput](#addkpimetricinput)
  - [AddKpiParameterUsageInput](#addkpiparameterusageinput)
  - [AddKpiParameterUsageRelationInput](#addkpiparameterusagerelationinput)
  - [AddLicenseInput](#addlicenseinput)
  - [AddMachineInput](#addmachineinput)
  - [AddMachineStatusInput](#addmachinestatusinput)
  - [AddMachineTypeCategoryInput](#addmachinetypecategoryinput)
  - [AddMachineTypeInput](#addmachinetypeinput)
  - [AddMarqueeActionInput](#addmarqueeactioninput)
  - [AddNotificationActionInput](#addnotificationactioninput)
  - [AddOutboundInput](#addoutboundinput)
  - [AddParameterInput](#addparameterinput)
  - [AddParameterInterfaceInput](#addparameterinterfaceinput)
  - [AddParameterMappingCodesEntry](#addparametermappingcodesentry)
  - [AddParameterMappingCodesInput](#addparametermappingcodesinput)
  - [AddParameterMappingRuleInput](#addparametermappingruleinput)
  - [AddProfileMachineInput](#addprofilemachineinput)
  - [AddRoleInput](#addroleinput)
  - [AddThresholdRuleInput](#addthresholdruleinput)
  - [AddTimeOfUsePeriodInput](#addtimeofuseperiodinput)
  - [AddTranslationLangInput](#addtranslationlanginput)
  - [AddUserInput](#adduserinput)
  - [ApplyParameterMappingToParametersInput](#applyparametermappingtoparametersinput)
  - [AssociateInboundGroupInput](#associateinboundgroupinput)
  - [BulkSetGenericKpiEntry](#bulksetgenerickpientry)
  - [BulkSetGenericKpiListInput](#bulksetgenerickpilistinput)
  - [CalculationPointOperandInput](#calculationpointoperandinput)
  - [CalculationSettingInput](#calculationsettinginput)
  - [ChangeMyPasswordInput](#changemypasswordinput)
  - [CodeRuleInput](#coderuleinput)
  - [CoordinateInput](#coordinateinput)
  - [CreateFirstAdminInput](#createfirstadmininput)
  - [DeleteValuesInRangeInput](#deletevaluesinrangeinput)
  - [DisableAlertsInput](#disablealertsinput)
  - [DropConstantParameterValuesInput](#dropconstantparametervaluesinput)
  - [DropPredictionPointValuesInput](#droppredictionpointvaluesinput)
  - [DryRunTransformationPointInput](#dryruntransformationpointinput)
  - [EnableAlertsInput](#enablealertsinput)
  - [EnergySplitOperandEntry](#energysplitoperandentry)
  - [EnergyUsageInterfaceUsageInput](#energyusageinterfaceusageinput)
  - [FullOutboundIfpCfgInput](#fulloutboundifpcfginput)
  - [GenerateEHSKpiConfigData](#generateehskpiconfigdata)
  - [GenerateEHSKpiConfigInput](#generateehskpiconfiginput)
  - [GenericKpiAlertEntry](#generickpialertentry)
  - [GroupCollection](#groupcollection)
  - [IfpCfgCollection](#ifpcfgcollection)
  - [IfpCfgItem](#ifpcfgitem)
  - [InboundIfpCfgInput](#inboundifpcfginput)
  - [MachineCollection](#machinecollection)
  - [MachineTypeInput](#machinetypeinput)
  - [ModbusTagInput](#modbustaginput)
  - [MoveMachineInput](#movemachineinput)
  - [OpcUaTagInput](#opcuataginput)
  - [OutboundIfpCfgInput](#outboundifpcfginput)
  - [ParameterCollection](#parametercollection)
  - [ParameterItem](#parameteritem)
  - [ParameterMappingCodeTranslationEntry](#parametermappingcodetranslationentry)
  - [ParameterOrder](#parameterorder)
  - [PointValueItem](#pointvalueitem)
  - [RemoveAlarmCodeEventInput](#removealarmcodeeventinput)
  - [RemoveAlarmLevelInput](#removealarmlevelinput)
  - [RemoveAlarmLevelTranslationInput](#removealarmleveltranslationinput)
  - [RemoveCalculationPointInput](#removecalculationpointinput)
  - [RemoveClientInput](#removeclientinput)
  - [RemoveCodeRuleInput](#removecoderuleinput)
  - [RemoveCodeRulesByCodeIdsInput](#removecoderulesbycodeidsinput)
  - [RemoveEhsUsageInput](#removeehsusageinput)
  - [RemoveEnergyDeviceKindInput](#removeenergydevicekindinput)
  - [RemoveEnergyDeviceKindTranslationInput](#removeenergydevicekindtranslationinput)
  - [RemoveEnergyUsageInput](#removeenergyusageinput)
  - [RemoveEnergyUsageInterfaceInput](#removeenergyusageinterfaceinput)
  - [RemoveEnergyUsageTranslationInput](#removeenergyusagetranslationinput)
  - [RemoveEventByParameterInput](#removeeventbyparameterinput)
  - [RemoveFormulaInput](#removeformulainput)
  - [RemoveGatewayInput](#removegatewayinput)
  - [RemoveGenericKpiEntry](#removegenerickpientry)
  - [RemoveGenericKpiListInput](#removegenerickpilistinput)
  - [RemoveGroupInput](#removegroupinput)
  - [RemoveGroupTranslationInput](#removegrouptranslationinput)
  - [RemoveHighLowEventInput](#removehighloweventinput)
  - [RemoveHolidaysInput](#removeholidaysinput)
  - [RemoveIAppGroupInput](#removeiappgroupinput)
  - [RemoveIAppGroupTranslationInput](#removeiappgrouptranslationinput)
  - [RemoveIAppInput](#removeiappinput)
  - [RemoveIAppMenuItemInput](#removeiappmenuiteminput)
  - [RemoveIAppMenuItemTranslationInput](#removeiappmenuitemtranslationinput)
  - [RemoveIAppTranslationInput](#removeiapptranslationinput)
  - [RemoveKpiMetricInput](#removekpimetricinput)
  - [RemoveKpiMetricTranslationInput](#removekpimetrictranslationinput)
  - [RemoveKpiParameterUsageInput](#removekpiparameterusageinput)
  - [RemoveKpiParameterUsageRelationInput](#removekpiparameterusagerelationinput)
  - [RemoveLicenseInput](#removelicenseinput)
  - [RemoveMachineInput](#removemachineinput)
  - [RemoveMachineStatusInput](#removemachinestatusinput)
  - [RemoveMachineTranslationInput](#removemachinetranslationinput)
  - [RemoveMachineTypeCategoryInput](#removemachinetypecategoryinput)
  - [RemoveMachineTypeInput](#removemachinetypeinput)
  - [RemoveMarqueeActionInput](#removemarqueeactioninput)
  - [RemoveNotificationActionInput](#removenotificationactioninput)
  - [RemoveOutboundInput](#removeoutboundinput)
  - [RemoveParameterInput](#removeparameterinput)
  - [RemoveParameterInterfaceInput](#removeparameterinterfaceinput)
  - [RemoveParameterMappingCodeInput](#removeparametermappingcodeinput)
  - [RemoveParameterMappingRuleInput](#removeparametermappingruleinput)
  - [RemoveParameterTranslationInput](#removeparametertranslationinput)
  - [RemoveRoleInput](#removeroleinput)
  - [RemoveThresholdRuleInput](#removethresholdruleinput)
  - [RemoveThresholdRuleTranslationInput](#removethresholdruletranslationinput)
  - [RemoveTimeOfUsePeriodInput](#removetimeofuseperiodinput)
  - [RemoveTransformationPointInput](#removetransformationpointinput)
  - [RemoveTranslationLangInput](#removetranslationlanginput)
  - [RemoveTranslationsByLangInput](#removetranslationsbylanginput)
  - [SetAsHolidaysInput](#setasholidaysinput)
  - [SetCalculationPointInput](#setcalculationpointinput)
  - [SetConstantParameterValuesInput](#setconstantparametervaluesinput)
  - [SetDHAclInput](#setdhaclinput)
  - [SetElectricityRateContractInput](#setelectricityratecontractinput)
  - [SetEnergyDeviceInput](#setenergydeviceinput)
  - [SetEnergyKpiInput](#setenergykpiinput)
  - [SetEnergySplitInput](#setenergysplitinput)
  - [SetGenericKpiEntry](#setgenerickpientry)
  - [SetGenericKpiEntryByHour](#setgenerickpientrybyhour)
  - [SetGenericKpiListInput](#setgenerickpilistinput)
  - [SetGroupEnergyUsageInterfaceRelationInput](#setgroupenergyusageinterfacerelationinput)
  - [SetGroupParameterInterfaceRelationInput](#setgroupparameterinterfacerelationinput)
  - [SetGroupUserInput](#setgroupuserinput)
  - [SetI18nLangInput](#seti18nlanginput)
  - [SetMyTenantInput](#setmytenantinput)
  - [SetOrgAclInput](#setorgaclinput)
  - [SetProductionKpiInput](#setproductionkpiinput)
  - [SetREAclInput](#setreaclinput)
  - [SetStatusExpirationPeriodInput](#setstatusexpirationperiodinput)
  - [SetTenantQuotaLimitInput](#settenantquotalimitinput)
  - [SetTenantQuotaQuotaInput](#settenantquotaquotainput)
  - [SetTransformationPointInput](#settransformationpointinput)
  - [SetUserPasswordInput](#setuserpasswordinput)
  - [SignInInput](#signininput)
  - [SourceTag](#sourcetag)
  - [ThresholdRuleInput](#thresholdruleinput)
  - [TransformationDiffRuleInput](#transformationdiffruleinput)
  - [TranslateAlarmLevelInput](#translatealarmlevelinput)
  - [TranslateEnergyDeviceKindInput](#translateenergydevicekindinput)
  - [TranslateEnergyUsageInput](#translateenergyusageinput)
  - [TranslateGroupInput](#translategroupinput)
  - [TranslateIAppGroupInput](#translateiappgroupinput)
  - [TranslateIAppInput](#translateiappinput)
  - [TranslateIAppMenuItemInput](#translateiappmenuiteminput)
  - [TranslateKpiMetricInput](#translatekpimetricinput)
  - [TranslateMachineInput](#translatemachineinput)
  - [TranslateMachineStatusInput](#translatemachinestatusinput)
  - [TranslateParameterInput](#translateparameterinput)
  - [TranslateParameterMappingCodeInput](#translateparametermappingcodeinput)
  - [TranslatePeriodTypeInfoInput](#translateperiodtypeinfoinput)
  - [TranslateThresholdRuleInput](#translatethresholdruleinput)
  - [TurnOffAcquiringInput](#turnoffacquiringinput)
  - [TurnOffSaveToHistoryInput](#turnoffsavetohistoryinput)
  - [TurnOnAcquiringInput](#turnonacquiringinput)
  - [UnbindGatewayInput](#unbindgatewayinput)
  - [UnbindModbusParameterDataSourceInput](#unbindmodbusparameterdatasourceinput)
  - [UnbindOpcUaParameterDataSourceByServerInput](#unbindopcuaparameterdatasourcebyserverinput)
  - [UnbindOpcUaParameterDataSourceBySubscriptionTagInput](#unbindopcuaparameterdatasourcebysubscriptiontaginput)
  - [UnbindParametersByTagInput](#unbindparametersbytaginput)
  - [UnbindParametersMappingInput](#unbindparametersmappinginput)
  - [UpdateAlarmCodeEventInput](#updatealarmcodeeventinput)
  - [UpdateAlarmLevelInput](#updatealarmlevelinput)
  - [UpdateClientInput](#updateclientinput)
  - [UpdateCodeRuleInput](#updatecoderuleinput)
  - [UpdateDHRetentionPolicyInput](#updatedhretentionpolicyinput)
  - [UpdateEnergyDeviceKindInput](#updateenergydevicekindinput)
  - [UpdateEnergyUsageInput](#updateenergyusageinput)
  - [UpdateEnergyUsageInterfaceNameInput](#updateenergyusageinterfacenameinput)
  - [UpdateEnergyUsageInterfaceUsagesInput](#updateenergyusageinterfaceusagesinput)
  - [UpdateGatewayInput](#updategatewayinput)
  - [UpdateGroupInput](#updategroupinput)
  - [UpdateHighLowEventInput](#updatehighloweventinput)
  - [UpdateIAppGroupIndexesInput](#updateiappgroupindexesinput)
  - [UpdateIAppGroupInput](#updateiappgroupinput)
  - [UpdateIAppIndexesInput](#updateiappindexesinput)
  - [UpdateIAppInput](#updateiappinput)
  - [UpdateIAppMenuItemIndexesInput](#updateiappmenuitemindexesinput)
  - [UpdateIAppMenuItemInput](#updateiappmenuiteminput)
  - [UpdateKpiMetricInput](#updatekpimetricinput)
  - [UpdateKpiParameterUsageInput](#updatekpiparameterusageinput)
  - [UpdateMachineInput](#updatemachineinput)
  - [UpdateMachineStatusInput](#updatemachinestatusinput)
  - [UpdateMachineTypeCategoryInput](#updatemachinetypecategoryinput)
  - [UpdateMachineTypeInput](#updatemachinetypeinput)
  - [UpdateMarqueeActionInput](#updatemarqueeactioninput)
  - [UpdateMeInput](#updatemeinput)
  - [UpdateNotificationActionInput](#updatenotificationactioninput)
  - [UpdateOutboundInput](#updateoutboundinput)
  - [UpdateParameterDataSourceInput](#updateparameterdatasourceinput)
  - [UpdateParameterInput](#updateparameterinput)
  - [UpdateParameterInterfaceCalculationSettingsInput](#updateparameterinterfacecalculationsettingsinput)
  - [UpdateParameterInterfaceNameInput](#updateparameterinterfacenameinput)
  - [UpdateParameterInterfaceParameterNamesAndKeyInput](#updateparameterinterfaceparameternamesandkeyinput)
  - [UpdateParameterMappingCodeInput](#updateparametermappingcodeinput)
  - [UpdateParameterMappingRuleInput](#updateparametermappingruleinput)
  - [UpdateProfileMachineInput](#updateprofilemachineinput)
  - [UpdateRoleInput](#updateroleinput)
  - [UpdateThresholdRuleInput](#updatethresholdruleinput)
  - [UpdateTimeOfUsePeriodInput](#updatetimeofuseperiodinput)
  - [UpdateTranslationLangInput](#updatetranslationlanginput)
  - [UpdateUserInput](#updateuserinput)
  - [WriteParameterValueInput](#writeparametervalueinput)
  - [WriteTagValueInput](#writetagvalueinput)
- [Enums](#enums)
  - [AclMode](#aclmode)
  - [CalculationFn](#calculationfn)
  - [CalculationSettingFunction](#calculationsettingfunction)
  - [ConnectStatus](#connectstatus)
  - [DHAclScope](#dhaclscope)
  - [DHRetentionPolicyKind](#dhretentionpolicykind)
  - [DayOfWeek](#dayofweek)
  - [DayOrMonth](#dayormonth)
  - [DefaultIAppKey](#defaultiappkey)
  - [DeviceStatusEvent](#devicestatusevent)
  - [DiffNonNegativeCorrection](#diffnonnegativecorrection)
  - [EhsUsageType](#ehsusagetype)
  - [ElectricityRateClassification](#electricityrateclassification)
  - [ElectricityRateContractedCapacityStatus](#electricityratecontractedcapacitystatus)
  - [ElectricityRateKind](#electricityratekind)
  - [EnergyUsageType](#energyusagetype)
  - [EventKind](#eventkind)
  - [FillOption](#filloption)
  - [GatewayKind](#gatewaykind)
  - [GenerateEHSKpiConfigDataFunction](#generateehskpiconfigdatafunction)
  - [GenerateEHSKpiConfigDataSource](#generateehskpiconfigdatasource)
  - [GenerateEHSKpiConfigKind](#generateehskpiconfigkind)
  - [GenericKpiAlertKind](#generickpialertkind)
  - [GenericKpiBind](#generickpibind)
  - [GenericKpiKind](#generickpikind)
  - [IAppDisplay](#iappdisplay)
  - [InboundAssociation](#inboundassociation)
  - [KpiParameterUsageIcon](#kpiparameterusageicon)
  - [MachineCriticality](#machinecriticality)
  - [OperandKind](#operandkind)
  - [Operator](#operator)
  - [OrderDirection](#orderdirection)
  - [OrgAclScope](#orgaclscope)
  - [OutboundTransport](#outboundtransport)
  - [ParameterKind](#parameterkind)
  - [ParameterOrderField](#parameterorderfield)
  - [PeriodType](#periodtype)
  - [PointSchedule](#pointschedule)
  - [ProductionKpiKind](#productionkpikind)
  - [REAclScope](#reaclscope)
  - [SamplingMethod](#samplingmethod)
  - [TagValueType](#tagvaluetype)
  - [ThresholdType](#thresholdtype)
  - [TimeInterval](#timeinterval)
  - [TimeShift](#timeshift)
  - [TimeType](#timetype)
  - [TransformationPointFn](#transformationpointfn)
  - [URModbusDataType](#urmodbusdatatype)
- [Scalars](#scalars)
  - [Boolean](#boolean)
  - [Currency](#currency)
  - [Date](#date)
  - [DateTime](#datetime)
  - [EmailAddress](#emailaddress)
  - [Float](#float)
  - [ID](#id)
  - [Int](#int)
  - [JSON](#json)
  - [JSONObject](#jsonobject)
  - [NonNegativeInt](#nonnegativeint)
  - [ObjectID](#objectid)
  - [PositiveFloat](#positivefloat)
  - [PositiveInt](#positiveint)
  - [String](#string)
  - [URL](#url)
  - [UUID](#uuid)
  - [Void](#void)
- [Interfaces](#interfaces)
  - [Action](#action)
  - [CommonValue](#commonvalue)
  - [DocumentNode](#documentnode)
  - [DownSampledCommonValue](#downsampledcommonvalue)
  - [Event](#event)
  - [Node](#node)
  - [Rule](#rule)

</details>

## 前言

閱讀本文件時，請一併參考 Graphql playground。

## GraphQL playground

### 啟用 playground

#### Cloud

預設便會開啟，不須特別設定。

#### On-Premises

1. 使用瀏覽器打開 portainer(9000 port)，輸入帳號密碼後按下「Login」。

![playground-1](photos/playground-1.png)

2. 點擊畫面中間的「primary」。

![playground-2](photos/playground-2.png)

3. 點選左側選單列的「Stacks」。

![playground-3](photos/playground-3.png)

4. 點選畫面中間的「ifp」。

![playground-4](photos/playground-4.png)

5. 點選畫面中間的「ifp_organizer」。

![playground-5](photos/playground-5.png)

6. 點選右上方功能列的「Environment variables」。

![playground-6](photos/playground-6.png)

7. 找到「NODE_ENV」，點擊右側的紅色垃圾桶。

![playground-7](photos/playground-7.png)

8. 點擊「Apply changes」。

![playground-8](photos/playground-8.png)

### 開啟 playground

1. 在 Desk UI 的 URL 後面加上「/graphql」，即可開啟 playground。

![playground-9](photos/playground-9.png)

2. 若是無法正確顯示此畫面，可以改用 App 版的 playground，首先，進到該 App 的[Github](https://github.com/graphql/graphql-playground)頁面。

![playground-10](photos/playground-10.png)

3. 點擊右側的「Releases」。

![playground-11](photos/playground-11.png)

4. 下載對應的 OS 的安裝檔，並進行安裝。

![playground-12](photos/playground-12.png)

5. 安裝完成後，開啟程式便會進到 App 版的 playground 的首頁。

![playground-13](photos/playground-13.png)

6. 點擊右側的「URL ENDPOINT」，並輸入 GraphQL API 的 URL(同 1.的 URL)，再按下「OPEN」。

![playground-14](photos/playground-14.png)

7. 看到和 1.相同的畫面，代表已順利設定完成。

![playground-15](photos/playground-15.png)

### 使用 playground

1. 點擊右上角的齒輪，會開啟「Settings」的分頁，找到「request.credentials」，將值改為「include」，再按下右上方的「SAVE SETTINGS」。

![playground-16](photos/playground-16.png)

2. 切換回「New Tab」的分頁，點擊右側的「Docs」，可以看到各 API 的說明。

![playground-17](photos/playground-17.png)

3. call API 時，server 會進行身份驗證，為了通過驗證，需要先透過 mutation signIn 進行登入，若能成功登入，則在回應的 header 中，會含有「set-cookie」，以將相關的 token 放入 cookie 之中。

- Cloud
  - 使用和 Management Portal 相同的帳密進行登入
  - 會將「IFPToken」以及「EIToken」放入 cookie 之中
- On-Premises
  - 預設的帳號為「ifs@advantech.com」，密碼為「password」
  - 會將「IFPToken」放入 cookie 之中

![playground-18](photos/playground-18.png)

4. 由於我們已經進行了 1.的設定，因此在後續的請求之中，會自動帶入 3.被放入 cookie 的 token，在 call API 時，便能通過 server 的身分驗證。

![playground-19](photos/playground-19.png)

## Group/Machine/Parameter

RTM 組織階層 Group, Machine, Parameter 設定

### Group/Machine/Parameter Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groups</strong></td>
<td valign="top">[<a href="#group">Group</a>!]!</td>
<td>

A list of groups.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isTenant</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, list only Tenant Group; If false, list only non-Tenant Group; If null, list both.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">rootGroupsOnly</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, restrict to only root groups.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#group">Group</a></td>
<td>

Fetches a group by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupsByIds</strong></td>
<td valign="top">[<a href="#group">Group</a>]!</td>
<td>

A list of groups.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

IDs of some objects.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machines</strong></td>
<td valign="top">[<a href="#machine">Machine</a>!]!</td>
<td>

A list of machines.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isStation</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Only list Station or only list Machine, omit it for Machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#machine">Machine</a></td>
<td>

Fetches a machine by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machinesByIds</strong></td>
<td valign="top">[<a href="#machine">Machine</a>]!</td>
<td>

A list of machines.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

IDs of some objects.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineTypeCategories</strong></td>
<td valign="top">[<a href="#machinetypecategory">MachineTypeCategory</a>!]!</td>
<td>

A list of machine type categories.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineTypeCategory</strong></td>
<td valign="top"><a href="#machinetypecategory">MachineTypeCategory</a>!</td>
<td>

Fetches a machine type category by its key.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">key</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parametersByTag</strong></td>
<td valign="top">[<a href="#parameter">Parameter</a>!]!</td>
<td>

List parameters by tag

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">scadaId</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">tagId</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a></td>
<td>

Fetches a parameter by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parametersByIds</strong></td>
<td valign="top">[<a href="#parameter">Parameter</a>]!</td>
<td>

A list of parameters.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

IDs of some objects.

</td>
</tr>
</tbody>
</table>

### Group/Machine/Parameter Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>addGroup</strong></td>
<td valign="top"><a href="#addgrouppayload">AddGroupPayload</a>!</td>
<td>

Adds a group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addgroupinput">AddGroupInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateGroup</strong></td>
<td valign="top"><a href="#updategrouppayload">UpdateGroupPayload</a>!</td>
<td>

Updates a group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updategroupinput">UpdateGroupInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeGroup</strong></td>
<td valign="top"><a href="#removegrouppayload">RemoveGroupPayload</a>!</td>
<td>

Removes a group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removegroupinput">RemoveGroupInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateGroup</strong></td>
<td valign="top"><a href="#translategrouppayload">TranslateGroupPayload</a>!</td>
<td>

Translate a group with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translategroupinput">TranslateGroupInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeGroupTranslation</strong></td>
<td valign="top"><a href="#removegrouptranslationpayload">RemoveGroupTranslationPayload</a>!</td>
<td>

Remove a group translate with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removegrouptranslationinput">RemoveGroupTranslationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>associateInboundGroup</strong></td>
<td valign="top"><a href="#associateinboundgrouppayload">AssociateInboundGroupPayload</a>!</td>
<td>

Associate a group with specific gateway.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#associateinboundgroupinput">AssociateInboundGroupInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addMachine</strong></td>
<td valign="top"><a href="#addmachinepayload">AddMachinePayload</a>!</td>
<td>

Adds a machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addmachineinput">AddMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateMachine</strong></td>
<td valign="top"><a href="#updatemachinepayload">UpdateMachinePayload</a>!</td>
<td>

Updates a machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatemachineinput">UpdateMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeMachine</strong></td>
<td valign="top"><a href="#removemachinepayload">RemoveMachinePayload</a>!</td>
<td>

Removes a machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removemachineinput">RemoveMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateMachine</strong></td>
<td valign="top"><a href="#translatemachinepayload">TranslateMachinePayload</a>!</td>
<td>

Translate a machine with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translatemachineinput">TranslateMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeMachineTranslation</strong></td>
<td valign="top"><a href="#removemachinetranslationpayload">RemoveMachineTranslationPayload</a>!</td>
<td>

Remove a machine translate with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removemachinetranslationinput">RemoveMachineTranslationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moveMachine</strong></td>
<td valign="top"><a href="#movemachinepayload">MoveMachinePayload</a>!</td>
<td>

Remove a machine translate with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#movemachineinput">MoveMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addMachineTypeCategory</strong></td>
<td valign="top"><a href="#addmachinetypecategorypayload">AddMachineTypeCategoryPayload</a>!</td>
<td>

Add a machine type category.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addmachinetypecategoryinput">AddMachineTypeCategoryInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeMachineTypeCategory</strong></td>
<td valign="top"><a href="#removemachinetypecategorypayload">RemoveMachineTypeCategoryPayload</a>!</td>
<td>

Remove the machine type category.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removemachinetypecategoryinput">RemoveMachineTypeCategoryInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateMachineTypeCategory</strong></td>
<td valign="top"><a href="#updatemachinetypecategorypayload">UpdateMachineTypeCategoryPayload</a>!</td>
<td>

Update the machine type category.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatemachinetypecategoryinput">UpdateMachineTypeCategoryInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addMachineType</strong></td>
<td valign="top"><a href="#addmachinetypepayload">AddMachineTypePayload</a>!</td>
<td>

Add a machine type.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addmachinetypeinput">AddMachineTypeInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeMachineType</strong></td>
<td valign="top"><a href="#removemachinetypepayload">RemoveMachineTypePayload</a>!</td>
<td>

Remove the machine type.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removemachinetypeinput">RemoveMachineTypeInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateMachineType</strong></td>
<td valign="top"><a href="#updatemachinetypepayload">UpdateMachineTypePayload</a>!</td>
<td>

Update the machine type.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatemachinetypeinput">UpdateMachineTypeInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addParameter</strong></td>
<td valign="top"><a href="#addparameterpayload">AddParameterPayload</a>!</td>
<td>

Adds a parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addparameterinput">AddParameterInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateParameter</strong></td>
<td valign="top"><a href="#updateparameterpayload">UpdateParameterPayload</a>!</td>
<td>

Updates a parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateparameterinput">UpdateParameterInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeParameter</strong></td>
<td valign="top"><a href="#removeparameterpayload">RemoveParameterPayload</a>!</td>
<td>

Removes a parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeparameterinput">RemoveParameterInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>unbindParametersByTag</strong></td>
<td valign="top"><a href="#unbindparametersbytagpayload">UnbindParametersByTagPayload</a>!</td>
<td>

Unbinds parameters with same tag

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#unbindparametersbytaginput">UnbindParametersByTagInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateParameter</strong></td>
<td valign="top"><a href="#translateparameterpayload">TranslateParameterPayload</a>!</td>
<td>

Translate a parameter with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translateparameterinput">TranslateParameterInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeParameterTranslation</strong></td>
<td valign="top"><a href="#removeparametertranslationpayload">RemoveParameterTranslationPayload</a>!</td>
<td>

Remove a parameter translate with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeparametertranslationinput">RemoveParameterTranslationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>unbindOpcUaParameterDataSourceByServer</strong></td>
<td valign="top"><a href="#unbindopcuaparameterdatasourcebyserverpayload">UnbindOpcUaParameterDataSourceByServerPayload</a>!</td>
<td>

Unbind an OPC UA parameter data source by server.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#unbindopcuaparameterdatasourcebyserverinput">UnbindOpcUaParameterDataSourceByServerInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>unbindOpcUaParameterDataSourceBySubscriptionTag</strong></td>
<td valign="top"><a href="#unbindopcuaparameterdatasourcebysubscriptiontagpayload">UnbindOpcUaParameterDataSourceBySubscriptionTagPayload</a>!</td>
<td>

Unbind an OPC UA parameter data source by subscription tag.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#unbindopcuaparameterdatasourcebysubscriptiontaginput">UnbindOpcUaParameterDataSourceBySubscriptionTagInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>unbindModbusParameterDataSource</strong></td>
<td valign="top"><a href="#unbindmodbusparameterdatasourcepayload">UnbindModbusParameterDataSourcePayload</a>!</td>
<td>

Unbind a Modbus parameter data source by server.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#unbindmodbusparameterdatasourceinput">UnbindModbusParameterDataSourceInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>writeParameterValue</strong></td>
<td valign="top"><a href="#void">Void</a></td>
<td>

Write a value of the parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#writeparametervalueinput">WriteParameterValueInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Enabler

### Enabler Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>info</strong></td>
<td valign="top"><a href="#apiinfo">ApiInfo</a>!</td>
<td>

Fetches the global API Information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>clients</strong></td>
<td valign="top">[<a href="#client">Client</a>!]!</td>
<td>

List Clients.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>client</strong></td>
<td valign="top"><a href="#client">Client</a></td>
<td>

Fetches a Client by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryCallingCodeInfos</strong></td>
<td valign="top">[<a href="#countrycallingcodeinfo">CountryCallingCodeInfo</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryCallingCodeInfo</strong></td>
<td valign="top"><a href="#countrycallingcodeinfo">CountryCallingCodeInfo</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">code</td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Country calling code, e.g., "886" and "1-684"

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>currencyInfos</strong></td>
<td valign="top">[<a href="#currencyinfo">CurrencyInfo</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>currencyInfo</strong></td>
<td valign="top"><a href="#currencyinfo">CurrencyInfo</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">code</td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Currency code, e.g., "USD" and "TWD"

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>languages</strong></td>
<td valign="top">[<a href="#language">Language</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>language</strong></td>
<td valign="top"><a href="#language">Language</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">code</td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Language code, e.g., "en-US" and "zh-TW"

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>wisePaasLicenseApiUrl</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

WISE-PaaS License API URL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>wisePaasLicenseId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

WISE-PaaS License Id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iFactoryPnList</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Fetched iFactory Part Number list.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>licenses</strong></td>
<td valign="top">[<a href="#license">License</a>!]!</td>
<td>

A list of licenses.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>license</strong></td>
<td valign="top"><a href="#license">License</a></td>
<td>

Fetches a license by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>licenseInfo</strong></td>
<td valign="top"><a href="#licensedata">LicenseData</a></td>
<td>

Shows license information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>licenseAgent</strong></td>
<td valign="top"><a href="#agentinfo">AgentInfo</a>!</td>
<td>

Shows license agent information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterInterfaces</strong></td>
<td valign="top">[<a href="#parameterinterface">ParameterInterface</a>!]!</td>
<td>

A list of parameter interfaces.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterInterface</strong></td>
<td valign="top"><a href="#parameterinterface">ParameterInterface</a>!</td>
<td>

Fetches a parameter interface by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>publicUser</strong></td>
<td valign="top"><a href="#publicuser">PublicUser</a></td>
<td>

Fetches a public user by its username.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">username</td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Username of Public User to fetch.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>serviceInfos</strong></td>
<td valign="top">[<a href="#registerinfo">RegisterInfo</a>!]!</td>
<td>

A list of Service RegisterInfo.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">all</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

List all RegisterInfo or only elected.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">names</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filter RegisterInfo list by names.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iAppInfos</strong></td>
<td valign="top">[<a href="#registerinfo">RegisterInfo</a>!]!</td>
<td>

A list of iApp RegisterInfo.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">all</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

List all RegisterInfo or only elected.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">names</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filter RegisterInfo list by names.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roles</strong></td>
<td valign="top">[<a href="#role">Role</a>!]!</td>
<td>

A list of Roles.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#role">Role</a>!</td>
<td>

Fetches a Role by its key.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">key</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeZoneInfos</strong></td>
<td valign="top">[<a href="#timezoneinfo">TimeZoneInfo</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeZoneInfo</strong></td>
<td valign="top"><a href="#timezoneinfo">TimeZoneInfo</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeZone</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translationLangs</strong></td>
<td valign="top">[<a href="#translationlang">TranslationLang</a>!]!</td>
<td>

A list of translation langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>me</strong></td>
<td valign="top"><a href="#me">Me</a></td>
<td>

當前登入的使用者<br>`Undefined` if not sign in

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>users</strong></td>
<td valign="top">[<a href="#user">User</a>!]!</td>
<td>

List Users.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]</td>
<td>

IDs of Users to list.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Fetches a user by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userByUsername</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Fetch User by username.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">username</td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Username of User to fetch.

</td>
</tr>
</tbody>
</table>

### Enabler Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>auth</strong></td>
<td valign="top"><a href="#authpayload">AuthPayload</a>!</td>
<td>

查看 Cookie 確認是否登入，如果只有登入 EnSaaS 的話，也要經過 SSO 處理並設定 Cookie<br>同時也要向 ifp-tenant 呼叫 query { me { user { id } } }，啟動 ifp-tenant 的 SSO 機制

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>signIn</strong></td>
<td valign="top"><a href="#signinpayload">SignInPayload</a>!</td>
<td>

User sign-in.<br>同時也要向 ifp-tenant 呼叫 query { me { user { id } } }，啟動 ifp-tenant 的 SSO 機制<br>成功登入後，會將 token 放入 cookie 之中。

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#signininput">SignInInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>signOut</strong></td>
<td valign="top"><a href="#void">Void</a></td>
<td>

登出後，會將登入時放入 cookie 裡的 token 也一併清除。

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createFirstAdmin</strong></td>
<td valign="top"><a href="#createfirstadminpayload">CreateFirstAdminPayload</a>!</td>
<td>

Creates the first admin.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#createfirstadmininput">CreateFirstAdminInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>refreshLicense</strong></td>
<td valign="top"><a href="#void">Void</a></td>
<td>

Refresh license.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addClient</strong></td>
<td valign="top"><a href="#addclientpayload">AddClientPayload</a>!</td>
<td>

Adds a Client.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addclientinput">AddClientInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateClient</strong></td>
<td valign="top"><a href="#updateclientpayload">UpdateClientPayload</a>!</td>
<td>

Updates a Client.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateclientinput">UpdateClientInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeClient</strong></td>
<td valign="top"><a href="#removeclientpayload">RemoveClientPayload</a>!</td>
<td>

Removes a Client.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeclientinput">RemoveClientInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removegenerickpilistinput">RemoveGenericKpiListInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setGroupParameterInterfaceRelation</strong></td>
<td valign="top"><a href="#setgroupparameterinterfacerelationpayload">SetGroupParameterInterfaceRelationPayload</a>!</td>
<td>

Sets a group parameter interface relation.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setgroupparameterinterfacerelationinput">SetGroupParameterInterfaceRelationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setGroupUser</strong></td>
<td valign="top"><a href="#setgroupuserpayload">SetGroupUserPayload</a>!</td>
<td>

Set a GroupUser.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setgroupuserinput">SetGroupUserInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setMyTenant</strong></td>
<td valign="top"><a href="#setmytenantpayload">SetMyTenantPayload</a>!</td>
<td>

Set my Tenant Group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setmytenantinput">SetMyTenantInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setI18nLang</strong></td>
<td valign="top"><a href="#seti18nlangpayload">SetI18nLangPayload</a>!</td>
<td>

Set i18n language

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#seti18nlanginput">SetI18nLangInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addLicense</strong></td>
<td valign="top"><a href="#addlicensepayload">AddLicensePayload</a>!</td>
<td>

Adds a license.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addlicenseinput">AddLicenseInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeLicense</strong></td>
<td valign="top"><a href="#removelicensepayload">RemoveLicensePayload</a>!</td>
<td>

Removes a license.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removelicenseinput">RemoveLicenseInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addParameterInterface</strong></td>
<td valign="top"><a href="#addparameterinterfacepayload">AddParameterInterfacePayload</a>!</td>
<td>

Adds a parameter interface.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addparameterinterfaceinput">AddParameterInterfaceInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateParameterInterfaceName</strong></td>
<td valign="top"><a href="#updateparameterinterfacenamepayload">UpdateParameterInterfaceNamePayload</a>!</td>
<td>

Updates the name of a parameter interface.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateparameterinterfacenameinput">UpdateParameterInterfaceNameInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateParameterInterfaceParameterNamesAndKey</strong></td>
<td valign="top"><a href="#updateparameterinterfaceparameternamesandkeypayload">UpdateParameterInterfaceParameterNamesAndKeyPayload</a>!</td>
<td>

Updates the parameter names of a parameter interface.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateparameterinterfaceparameternamesandkeyinput">UpdateParameterInterfaceParameterNamesAndKeyInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateParameterInterfaceCalculationSettings</strong></td>
<td valign="top"><a href="#updateparameterinterfacecalculationsettingspayload">UpdateParameterInterfaceCalculationSettingsPayload</a>!</td>
<td>

Updates the calculation settings of a parameter interface.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateparameterinterfacecalculationsettingsinput">UpdateParameterInterfaceCalculationSettingsInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeParameterInterface</strong></td>
<td valign="top"><a href="#removeparameterinterfacepayload">RemoveParameterInterfacePayload</a>!</td>
<td>

Removes a parameter interface.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeparameterinterfaceinput">RemoveParameterInterfaceInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addProfileMachine</strong></td>
<td valign="top"><a href="#addprofilemachinepayload">AddProfileMachinePayload</a>!</td>
<td>

Adds a profile machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addprofilemachineinput">AddProfileMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateProfileMachine</strong></td>
<td valign="top"><a href="#updateprofilemachinepayload">UpdateProfileMachinePayload</a>!</td>
<td>

Updates a profile machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateprofilemachineinput">UpdateProfileMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeProfileMachine</strong></td>
<td valign="top"><a href="#removemachinepayload">RemoveMachinePayload</a>!</td>
<td>

Removes a profile machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removemachineinput">RemoveMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateProfileMachine</strong></td>
<td valign="top"><a href="#translateprofilemachinepayload">TranslateProfileMachinePayload</a>!</td>
<td>

Translate a profile machine with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translatemachineinput">TranslateMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addProfileParameter</strong></td>
<td valign="top"><a href="#addprofileparameterpayload">AddProfileParameterPayload</a>!</td>
<td>

Adds a profile parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addparameterinput">AddParameterInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateProfileParameter</strong></td>
<td valign="top"><a href="#updateprofileparameterpayload">UpdateProfileParameterPayload</a>!</td>
<td>

Updates a profile parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateparameterinput">UpdateParameterInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeProfileParameter</strong></td>
<td valign="top"><a href="#removeparameterpayload">RemoveParameterPayload</a>!</td>
<td>

Removes a profile parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeparameterinput">RemoveParameterInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateProfileParameter</strong></td>
<td valign="top"><a href="#translateprofileparameterpayload">TranslateProfileParameterPayload</a>!</td>
<td>

Translate a profile parameter with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translateparameterinput">TranslateParameterInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Profile Management

Profile Management 功能

### Profile Management Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>profileMachines</strong></td>
<td valign="top">[<a href="#profilemachine">ProfileMachine</a>!]!</td>
<td>

A list of profile machines.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>profileMachine</strong></td>
<td valign="top"><a href="#profilemachine">ProfileMachine</a></td>
<td>

Fetches a profile machine by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>profileParameter</strong></td>
<td valign="top"><a href="#profileparameter">ProfileParameter</a></td>
<td>

Fetches a profile parameter by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
</tbody>
</table>

### Profile Management Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>addProfileMachine</strong></td>
<td valign="top"><a href="#addprofilemachinepayload">AddProfileMachinePayload</a>!</td>
<td>

Adds a profile machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addprofilemachineinput">AddProfileMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateProfileMachine</strong></td>
<td valign="top"><a href="#updateprofilemachinepayload">UpdateProfileMachinePayload</a>!</td>
<td>

Updates a profile machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateprofilemachineinput">UpdateProfileMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeProfileMachine</strong></td>
<td valign="top"><a href="#removemachinepayload">RemoveMachinePayload</a>!</td>
<td>

Removes a profile machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removemachineinput">RemoveMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateProfileMachine</strong></td>
<td valign="top"><a href="#translateprofilemachinepayload">TranslateProfileMachinePayload</a>!</td>
<td>

Translate a profile machine with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translatemachineinput">TranslateMachineInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addProfileParameter</strong></td>
<td valign="top"><a href="#addprofileparameterpayload">AddProfileParameterPayload</a>!</td>
<td>

Adds a profile parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addparameterinput">AddParameterInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateProfileParameter</strong></td>
<td valign="top"><a href="#updateprofileparameterpayload">UpdateProfileParameterPayload</a>!</td>
<td>

Updates a profile parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateparameterinput">UpdateParameterInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeProfileParameter</strong></td>
<td valign="top"><a href="#removeparameterpayload">RemoveParameterPayload</a>!</td>
<td>

Removes a profile parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeparameterinput">RemoveParameterInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateProfileParameter</strong></td>
<td valign="top"><a href="#translateprofileparameterpayload">TranslateProfileParameterPayload</a>!</td>
<td>

Translate a profile parameter with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translateparameterinput">TranslateParameterInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Machine Status

### Machine Status Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>addMachineStatus</strong></td>
<td valign="top"><a href="#addmachinestatuspayload">AddMachineStatusPayload</a>!</td>
<td>

Adds a machine status.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addmachinestatusinput">AddMachineStatusInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateMachineStatus</strong></td>
<td valign="top"><a href="#updatemachinestatuspayload">UpdateMachineStatusPayload</a>!</td>
<td>

Updates a machine status.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatemachinestatusinput">UpdateMachineStatusInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeMachineStatus</strong></td>
<td valign="top"><a href="#removemachinestatuspayload">RemoveMachineStatusPayload</a>!</td>
<td>

Removes a machine status.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removemachinestatusinput">RemoveMachineStatusInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateMachineStatus</strong></td>
<td valign="top"><a href="#translatemachinestatuspayload">TranslateMachineStatusPayload</a>!</td>
<td>

Translate a machine status with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translatemachinestatusinput">TranslateMachineStatusInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Rule Management

### Rule Management Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterMappingCode</strong></td>
<td valign="top"><a href="#parametermappingcode">ParameterMappingCode</a></td>
<td>

Fetches a parameter mapping code by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterMappingCodesByIds</strong></td>
<td valign="top">[<a href="#parametermappingcode">ParameterMappingCode</a>]!</td>
<td>

A list of parameter mapping codes.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

IDs of some objects.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterMappings</strong></td>
<td valign="top">[<a href="#parametermappingrule">ParameterMappingRule</a>!]!</td>
<td>

A list of parameter mapping rules.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">pType</td>
<td valign="top"><a href="#tagvaluetype">TagValueType</a></td>
<td>

If specified, only returns mappings of this type.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterMapping</strong></td>
<td valign="top"><a href="#parametermappingrule">ParameterMappingRule</a></td>
<td>

Fetches a parameter mapping rule by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterMappingsByIds</strong></td>
<td valign="top">[<a href="#parametermappingrule">ParameterMappingRule</a>]!</td>
<td>

A list of parameter mapping rules.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

IDs of some objects.

</td>
</tr>
</tbody>
</table>

### Rule Management Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>addParameterMappingCodes</strong></td>
<td valign="top"><a href="#addparametermappingcodespayload">AddParameterMappingCodesPayload</a>!</td>
<td>

Adds a parameter mapping code.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addparametermappingcodesinput">AddParameterMappingCodesInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateParameterMappingCode</strong></td>
<td valign="top"><a href="#updateparametermappingcodepayload">UpdateParameterMappingCodePayload</a>!</td>
<td>

Updates a parameter mapping code.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateparametermappingcodeinput">UpdateParameterMappingCodeInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeParameterMappingCode</strong></td>
<td valign="top"><a href="#removeparametermappingcodepayload">RemoveParameterMappingCodePayload</a>!</td>
<td>

Removes a parameter mapping code.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeparametermappingcodeinput">RemoveParameterMappingCodeInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateParameterMappingCode</strong></td>
<td valign="top"><a href="#translateparametermappingcodepayload">TranslateParameterMappingCodePayload</a>!</td>
<td>

Translates a parameter mapping code.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translateparametermappingcodeinput">TranslateParameterMappingCodeInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>applyParameterMappingToParameters</strong></td>
<td valign="top"><a href="#applyparametermappingtoparameterspayload">ApplyParameterMappingToParametersPayload</a>!</td>
<td>

Applies a parameter mapping rule to parameters.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#applyparametermappingtoparametersinput">ApplyParameterMappingToParametersInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addParameterMappingRule</strong></td>
<td valign="top"><a href="#addparametermappingrulepayload">AddParameterMappingRulePayload</a>!</td>
<td>

Adds a parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addparametermappingruleinput">AddParameterMappingRuleInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateParameterMappingRule</strong></td>
<td valign="top"><a href="#updateparametermappingrulepayload">UpdateParameterMappingRulePayload</a>!</td>
<td>

Updates a parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateparametermappingruleinput">UpdateParameterMappingRuleInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeParameterMappingRule</strong></td>
<td valign="top"><a href="#removeparametermappingrulepayload">RemoveParameterMappingRulePayload</a>!</td>
<td>

Removes a parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeparametermappingruleinput">RemoveParameterMappingRuleInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>unbindParametersMapping</strong></td>
<td valign="top"><a href="#unbindparametersmappingpayload">UnbindParametersMappingPayload</a>!</td>
<td>

Unbind parameters mapping.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#unbindparametersmappinginput">UnbindParametersMappingInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Command Center

Command Center I.App 設定

### Command Center Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iAppGroups</strong></td>
<td valign="top">[<a href="#iappgroup">IAppGroup</a>!]!</td>
<td>

A list of I.App Groups.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iAppGroup</strong></td>
<td valign="top"><a href="#iappgroup">IAppGroup</a></td>
<td>

Fetches an I.App Group by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iAppMenuItems</strong></td>
<td valign="top">[<a href="#iappmenuitem">IAppMenuItem</a>!]!</td>
<td>

A list of I.App menu items.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iAppMenuItem</strong></td>
<td valign="top"><a href="#iappmenuitem">IAppMenuItem</a></td>
<td>

Fetches an I.App menu item by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iApps</strong></td>
<td valign="top">[<a href="#iapp">IApp</a>!]!</td>
<td>

A list of I.Apps.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iApp</strong></td>
<td valign="top"><a href="#iapp">IApp</a></td>
<td>

Fetches an I.App by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iAppsByIds</strong></td>
<td valign="top">[<a href="#iapp">IApp</a>]!</td>
<td>

A list of I.Apps.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

IDs of some objects.

</td>
</tr>
</tbody>
</table>

### Command Center Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>translateIAppGroup</strong></td>
<td valign="top"><a href="#translateiappgrouppayload">TranslateIAppGroupPayload</a>!</td>
<td>

Translate an I.App Group with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translateiappgroupinput">TranslateIAppGroupInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeIAppGroupTranslation</strong></td>
<td valign="top"><a href="#removeiappgrouptranslationpayload">RemoveIAppGroupTranslationPayload</a>!</td>
<td>

Remove an I.App Group translate with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeiappgrouptranslationinput">RemoveIAppGroupTranslationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addIAppGroup</strong></td>
<td valign="top"><a href="#addiappgroupgrouppayload">AddIAppGroupGroupPayload</a>!</td>
<td>

Adds an I.App Group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addiappgroupinput">AddIAppGroupInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateIAppGroup</strong></td>
<td valign="top"><a href="#updateiappgrouppayload">UpdateIAppGroupPayload</a>!</td>
<td>

Updates an I.App Group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateiappgroupinput">UpdateIAppGroupInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateIAppGroupIndexes</strong></td>
<td valign="top"><a href="#updateiappgroupindexespayload">UpdateIAppGroupIndexesPayload</a>!</td>
<td>

Updates I.App Group indexes.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateiappgroupindexesinput">UpdateIAppGroupIndexesInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeIAppGroup</strong></td>
<td valign="top"><a href="#removeiappgrouppayload">RemoveIAppGroupPayload</a>!</td>
<td>

Removes an I.App Group and its I.Apps.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeiappgroupinput">RemoveIAppGroupInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateIAppMenuItem</strong></td>
<td valign="top"><a href="#translateiappmenuitempayload">TranslateIAppMenuItemPayload</a>!</td>
<td>

Translate an I.App menu item with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translateiappmenuiteminput">TranslateIAppMenuItemInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeIAppMenuItemTranslation</strong></td>
<td valign="top"><a href="#removeiappmenuitemtranslationpayload">RemoveIAppMenuItemTranslationPayload</a>!</td>
<td>

Remove an I.App menu item translate with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeiappmenuitemtranslationinput">RemoveIAppMenuItemTranslationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addIAppMenuItem</strong></td>
<td valign="top"><a href="#addiappmenuitempayload">AddIAppMenuItemPayload</a>!</td>
<td>

Adds an I.App menu item.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addiappmenuiteminput">AddIAppMenuItemInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateIAppMenuItem</strong></td>
<td valign="top"><a href="#updateiappmenuitempayload">UpdateIAppMenuItemPayload</a>!</td>
<td>

Updates an I.App menu item.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateiappmenuiteminput">UpdateIAppMenuItemInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateIAppMenuItemIndexes</strong></td>
<td valign="top"><a href="#updateiappmenuitemindexespayload">UpdateIAppMenuItemIndexesPayload</a>!</td>
<td>

Updates I.App menu item indexes.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateiappmenuitemindexesinput">UpdateIAppMenuItemIndexesInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeIAppMenuItem</strong></td>
<td valign="top"><a href="#removeiappmenuitempayload">RemoveIAppMenuItemPayload</a>!</td>
<td>

Removes an I.App menu item.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeiappmenuiteminput">RemoveIAppMenuItemInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateIApp</strong></td>
<td valign="top"><a href="#translateiapppayload">TranslateIAppPayload</a>!</td>
<td>

Translate an I.App with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translateiappinput">TranslateIAppInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeIAppTranslation</strong></td>
<td valign="top"><a href="#removeiapptranslationpayload">RemoveIAppTranslationPayload</a>!</td>
<td>

Remove an I.App translate with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeiapptranslationinput">RemoveIAppTranslationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addIApp</strong></td>
<td valign="top"><a href="#addiapppayload">AddIAppPayload</a>!</td>
<td>

Adds an I.App.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addiappinput">AddIAppInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateIApp</strong></td>
<td valign="top"><a href="#updateiapppayload">UpdateIAppPayload</a>!</td>
<td>

Updates an I.App.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateiappinput">UpdateIAppInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateIAppIndexes</strong></td>
<td valign="top"><a href="#updateiappindexespayload">UpdateIAppIndexesPayload</a>!</td>
<td>

Updates I.Apps indexes.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateiappindexesinput">UpdateIAppIndexesInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeIApp</strong></td>
<td valign="top"><a href="#removeiapppayload">RemoveIAppPayload</a>!</td>
<td>

Removes an I.App.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeiappinput">RemoveIAppInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Production KPI

### Production KPI Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kpiMetrics</strong></td>
<td valign="top">[<a href="#kpimetric">KpiMetric</a>!]!</td>
<td>

A list of kpi metrics.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpiMetric</strong></td>
<td valign="top"><a href="#kpimetric">KpiMetric</a></td>
<td>

Fetches a kpi metric by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpiMetricsByIds</strong></td>
<td valign="top">[<a href="#kpimetric">KpiMetric</a>]!</td>
<td>

A list of kpi metrics.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

IDs of some objects.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpiParameterUsages</strong></td>
<td valign="top">[<a href="#kpiparameterusage">KpiParameterUsage</a>!]!</td>
<td>

A list of kpi parameter usages.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpiParameterUsage</strong></td>
<td valign="top"><a href="#kpiparameterusage">KpiParameterUsage</a></td>
<td>

Fetches a kpi parameter usage by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpiParameterUsagesByIds</strong></td>
<td valign="top">[<a href="#kpiparameterusage">KpiParameterUsage</a>]!</td>
<td>

A list of kpi parameter usages.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

IDs of some objects.

</td>
</tr>
</tbody>
</table>

### Production KPI Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>addKpiMetric</strong></td>
<td valign="top"><a href="#addkpimetricpayload">AddKpiMetricPayload</a>!</td>
<td>

Adds a kpi metric.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addkpimetricinput">AddKpiMetricInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateKpiMetric</strong></td>
<td valign="top"><a href="#updatekpimetricpayload">UpdateKpiMetricPayload</a>!</td>
<td>

Updates a kpi metric.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatekpimetricinput">UpdateKpiMetricInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeKpiMetric</strong></td>
<td valign="top"><a href="#removekpimetricpayload">RemoveKpiMetricPayload</a>!</td>
<td>

Removes a kpi metric.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removekpimetricinput">RemoveKpiMetricInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateKpiMetric</strong></td>
<td valign="top"><a href="#translatekpimetricpayload">TranslateKpiMetricPayload</a>!</td>
<td>

Translate a kpi metric with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translatekpimetricinput">TranslateKpiMetricInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeKpiMetricTranslation</strong></td>
<td valign="top"><a href="#removekpimetrictranslationpayload">RemoveKpiMetricTranslationPayload</a>!</td>
<td>

Remove a kpi metric translate with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removekpimetrictranslationinput">RemoveKpiMetricTranslationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addKpiParameterUsageRelation</strong></td>
<td valign="top"><a href="#addkpiparameterusagerelationpayload">AddKpiParameterUsageRelationPayload</a>!</td>
<td>

Adds a kpi parameter usage relation.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addkpiparameterusagerelationinput">AddKpiParameterUsageRelationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeKpiParameterUsageRelation</strong></td>
<td valign="top"><a href="#removekpiparameterusagerelationpayload">RemoveKpiParameterUsageRelationPayload</a>!</td>
<td>

Removes an kpi parameter usage relation.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removekpiparameterusagerelationinput">RemoveKpiParameterUsageRelationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addKpiParameterUsage</strong></td>
<td valign="top"><a href="#addkpiparameterusagepayload">AddKpiParameterUsagePayload</a>!</td>
<td>

Adds a kpi parameter usage.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addkpiparameterusageinput">AddKpiParameterUsageInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateKpiParameterUsage</strong></td>
<td valign="top"><a href="#updatekpiparameterusagepayload">UpdateKpiParameterUsagePayload</a>!</td>
<td>

Updates a kpi parameter usage.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatekpiparameterusageinput">UpdateKpiParameterUsageInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeKpiParameterUsage</strong></td>
<td valign="top"><a href="#removekpiparameterusagepayload">RemoveKpiParameterUsagePayload</a>!</td>
<td>

Removes an kpi parameter usage.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removekpiparameterusageinput">RemoveKpiParameterUsageInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setProductionKpi</strong></td>
<td valign="top"><a href="#setproductionkpipayload">SetProductionKpiPayload</a>!</td>
<td>

Set Production KPI on Group or Machine or Parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setproductionkpiinput">SetProductionKpiInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Data Gateway

Gateway 與 ifp-data-hub 相關資訊

### Data Gateway Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>gateway</strong></td>
<td valign="top"><a href="#gateway">Gateway</a></td>
<td>

Fetches the gateway by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gateways</strong></td>
<td valign="top">[<a href="#gateway">Gateway</a>!]!</td>
<td>

List gateways.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">searchNameOrUsername</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Fuzzy Search of Gateway Name or Username

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kinds</td>
<td valign="top">[<a href="#gatewaykind">GatewayKind</a>!]</td>
<td>

Filter Gateway Kind

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gatewaysByScadaId</strong></td>
<td valign="top">[<a href="#gateway">Gateway</a>!]!</td>
<td>

List gateways by scadaId.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">searchScadaId</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Fuzzy Search of ScadaId

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gatewayByScadaId</strong></td>
<td valign="top"><a href="#gateway">Gateway</a></td>
<td>

Find the gateway by scadaId.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">scadaId</td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

ScadaId to Find

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>outboundTransportProtocols</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Protocols supported by Outbound transports.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">transport</td>
<td valign="top"><a href="#outboundtransport">OutboundTransport</a></td>
<td>

Filter by the transport of Outbound.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>outboundList</strong></td>
<td valign="top">[<a href="#outbound">Outbound</a>!]!</td>
<td>

A list of Outbound.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">transport</td>
<td valign="top"><a href="#outboundtransport">OutboundTransport</a></td>
<td>

Filter by the transport of Outbound.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>outbound</strong></td>
<td valign="top"><a href="#outbound">Outbound</a></td>
<td>

Fetches an Outbound by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>associatedParameterIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

List parameter ids with various kinds by source.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">sourceParameterId</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### Data Gateway Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>setStatusExpirationPeriod</strong></td>
<td valign="top"><a href="#setstatusexpirationperiodpayload">SetStatusExpirationPeriodPayload</a>!</td>
<td>

Set gateway current status expiration period.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setstatusexpirationperiodinput">SetStatusExpirationPeriodInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addGateway</strong></td>
<td valign="top"><a href="#addgatewaypayload">AddGatewayPayload</a>!</td>
<td>

Add a gateway.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addgatewayinput">AddGatewayInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeGateway</strong></td>
<td valign="top"><a href="#removegatewaypayload">RemoveGatewayPayload</a>!</td>
<td>

Remove the gateway.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removegatewayinput">RemoveGatewayInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateGateway</strong></td>
<td valign="top"><a href="#updategatewaypayload">UpdateGatewayPayload</a>!</td>
<td>

Update the gateway.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updategatewayinput">UpdateGatewayInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateDHRetentionPolicy</strong></td>
<td valign="top"><a href="#void">Void</a></td>
<td>

Update the retention policy.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatedhretentionpolicyinput">UpdateDHRetentionPolicyInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addOutbound</strong></td>
<td valign="top"><a href="#addoutboundpayload">AddOutboundPayload</a>!</td>
<td>

Adds an Outbound.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addoutboundinput">AddOutboundInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateOutbound</strong></td>
<td valign="top"><a href="#updateoutboundpayload">UpdateOutboundPayload</a>!</td>
<td>

Updates an Outbound.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateoutboundinput">UpdateOutboundInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeOutbound</strong></td>
<td valign="top"><a href="#removeoutboundpayload">RemoveOutboundPayload</a>!</td>
<td>

Removes an Outbound.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeoutboundinput">RemoveOutboundInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>outboundIfpCfg</strong></td>
<td valign="top"><a href="#void">Void</a></td>
<td>

Outbound IfpCfg.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#outboundifpcfginput">OutboundIfpCfgInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setConstantParameterValues</strong></td>
<td valign="top"><a href="#setconstantparametervaluespayload">SetConstantParameterValuesPayload</a></td>
<td>

Sets Constant Parameter values.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setconstantparametervaluesinput">SetConstantParameterValuesInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dropConstantParameterValues</strong></td>
<td valign="top"><a href="#void">Void</a></td>
<td>

Drops Constant Parameter values.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#dropconstantparametervaluesinput">DropConstantParameterValuesInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setTransformationPoint</strong></td>
<td valign="top"><a href="#settransformationpointpayload">SetTransformationPointPayload</a>!</td>
<td>

Sets Transformation Point.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#settransformationpointinput">SetTransformationPointInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeTransformationPoint</strong></td>
<td valign="top"><a href="#removetransformationpointpayload">RemoveTransformationPointPayload</a>!</td>
<td>

Removes a Transformation Point.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removetransformationpointinput">RemoveTransformationPointInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dryRunTransformationPoint</strong></td>
<td valign="top"><a href="#dryruntransformationpointpayload">DryRunTransformationPointPayload</a>!</td>
<td>

Dry runs Transformation Point.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#dryruntransformationpointinput">DryRunTransformationPointInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setCalculationPoint</strong></td>
<td valign="top"><a href="#setcalculationpointpayload">SetCalculationPointPayload</a>!</td>
<td>

Sets Calculation Point.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setcalculationpointinput">SetCalculationPointInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeCalculationPoint</strong></td>
<td valign="top"><a href="#removecalculationpointpayload">RemoveCalculationPointPayload</a>!</td>
<td>

Removes a Calculation Point.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removecalculationpointinput">RemoveCalculationPointInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dropPredictionPointValues</strong></td>
<td valign="top"><a href="#void">Void</a></td>
<td>

Drop Values of a Prediction Point.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#droppredictionpointvaluesinput">DropPredictionPointValuesInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>writeTagValue</strong></td>
<td valign="top"><a href="#void">Void</a></td>
<td>

Write tag value.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#writetagvalueinput">WriteTagValueInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>turnOnAcquiring</strong></td>
<td valign="top"><a href="#turnonacquiringpayload">TurnOnAcquiringPayload</a>!</td>
<td>

Turn on acquiring of the tag.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#turnonacquiringinput">TurnOnAcquiringInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>turnOffAcquiring</strong></td>
<td valign="top"><a href="#turnoffacquiringpayload">TurnOffAcquiringPayload</a>!</td>
<td>

Turn off acquiring of the tag.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#turnoffacquiringinput">TurnOffAcquiringInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>turnOffSaveToHistory</strong></td>
<td valign="top"><a href="#turnoffsavetohistorypayload">TurnOffSaveToHistoryPayload</a>!</td>
<td>

Turn off saveToHistory of the tag.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#turnoffsavetohistoryinput">TurnOffSaveToHistoryInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleteValuesInRange</strong></td>
<td valign="top"><a href="#deletevaluesinrangepayload">DeleteValuesInRangePayload</a>!</td>
<td>

Delete all tag values (include bad tag values) in time range.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#deletevaluesinrangeinput">DeleteValuesInRangeInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>unbindGateway</strong></td>
<td valign="top"><a href="#unbindgatewaypayload">UnbindGatewayPayload</a>!</td>
<td>

Unbind groups, machines and parameters with gateway.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#unbindgatewayinput">UnbindGatewayInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fullOutboundIfpCfg</strong></td>
<td valign="top"><a href="#fulloutboundifpcfgpayload">FullOutboundIfpCfgPayload</a>!</td>
<td>

Run full outbound IfpCfg.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#fulloutboundifpcfginput">FullOutboundIfpCfgInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Event/Notification

Alarm Event 與 Notification 與 ifp-rule-engine 相關資訊

### Event/Notification Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>event</strong></td>
<td valign="top"><a href="#event">Event</a></td>
<td>

Fetches the event by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmCodeEvents</strong></td>
<td valign="top">[<a href="#alarmcodeevent">AlarmCodeEvent</a>!]!</td>
<td>

List alarm code events.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">searchName</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Fuzzy Search of Alarm Code Event Name

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmLevels</strong></td>
<td valign="top">[<a href="#alarmlevel">AlarmLevel</a>!]!</td>
<td>

List alarmLevels.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>highLowEvents</strong></td>
<td valign="top">[<a href="#highlowevent">HighLowEvent</a>!]!</td>
<td>

List High-Low events.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">searchName</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Fuzzy Search of High-Low Event Name

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>notificationGroups</strong></td>
<td valign="top">[<a href="#notificationgroup">NotificationGroup</a>!]!</td>
<td>

List all Notification Center groups.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>notificationGroup</strong></td>
<td valign="top"><a href="#notificationgroup">NotificationGroup</a>!</td>
<td>

Fetches the Notification Center group by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>notificationGroupName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Fetches the Notification Center group name by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### Event/Notification Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removeEventByParameter</strong></td>
<td valign="top"><a href="#removeeventbyparameterpayload">RemoveEventByParameterPayload</a>!</td>
<td>

Remove the event by parameter node ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeeventbyparameterinput">RemoveEventByParameterInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addAlarmCodeEvent</strong></td>
<td valign="top"><a href="#addalarmcodeeventpayload">AddAlarmCodeEventPayload</a>!</td>
<td>

Add an alarm code event.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addalarmcodeeventinput">AddAlarmCodeEventInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeAlarmCodeEvent</strong></td>
<td valign="top"><a href="#removealarmcodeeventpayload">RemoveAlarmCodeEventPayload</a>!</td>
<td>

Remove the alarm code event.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removealarmcodeeventinput">RemoveAlarmCodeEventInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateAlarmCodeEvent</strong></td>
<td valign="top"><a href="#updatealarmcodeeventpayload">UpdateAlarmCodeEventPayload</a>!</td>
<td>

Update the alarm code event.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatealarmcodeeventinput">UpdateAlarmCodeEventInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addAlarmLevel</strong></td>
<td valign="top"><a href="#addalarmlevelpayload">AddAlarmLevelPayload</a>!</td>
<td>

Add an alarmLevel.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addalarmlevelinput">AddAlarmLevelInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeAlarmLevel</strong></td>
<td valign="top"><a href="#removealarmlevelpayload">RemoveAlarmLevelPayload</a>!</td>
<td>

Remove the alarmLevel.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removealarmlevelinput">RemoveAlarmLevelInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateAlarmLevel</strong></td>
<td valign="top"><a href="#updatealarmlevelpayload">UpdateAlarmLevelPayload</a>!</td>
<td>

Update the alarmLevel.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatealarmlevelinput">UpdateAlarmLevelInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateAlarmLevel</strong></td>
<td valign="top"><a href="#translatealarmlevelpayload">TranslateAlarmLevelPayload</a>!</td>
<td>

Translate the alarm level with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translatealarmlevelinput">TranslateAlarmLevelInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeAlarmLevelTranslation</strong></td>
<td valign="top"><a href="#removealarmleveltranslationpayload">RemoveAlarmLevelTranslationPayload</a>!</td>
<td>

Remove the alarm level translation with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removealarmleveltranslationinput">RemoveAlarmLevelTranslationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addCodeRule</strong></td>
<td valign="top"><a href="#addcoderulepayload">AddCodeRulePayload</a>!</td>
<td>

Add a code rule of the alarm code event.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addcoderuleinput">AddCodeRuleInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeCodeRule</strong></td>
<td valign="top"><a href="#removecoderulepayload">RemoveCodeRulePayload</a>!</td>
<td>

Remove the code rule.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removecoderuleinput">RemoveCodeRuleInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateCodeRule</strong></td>
<td valign="top"><a href="#updatecoderulepayload">UpdateCodeRulePayload</a>!</td>
<td>

Update the code rule.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatecoderuleinput">UpdateCodeRuleInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeCodeRulesByCodeIds</strong></td>
<td valign="top"><a href="#removecoderulesbycodeidspayload">RemoveCodeRulesByCodeIdsPayload</a>!</td>
<td>

Remove the code rules by mapping code node IDs.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removecoderulesbycodeidsinput">RemoveCodeRulesByCodeIdsInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addHighLowEvent</strong></td>
<td valign="top"><a href="#addhighloweventpayload">AddHighLowEventPayload</a>!</td>
<td>

Add a High-Low event.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addhighloweventinput">AddHighLowEventInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeHighLowEvent</strong></td>
<td valign="top"><a href="#removehighloweventpayload">RemoveHighLowEventPayload</a>!</td>
<td>

Remove the High-Low event.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removehighloweventinput">RemoveHighLowEventInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateHighLowEvent</strong></td>
<td valign="top"><a href="#updatehighloweventpayload">UpdateHighLowEventPayload</a>!</td>
<td>

Update the High-Low event.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatehighloweventinput">UpdateHighLowEventInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addMarqueeAction</strong></td>
<td valign="top"><a href="#addmarqueeactionpayload">AddMarqueeActionPayload</a>!</td>
<td>

Add a marquee action of the rule.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addmarqueeactioninput">AddMarqueeActionInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeMarqueeAction</strong></td>
<td valign="top"><a href="#removemarqueeactionpayload">RemoveMarqueeActionPayload</a>!</td>
<td>

Remove the marquee action.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removemarqueeactioninput">RemoveMarqueeActionInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateMarqueeAction</strong></td>
<td valign="top"><a href="#updatemarqueeactionpayload">UpdateMarqueeActionPayload</a>!</td>
<td>

Update the marquee action.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatemarqueeactioninput">UpdateMarqueeActionInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addNotificationAction</strong></td>
<td valign="top"><a href="#addnotificationactionpayload">AddNotificationActionPayload</a>!</td>
<td>

Add a notification action of the rule.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addnotificationactioninput">AddNotificationActionInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeNotificationAction</strong></td>
<td valign="top"><a href="#removenotificationactionpayload">RemoveNotificationActionPayload</a>!</td>
<td>

Remove the notification action.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removenotificationactioninput">RemoveNotificationActionInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateNotificationAction</strong></td>
<td valign="top"><a href="#updatenotificationactionpayload">UpdateNotificationActionPayload</a>!</td>
<td>

Update the notification action.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatenotificationactioninput">UpdateNotificationActionInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addThresholdRule</strong></td>
<td valign="top"><a href="#addthresholdrulepayload">AddThresholdRulePayload</a>!</td>
<td>

Add a threshold rule of the High-Low event.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addthresholdruleinput">AddThresholdRuleInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeThresholdRule</strong></td>
<td valign="top"><a href="#removethresholdrulepayload">RemoveThresholdRulePayload</a>!</td>
<td>

Remove the threshold rule.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removethresholdruleinput">RemoveThresholdRuleInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateThresholdRule</strong></td>
<td valign="top"><a href="#updatethresholdrulepayload">UpdateThresholdRulePayload</a>!</td>
<td>

Update the threshold rule.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatethresholdruleinput">UpdateThresholdRuleInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateThresholdRule</strong></td>
<td valign="top"><a href="#translatethresholdrulepayload">TranslateThresholdRulePayload</a>!</td>
<td>

Translate the threshold rule with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translatethresholdruleinput">TranslateThresholdRuleInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeThresholdRuleTranslation</strong></td>
<td valign="top"><a href="#removethresholdruletranslationpayload">RemoveThresholdRuleTranslationPayload</a>!</td>
<td>

Remove the threshold rule translation with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removethresholdruletranslationinput">RemoveThresholdRuleTranslationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeTranslationsByLang</strong></td>
<td valign="top"><a href="#removetranslationsbylangpayload">RemoveTranslationsByLangPayload</a>!</td>
<td>

Remove translations by language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removetranslationsbylanginput">RemoveTranslationsByLangInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Calculator

ifp-calculator 相關 API

### Calculator Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>formulas</strong></td>
<td valign="top">[<a href="#formula">Formula</a>!]!</td>
<td>

A list of all formulas.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulasByRoot</strong></td>
<td valign="top">[<a href="#formula">Formula</a>!]!</td>
<td>

A list of formulas by root.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulasGroupByRoot</strong></td>
<td valign="top">[<a href="#formulaslist">FormulasList</a>!]!</td>
<td>

Formulas with roots.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formula</strong></td>
<td valign="top"><a href="#formula">Formula</a></td>
<td>

Fetch a formula by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulasByIds</strong></td>
<td valign="top">[<a href="#formula">Formula</a>]!</td>
<td>

A list of formulas.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

IDs of some objects.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>calculateFormula</strong></td>
<td valign="top"><a href="#calculateformulapayload">CalculateFormulaPayload</a>!</td>
<td>

Calculate by formula id.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">variables</td>
<td valign="top"><a href="#jsonobject">JSONObject</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### Calculator Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>addFormulasFromText</strong></td>
<td valign="top"><a href="#addformulaspayload">AddFormulasPayload</a>!</td>
<td>

Add Formulas From Text.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">text</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addFormulas</strong></td>
<td valign="top"><a href="#addformulaspayload">AddFormulasPayload</a>!</td>
<td>

Add formulas with binary tree array representation.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addformulasinput">AddFormulasInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeFormulasByRoot</strong></td>
<td valign="top"><a href="#removeformulapayload">RemoveFormulaPayload</a>!</td>
<td>

Remove formulas by root id.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeformulainput">RemoveFormulaInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Generic KPI

以 RTM group/machine/parameter 為底設定的 KPI 資訊，包含 Holidays 設定與 Energy KPI

### Generic KPI Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>holidays</strong></td>
<td valign="top">[<a href="#generickpiholiday">GenericKpiHoliday</a>!]!</td>
<td>

List Generic KPI Holidays.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">year</td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

The year of Generic KPI Holiday.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">month</td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td>

The month of Generic KPI Holiday, 0 ~ 11.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">date</td>
<td valign="top"><a href="#positiveint">PositiveInt</a></td>
<td>

The date of Generic KPI Holiday, 1 ~ 31.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>genericKpiByMachines</strong></td>
<td valign="top">[<a href="#generickpi">GenericKpi</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">machineIds</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">relationKeys</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Which relationKeys to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kind</td>
<td valign="top"><a href="#generickpialertkind">GenericKpiAlertKind</a>!</td>
<td>

Which kind to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isoDate</td>
<td valign="top"><a href="#date">Date</a>!</td>
<td>

The full date on Generic KPI Calendar.

</td>
</tr>
</tbody>
</table>

### Generic KPI Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#enablealertsinput">EnableAlertsInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>disableAlerts</strong></td>
<td valign="top"><a href="#disablealertspayload">DisableAlertsPayload</a>!</td>
<td>

Disable Generic KPI Alerts.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#disablealertsinput">DisableAlertsInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setAsHolidays</strong></td>
<td valign="top"><a href="#setasholidayspayload">SetAsHolidaysPayload</a>!</td>
<td>

Set Dates as Generic KPI Holidays.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setasholidaysinput">SetAsHolidaysInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeHolidays</strong></td>
<td valign="top"><a href="#removeholidayspayload">RemoveHolidaysPayload</a>!</td>
<td>

Remove Dates from Generic KPI Holidays.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeholidaysinput">RemoveHolidaysInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setGenericKpiList</strong></td>
<td valign="top"><a href="#setgenerickpilistpayload">SetGenericKpiListPayload</a>!</td>
<td>

Set Generic KPI List.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setgenerickpilistinput">SetGenericKpiListInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>bulkSetGenericKpiList</strong></td>
<td valign="top"><a href="#bulksetgenerickpilistpayload">BulkSetGenericKpiListPayload</a>!</td>
<td>

Bulk Set Generic KPI List.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#bulksetgenerickpilistinput">BulkSetGenericKpiListInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeGenericKpiList</strong></td>
<td valign="top"><a href="#removegenerickpilistpayload">RemoveGenericKpiListPayload</a>!</td>
<td>

Remove Generic KPI List.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removegenerickpilistinput">RemoveGenericKpiListInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## EHS

ifp-ehs 或是 FEMS 或是 FMS 相關 API

### Energy Device

設定 Energy device 項目，kind 種類，usage 為子分類

#### Energy Device Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyDeviceKinds</strong></td>
<td valign="top">[<a href="#energydevicekind">EnergyDeviceKind</a>!]!</td>
<td>

A list of energy device kinds.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyDeviceKind</strong></td>
<td valign="top"><a href="#energydevicekind">EnergyDeviceKind</a>!</td>
<td>

Fetches an energy device kind by its key.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">key</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyDevices</strong></td>
<td valign="top">[<a href="#energydevice">EnergyDevice</a>!]!</td>
<td>

A list of Energy Devices.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kind</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Filter by the kind of Energy Device.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usages</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filter by the usages of kind of Energy Device.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isExclude</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
</tbody>
</table>

#### Energy Device Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>addEnergyDeviceKind</strong></td>
<td valign="top"><a href="#addenergydevicekindpayload">AddEnergyDeviceKindPayload</a>!</td>
<td>

Add an energy device kind.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addenergydevicekindinput">AddEnergyDeviceKindInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeEnergyDeviceKind</strong></td>
<td valign="top"><a href="#removeenergydevicekindpayload">RemoveEnergyDeviceKindPayload</a>!</td>
<td>

Remove the energy device kind.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeenergydevicekindinput">RemoveEnergyDeviceKindInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateEnergyDeviceKind</strong></td>
<td valign="top"><a href="#updateenergydevicekindpayload">UpdateEnergyDeviceKindPayload</a>!</td>
<td>

Update the energy device kind.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateenergydevicekindinput">UpdateEnergyDeviceKindInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateEnergyDeviceKind</strong></td>
<td valign="top"><a href="#translateenergydevicekindpayload">TranslateEnergyDeviceKindPayload</a>!</td>
<td>

Translate the energy device kind with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translateenergydevicekindinput">TranslateEnergyDeviceKindInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeEnergyDeviceKindTranslation</strong></td>
<td valign="top"><a href="#removeenergydevicekindtranslationpayload">RemoveEnergyDeviceKindTranslationPayload</a>!</td>
<td>

Remove the energy device kind translation with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeenergydevicekindtranslationinput">RemoveEnergyDeviceKindTranslationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setEnergyDevice</strong></td>
<td valign="top"><a href="#setenergydevicepayload">SetEnergyDevicePayload</a>!</td>
<td>

Set Energy Device on Machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setenergydeviceinput">SetEnergyDeviceInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addEnergyUsage</strong></td>
<td valign="top"><a href="#addenergyusagepayload">AddEnergyUsagePayload</a>!</td>
<td>

Add an energy usage.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addenergyusageinput">AddEnergyUsageInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeEnergyUsage</strong></td>
<td valign="top"><a href="#removeenergyusagepayload">RemoveEnergyUsagePayload</a>!</td>
<td>

Remove the energy usage.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeenergyusageinput">RemoveEnergyUsageInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateEnergyUsage</strong></td>
<td valign="top"><a href="#updateenergyusagepayload">UpdateEnergyUsagePayload</a>!</td>
<td>

Update the energy usage.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateenergyusageinput">UpdateEnergyUsageInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translateEnergyUsage</strong></td>
<td valign="top"><a href="#translateenergyusagepayload">TranslateEnergyUsagePayload</a>!</td>
<td>

Translate the energy usage with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translateenergyusageinput">TranslateEnergyUsageInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeEnergyUsageTranslation</strong></td>
<td valign="top"><a href="#removeenergyusagetranslationpayload">RemoveEnergyUsageTranslationPayload</a>!</td>
<td>

Remove the energy usage translation with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeenergyusagetranslationinput">RemoveEnergyUsageTranslationInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setGroupEnergyUsageInterfaceRelation</strong></td>
<td valign="top"><a href="#setgroupenergyusageinterfacerelationpayload">SetGroupEnergyUsageInterfaceRelationPayload</a>!</td>
<td>

Sets a group energy usage interface relation.

</td>
</tr>
</tbody>
</table>

### Parameter Usage

綁定 machine/parameter 資料到 EHS 運算使用

#### Parameter Usage Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>ehsUsages</strong></td>
<td valign="top">[<a href="#ehsusage">EhsUsage</a>!]!</td>
<td>

A list of usages.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usageTypes</td>
<td valign="top">[<a href="#ehsusagetype">EhsUsageType</a>!]!</td>
<td>

the usage types

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ehsUsage</strong></td>
<td valign="top"><a href="#ehsusage">EhsUsage</a></td>
<td>

Fetches a usage by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ehsUsagesByIds</strong></td>
<td valign="top">[<a href="#ehsusage">EhsUsage</a>]!</td>
<td>

A list of usages.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

IDs of some objects.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ehsUsagesByParameter</strong></td>
<td valign="top">[<a href="#ehsusage">EhsUsage</a>!]!</td>
<td>

A list of usages by parameter ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
</tbody>
</table>

#### Parameter Usage Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>addEhsUsage</strong></td>
<td valign="top"><a href="#addehsusagepayload">AddEhsUsagePayload</a></td>
<td>

Adds a usage.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addehsusageinput">AddEhsUsageInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeEhsUsage</strong></td>
<td valign="top"><a href="#removeehsusagepayload">RemoveEhsUsagePayload</a>!</td>
<td>

Removes a usage.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeehsusageinput">RemoveEhsUsageInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### Energy KPI Settings

FEMS 設定與 demand/consumption 相關 KPI，其餘請參考 Generic KPI

#### Energy KPI Settings Query

#### Energy KPI Settings Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>setEnergyKpi</strong></td>
<td valign="top"><a href="#setenergykpipayload">SetEnergyKpiPayload</a>!</td>
<td>

Set Energy KPI on Group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setenergykpiinput">SetEnergyKpiInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>generateEHSKpiConfig</strong></td>
<td valign="top"><a href="#generateehskpiconfigpayload">GenerateEHSKpiConfigPayload</a>!</td>
<td>

Generate EHS KPI Configuration.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#generateehskpiconfiginput">GenerateEHSKpiConfigInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### Time-of-Use Period

離尖峰設定

#### Time-of-Use Period Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>periodTypeInfos</strong></td>
<td valign="top">[<a href="#periodtypeinfo">PeriodTypeInfo</a>!]!</td>
<td>

A list of period types.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>periodTypeInfo</strong></td>
<td valign="top"><a href="#periodtypeinfo">PeriodTypeInfo</a></td>
<td>

Fetches a period type information by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeOfUsePeriod</strong></td>
<td valign="top"><a href="#timeofuseperiod">TimeOfUsePeriod</a></td>
<td>

Fetches a Time-of-Use period by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeOfUsePeriodsByIds</strong></td>
<td valign="top">[<a href="#timeofuseperiod">TimeOfUsePeriod</a>]!</td>
<td>

A list of Time-of-Use periods.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">ids</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

IDs of some objects.

</td>
</tr>
</tbody>
</table>

#### Time-of-Use Period Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>translatePeriodTypeInfo</strong></td>
<td valign="top"><a href="#translateperiodtypeinfopayload">TranslatePeriodTypeInfoPayload</a>!</td>
<td>

Translate a period type with specific language.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#translateperiodtypeinfoinput">TranslatePeriodTypeInfoInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addTimeOfUsePeriod</strong></td>
<td valign="top"><a href="#addtimeofuseperiodpayload">AddTimeOfUsePeriodPayload</a>!</td>
<td>

Adds a Time-of-Use period.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addtimeofuseperiodinput">AddTimeOfUsePeriodInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateTimeOfUsePeriod</strong></td>
<td valign="top"><a href="#updatetimeofuseperiodpayload">UpdateTimeOfUsePeriodPayload</a>!</td>
<td>

Updates a Time-of-Use period.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updatetimeofuseperiodinput">UpdateTimeOfUsePeriodInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeTimeOfUsePeriod</strong></td>
<td valign="top"><a href="#removetimeofuseperiodpayload">RemoveTimeOfUsePeriodPayload</a>!</td>
<td>

Removes a Time-of-Use period.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removetimeofuseperiodinput">RemoveTimeOfUsePeriodInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### Energy Split

部門分攤與虛擬電表

#### Energy Split Query

#### Energy Split Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>setEnergySplit</strong></td>
<td valign="top"><a href="#setenergysplitpayload">SetEnergySplitPayload</a>!</td>
<td>

Set Energy Split on Group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setenergysplitinput">SetEnergySplitInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### Energy Usage Interface

#### Energy Usage Interface Query

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyUsageInterfaces</strong></td>
<td valign="top">[<a href="#energyusageinterface">EnergyUsageInterface</a>!]!</td>
<td>

A list of energy usage interfaces.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyUsageInterface</strong></td>
<td valign="top"><a href="#energyusageinterface">EnergyUsageInterface</a>!</td>
<td>

Fetches an energy usage interface by its ID.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
</tbody>
</table>

#### Energy Usage Interface Mutation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>addEnergyUsageInterface</strong></td>
<td valign="top"><a href="#addenergyusageinterfacepayload">AddEnergyUsageInterfacePayload</a>!</td>
<td>

Adds an energy usage interface.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#addenergyusageinterfaceinput">AddEnergyUsageInterfaceInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateEnergyUsageInterfaceName</strong></td>
<td valign="top"><a href="#updateenergyusageinterfacenamepayload">UpdateEnergyUsageInterfaceNamePayload</a>!</td>
<td>

Updates the name of an energy usage interface.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateenergyusageinterfacenameinput">UpdateEnergyUsageInterfaceNameInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateEnergyUsageInterfaceUsages</strong></td>
<td valign="top"><a href="#updateenergyusageinterfaceusagespayload">UpdateEnergyUsageInterfaceUsagesPayload</a>!</td>
<td>

Updates the usages of an energy usage interface.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#updateenergyusageinterfaceusagesinput">UpdateEnergyUsageInterfaceUsagesInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeEnergyUsageInterface</strong></td>
<td valign="top"><a href="#removeenergyusageinterfacepayload">RemoveEnergyUsageInterfacePayload</a>!</td>
<td>

Removes an energy usage interface.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#removeenergyusageinterfaceinput">RemoveEnergyUsageInterfaceInput</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">input</td>
<td valign="top"><a href="#setgroupenergyusageinterfacerelationinput">SetGroupEnergyUsageInterfaceRelationInput</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Objects

### AddAlarmCodeEventPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>event</strong></td>
<td valign="top"><a href="#alarmcodeevent">AlarmCodeEvent</a>!</td>
<td>

The AlarmCodeEvent Object

</td>
</tr>
</tbody>
</table>

### AddAlarmLevelPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>alarmLevel</strong></td>
<td valign="top"><a href="#alarmlevel">AlarmLevel</a>!</td>
<td>

The AlarmLevel Object

</td>
</tr>
</tbody>
</table>

### AddClientPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>client</strong></td>
<td valign="top"><a href="#client">Client</a>!</td>
<td>

The Client Object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>apiKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Api Key of the Client.

</td>
</tr>
</tbody>
</table>

### AddCodeRulePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>rule</strong></td>
<td valign="top"><a href="#coderule">CodeRule</a>!</td>
<td>

The CodeRule Object

</td>
</tr>
</tbody>
</table>

### AddEhsUsagePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>usage</strong></td>
<td valign="top"><a href="#ehsusage">EhsUsage</a>!</td>
<td>

The usage object.

</td>
</tr>
</tbody>
</table>

### AddEnergyDeviceKindPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyDeviceKind</strong></td>
<td valign="top"><a href="#energydevicekind">EnergyDeviceKind</a>!</td>
<td>

The energy device kind object.

</td>
</tr>
</tbody>
</table>

### AddEnergyUsageInterfacePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>interface</strong></td>
<td valign="top"><a href="#energyusageinterface">EnergyUsageInterface</a>!</td>
<td>

The energy usage interface.

</td>
</tr>
</tbody>
</table>

### AddEnergyUsagePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyUsage</strong></td>
<td valign="top"><a href="#energyusage">EnergyUsage</a>!</td>
<td>

The energy usage object.

</td>
</tr>
</tbody>
</table>

### AddFormulasPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>formulas</strong></td>
<td valign="top">[<a href="#formula">Formula</a>!]!</td>
<td>

The formulas.

</td>
</tr>
</tbody>
</table>

### AddGatewayPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>gateway</strong></td>
<td valign="top"><a href="#gateway">Gateway</a>!</td>
<td>

The Gateway Object

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Password of the Gateway to Login MQTT Server

</td>
</tr>
</tbody>
</table>

### AddGroupPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#group">Group</a>!</td>
<td>

The group object.

</td>
</tr>
</tbody>
</table>

### AddHighLowEventPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>event</strong></td>
<td valign="top"><a href="#highlowevent">HighLowEvent</a>!</td>
<td>

The HighLowEvent Object

</td>
</tr>
</tbody>
</table>

### AddIAppGroupGroupPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iAppGroup</strong></td>
<td valign="top"><a href="#iappgroup">IAppGroup</a>!</td>
<td>

The I.App Group object.

</td>
</tr>
</tbody>
</table>

### AddIAppMenuItemPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iAppMenuItem</strong></td>
<td valign="top"><a href="#iappmenuitem">IAppMenuItem</a>!</td>
<td>

The I.App menu item object.

</td>
</tr>
</tbody>
</table>

### AddIAppPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iApp</strong></td>
<td valign="top"><a href="#iapp">IApp</a>!</td>
<td>

The I.App object.

</td>
</tr>
</tbody>
</table>

### AddKpiMetricPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kpiMetric</strong></td>
<td valign="top"><a href="#kpimetric">KpiMetric</a>!</td>
<td>

The kpi metric object.

</td>
</tr>
</tbody>
</table>

### AddKpiParameterUsagePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kpiParameterUsage</strong></td>
<td valign="top"><a href="#kpiparameterusage">KpiParameterUsage</a>!</td>
<td>

The kpi parameter usage object.

</td>
</tr>
</tbody>
</table>

### AddKpiParameterUsageRelationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kpiParameterUsageRelation</strong></td>
<td valign="top"><a href="#kpiparameterusagerelation">KpiParameterUsageRelation</a>!</td>
<td>

The kpi parameter usage relation object.

</td>
</tr>
</tbody>
</table>

### AddLicensePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>license</strong></td>
<td valign="top"><a href="#license">License</a>!</td>
<td>

The license object.

</td>
</tr>
</tbody>
</table>

### AddMachinePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#machine">Machine</a>!</td>
<td>

The machine object.

</td>
</tr>
</tbody>
</table>

### AddMachineStatusPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machineStatus</strong></td>
<td valign="top"><a href="#machinestatus">MachineStatus</a>!</td>
<td>

The machine status object.

</td>
</tr>
</tbody>
</table>

### AddMachineTypeCategoryPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machineTypeCategory</strong></td>
<td valign="top"><a href="#machinetypecategory">MachineTypeCategory</a>!</td>
<td>

The machine type category object.

</td>
</tr>
</tbody>
</table>

### AddMachineTypePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machineType</strong></td>
<td valign="top"><a href="#machinetype">MachineType</a>!</td>
<td>

The machine type object.

</td>
</tr>
</tbody>
</table>

### AddMarqueeActionPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>action</strong></td>
<td valign="top"><a href="#marqueeaction">MarqueeAction</a>!</td>
<td>

The MarqueeAction Object

</td>
</tr>
</tbody>
</table>

### AddNotificationActionPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>action</strong></td>
<td valign="top"><a href="#notificationaction">NotificationAction</a>!</td>
<td>

The NotificationAction Object

</td>
</tr>
</tbody>
</table>

### AddOutboundPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>outbound</strong></td>
<td valign="top"><a href="#outbound">Outbound</a>!</td>
<td>

The Outbound object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>error</strong></td>
<td valign="top"><a href="#jsonobject">JSONObject</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddParameterInterfacePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>interface</strong></td>
<td valign="top"><a href="#parameterinterface">ParameterInterface</a>!</td>
<td>

The parameter interface.

</td>
</tr>
</tbody>
</table>

### AddParameterMappingCodesPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>mappingCodes</strong></td>
<td valign="top">[<a href="#parametermappingcode">ParameterMappingCode</a>!]!</td>
<td>

The parameter mapping codes.

</td>
</tr>
</tbody>
</table>

### AddParameterMappingRulePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>mappingRule</strong></td>
<td valign="top"><a href="#parametermappingrule">ParameterMappingRule</a>!</td>
<td>

The parameter mapping rule.

</td>
</tr>
</tbody>
</table>

### AddParameterPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a>!</td>
<td>

The parameter object.

</td>
</tr>
</tbody>
</table>

### AddProfileMachinePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>profileMachine</strong></td>
<td valign="top"><a href="#profilemachine">ProfileMachine</a>!</td>
<td>

The profile machine object.

</td>
</tr>
</tbody>
</table>

### AddProfileParameterPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>profileParameter</strong></td>
<td valign="top"><a href="#profileparameter">ProfileParameter</a>!</td>
<td>

The profile parameter object.

</td>
</tr>
</tbody>
</table>

### AddRolePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#role">Role</a>!</td>
<td>

The Role object.

</td>
</tr>
</tbody>
</table>

### AddThresholdRulePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>rule</strong></td>
<td valign="top"><a href="#thresholdrule">ThresholdRule</a>!</td>
<td>

The ThresholdRule Object

</td>
</tr>
</tbody>
</table>

### AddTimeOfUsePeriodPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>timeOfUsePeriod</strong></td>
<td valign="top"><a href="#timeofuseperiod">TimeOfUsePeriod</a>!</td>
<td>

The Time-of-Use Period object.

</td>
</tr>
</tbody>
</table>

### AddTranslationLangPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>translationLang</strong></td>
<td valign="top"><a href="#translationlang">TranslationLang</a>!</td>
<td>

The translation lang object.

</td>
</tr>
</tbody>
</table>

### AddUserPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a>!</td>
<td>

The User Object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Password of the User.

</td>
</tr>
</tbody>
</table>

### AgentInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>version</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>online</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastInfoTime</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
</tbody>
</table>

### AlarmCodeEvent

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Name of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Description of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Machine of the Parameter of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Parameter of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#eventkind">EventKind</a>!</td>
<td>

The Kind of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The event created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The event updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codeRules</strong></td>
<td valign="top">[<a href="#coderule">CodeRule</a>!]!</td>
<td>

The Code Rules of the Alarm Code Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastLog</strong></td>
<td valign="top"><a href="#alarmcodeeventlog">AlarmCodeEventLog</a></td>
<td>

Last Alarm Code Event Log, If It is Not Over

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>logsInRange</strong></td>
<td valign="top">[<a href="#alarmcodeeventlog">AlarmCodeEventLog</a>!]!</td>
<td>

Alarm Code Event Logs in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">alarmLevelLevels</td>
<td valign="top">[<a href="#int">Int</a>!]</td>
<td>

If this argument is set and not an empty array, logs will be filtered by alarmLevels.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>logCountInRange</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Number of Alarm Code Event Logs in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">alarmLevelLevels</td>
<td valign="top">[<a href="#int">Int</a>!]</td>
<td>

If this argument is set and not an empty array, logs will be filtered by alarmLevels.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>logElapsedInRange</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Total Elapsed Time (ms) of Alarm Code Event Logs in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">alarmLevelLevels</td>
<td valign="top">[<a href="#int">Int</a>!]</td>
<td>

If this argument is set and not an empty array, logs will be filtered by alarmLevels.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>logCountAndElapsedByAlarmLevelInRange</strong></td>
<td valign="top">[<a href="#alarmcodeeventlogcountandelapsedbyalarmlevelitem">AlarmCodeEventLogCountAndElapsedByAlarmLevelItem</a>!]!</td>
<td>

The Number of Alarm Code Event Logs and Total Elapsed Time (ms) by AlarmLevel in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">alarmLevelLevels</td>
<td valign="top">[<a href="#int">Int</a>!]</td>
<td>

If this argument is set and not an empty array, logs will be filtered by alarmLevels.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>logAnalysisInRange</strong></td>
<td valign="top">[<a href="#alarmcodeeventloganalysisitem">AlarmCodeEventLogAnalysisItem</a>!]!</td>
<td>

Analysis of Alarm Code Event Logs in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">alarmLevelLevels</td>
<td valign="top">[<a href="#int">Int</a>!]</td>
<td>

If this argument is set and not an empty array, logs will be filtered by alarmLevels.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a></td>
<td>

The Parameter of the Event

</td>
</tr>
</tbody>
</table>

### AlarmCodeEventLog

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelLevel</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codeValue</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>valueType</strong></td>
<td valign="top"><a href="#tagvaluetype">TagValueType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codeMessage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>elapsed</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
</tbody>
</table>

### AlarmCodeEventLogAnalysisItem

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codeMessage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>count</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalElapsed</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
</tbody>
</table>

### AlarmCodeEventLogCountAndElapsedByAlarmLevelItem

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelLevel</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>count</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalElapsed</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
</tbody>
</table>

### AlarmLevel

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Name of the AlarmLevel

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>level</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Priority of the AlarmLevel, Larger Means Higher

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>color</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Color of the AlarmLevel

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether the AlarmLevel is Default

</td>
</tr>
</tbody>
</table>

### ApiInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Constant string ID.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>l3Mode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>l3ModeRtm</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ifpProduct</strong></td>
<td valign="top"><a href="#ifpproduct">IfpProduct</a>!</td>
<td>

iFactory Product Information, presented to activate features.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>organizer</strong></td>
<td valign="top"><a href="#organizerapiinfo">OrganizerApiInfo</a>!</td>
<td>

Organizer API Information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>calculator</strong></td>
<td valign="top"><a href="#calculatorapiinfo">CalculatorApiInfo</a>!</td>
<td>

Calculator API Information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dataHub</strong></td>
<td valign="top"><a href="#datahubapiinfo">DataHubApiInfo</a>!</td>
<td>

DataHub API Information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ehs</strong></td>
<td valign="top"><a href="#ehsapiinfo">EhsApiInfo</a>!</td>
<td>

EHS API Information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ruleEngine</strong></td>
<td valign="top"><a href="#ruleengineapiinfo">RuleEngineApiInfo</a>!</td>
<td>

RuleEngine API Information.

</td>
</tr>
</tbody>
</table>

### AppIFactory

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>OEE</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>EHS</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>EAN</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ProductionKPI</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>SPC</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Predict</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Andon</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ShopFloor</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Maintenance</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>DWI</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>eManual</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### AppMaxnerva

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>MES</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>QMS</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ESOP</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>portalMES</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>portalQMS</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>portalESOP</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### ApplyParameterMappingToParametersPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>mappingRule</strong></td>
<td valign="top"><a href="#parametermappingrule">ParameterMappingRule</a>!</td>
<td>

The parameter mapping rule.

</td>
</tr>
</tbody>
</table>

### AssociateInboundGroupPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#group">Group</a>!</td>
<td>

The group object.

</td>
</tr>
</tbody>
</table>

### AuthPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

The Signed In User.

</td>
</tr>
</tbody>
</table>

### Author

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>url</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### BadTagValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>savedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deviceId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### BulkSetGenericKpiListPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>message</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### CalculateFormulaPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>result</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isFinite</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulaText</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>resultText</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>variables</strong></td>
<td valign="top"><a href="#jsonobject">JSONObject</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### CalculationPoint

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Calculation Parameter id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulaId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Calculation Formula id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operands</strong></td>
<td valign="top">[<a href="#calculationpointoperand">CalculationPointOperand</a>!]!</td>
<td>

The Calculation operands.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schedule</strong></td>
<td valign="top"><a href="#pointschedule">PointSchedule</a>!</td>
<td>

The schedule to run calculation.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dailyHour</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td>

The hour to run with daily schedule, 0 ~ 23.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>initialValue</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The point created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The point updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formula</strong></td>
<td valign="top"><a href="#formula">Formula</a></td>
<td>

The formula

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulasByRoot</strong></td>
<td valign="top">[<a href="#formula">Formula</a>!]</td>
<td>

A list of formulas by root.

</td>
</tr>
</tbody>
</table>

### CalculationPointOperand

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>variable</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceParameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The source Parameter id to calculate.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceParameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a></td>
<td>

The source parameter.

</td>
</tr>
</tbody>
</table>

### CalculationSetting

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>function</strong></td>
<td valign="top"><a href="#calculationsettingfunction">CalculationSettingFunction</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### CalculatorApiInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Constant string ID.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App short name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App description.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>version</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App version.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>author</strong></td>
<td valign="top"><a href="#author">Author</a>!</td>
<td>

The App author.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mongoVersion</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The MongoDB version.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td>

The App server time.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isEnSaaS</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that is running on WISE-PaaS/EnSaaS 4.0.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isIfpApp</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that query is performed by iFactory+ App.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ifpAppSecretReady</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that iFactory+ App Secret is ready.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sid</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Service Register metadata sid.

</td>
</tr>
</tbody>
</table>

### ChangeMyPasswordPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a>!</td>
<td>

The User Object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Password of the User.

</td>
</tr>
</tbody>
</table>

### Client

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Client name for display.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The description of Client.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roles</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

The roles key this Client belongs to.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roleDetails</strong></td>
<td valign="top">[<a href="#role">Role</a>!]!</td>
<td>

The roles this Client belongs to.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>active</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Only active Client can be authenticated.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>orgAcls</strong></td>
<td valign="top">[<a href="#orgacl">OrgAcl</a>!]!</td>
<td>

Authorized Organizer ACL entries.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Client created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Client updated at.

</td>
</tr>
</tbody>
</table>

### CodeRule

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>event</strong></td>
<td valign="top"><a href="#alarmcodeevent">AlarmCodeEvent</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmLevel</strong></td>
<td valign="top"><a href="#alarmlevel">AlarmLevel</a>!</td>
<td>

AlarmLevel that this Rule Related to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>actions</strong></td>
<td valign="top">[<a href="#action">Action</a>!]!</td>
<td>

The Actions of the Rule

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codeId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Code Node ID of the Code Rule

</td>
</tr>
</tbody>
</table>

### ConnectionStatus

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ts</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### Coordinate

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>longitude</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>latitude</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### CountryCallingCodeInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>codes</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Country calling codes

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>country</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Country name

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isoCode2</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Country ISO code2

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isoCode3</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Country ISO code3

</td>
</tr>
</tbody>
</table>

### CreateFirstAdminPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a>!</td>
<td>

The User Object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Password of the User.

</td>
</tr>
</tbody>
</table>

### CurrencyInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>symbol</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### DHAcl

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Role about this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scope</strong></td>
<td valign="top"><a href="#dhaclscope">DHAclScope</a>!</td>
<td>

The scope of this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mode</strong></td>
<td valign="top"><a href="#aclmode">AclMode</a>!</td>
<td>

The mode of this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The ACL created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The ACL updated at.

</td>
</tr>
</tbody>
</table>

### DHAclEntry

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Role about this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scope</strong></td>
<td valign="top"><a href="#dhaclscope">DHAclScope</a>!</td>
<td>

The scope of this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mode</strong></td>
<td valign="top"><a href="#aclmode">AclMode</a>!</td>
<td>

The mode of this ACL.

</td>
</tr>
</tbody>
</table>

### DataHubApiInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Constant string ID.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App short name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App description.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>version</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App version.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>author</strong></td>
<td valign="top"><a href="#author">Author</a>!</td>
<td>

The App author.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mongoVersion</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The MongoDB version.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>influxInfo</strong></td>
<td valign="top">[<a href="#influxinfo">InfluxInfo</a>!]!</td>
<td>

The InfluxDB Information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td>

The App server time.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isEnSaaS</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that is running on WISE-PaaS/EnSaaS 4.0.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isIfpApp</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that query is performed by iFactory+ App.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ifpAppSecretReady</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that iFactory+ App Secret is ready.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sid</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Service Register metadata sid.

</td>
</tr>
</tbody>
</table>

### DataHubAppSetting

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>statusExpirationPeriod</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Gateway Current Status Expiration Period in Seconds

</td>
</tr>
</tbody>
</table>

### DeleteValuesInRangePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>success</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### DeviceStatus

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>event</strong></td>
<td valign="top"><a href="#devicestatusevent">DeviceStatusEvent</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>savedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
</tbody>
</table>

### DisableAlertsPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### DownSampledModbusValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>str</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### DownSampledOpcUaValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>str</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### DownSampledTagValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>str</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingCodeId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the parameter mapping code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingCode</strong></td>
<td valign="top"><a href="#parametermappingcode">ParameterMappingCode</a></td>
<td>

The parameter mapping code.

</td>
</tr>
</tbody>
</table>

### DryRunTransformationPointPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#pointvalue">PointValue</a></td>
<td>

The Transformation Point dry run value.

</td>
</tr>
</tbody>
</table>

### EhsApiInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Constant string ID.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App short name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App description.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>version</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App version.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>author</strong></td>
<td valign="top"><a href="#author">Author</a>!</td>
<td>

The App author.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mongoVersion</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The MongoDB version.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td>

The App server time.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isEnSaaS</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that is running on WISE-PaaS/EnSaaS 4.0.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isIfpApp</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that query is performed by iFactory+ App.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ifpAppSecretReady</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that iFactory+ App Secret is ready.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sid</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Service Register metadata sid.

</td>
</tr>
</tbody>
</table>

### EhsTranslation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EhsUsage

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usageType</strong></td>
<td valign="top"><a href="#ehsusagetype">EhsUsageType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The parameter scada id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The parameter tag id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The usage created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The usage updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#machine">Machine</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a></td>
<td></td>
</tr>
</tbody>
</table>

### ElectricityRateContract

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>country</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The country of Electricity Rate Contract.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>province</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The province of Electricity Rate Contract.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#electricityratekind">ElectricityRateKind</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>classification</strong></td>
<td valign="top"><a href="#electricityrateclassification">ElectricityRateClassification</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startTime</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td>

The time of Electricity Rate Contract.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#jsonobject">JSONObject</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Energy Device created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Energy Device updated at.

</td>
</tr>
</tbody>
</table>

### ElectricityRateContractedCapacity

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>values</strong></td>
<td valign="top">[<a href="#float">Float</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>plans</strong></td>
<td valign="top">[<a href="#float">Float</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isCurrent</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isRecommended</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
</tbody>
</table>

### ElectricityRateContractedCapacityPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#electricityratecontractedcapacitystatus">ElectricityRateContractedCapacityStatus</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>list</strong></td>
<td valign="top">[<a href="#electricityratecontractedcapacity">ElectricityRateContractedCapacity</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### ElectricityRatePriceComparison

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#electricityratekind">ElectricityRateKind</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>list</strong></td>
<td valign="top">[<a href="#electricityratepricecomparisondata">ElectricityRatePriceComparisonData</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### ElectricityRatePriceComparisonData

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>demandCharge</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyCharge</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
</tbody>
</table>

### ElectricityRatePriceComparisonPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>list</strong></td>
<td valign="top">[<a href="#electricityratepricecomparison">ElectricityRatePriceComparison</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### EnableAlertsPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>alerts</strong></td>
<td valign="top">[<a href="#generickpialert">GenericKpiAlert</a>!]!</td>
<td>

List enabled Generic KPI Alerts.

</td>
</tr>
</tbody>
</table>

### EnergyAlertTotalCount

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>count</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyAlertTotalCountListItem

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>count</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alertType</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyAlertTotalListItem

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupNodeId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alertType</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>standardValue</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>actualValue</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>logTime</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyAlertTypeCountListItem

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>count</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alertType</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyAlertValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#energyalerttotalcount">EnergyAlertTotalCount</a>!</td>
<td>

The Energy Alert Total Count.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">currentGroupOnly</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCountList</strong></td>
<td valign="top">[<a href="#energyalerttotalcountlistitem">EnergyAlertTotalCountListItem</a>!]!</td>
<td>

The Energy Alert Total Count List.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalList</strong></td>
<td valign="top">[<a href="#energyalerttotallistitem">EnergyAlertTotalListItem</a>!]!</td>
<td>

The Energy Alert Total List.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alertTypeCountList</strong></td>
<td valign="top">[<a href="#energyalerttypecountlistitem">EnergyAlertTypeCountListItem</a>!]!</td>
<td>

The Energy Alert Type Count List.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyConnectAnalysis

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>count</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyConnectInformation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineDescription</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyConnectMachineValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#energyconnectstatus">EnergyConnectStatus</a>!</td>
<td>

The Energy Device Connect Status.

</td>
</tr>
</tbody>
</table>

### EnergyConnectStatus

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyConnectValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

The Energy Total Count.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusCount</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

The Energy Link Status Count.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">statusType</td>
<td valign="top"><a href="#connectstatus">ConnectStatus</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top">[<a href="#energyconnectstatus">EnergyConnectStatus</a>!]!</td>
<td>

The Energy Device Connect Status.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">statusType</td>
<td valign="top"><a href="#connectstatus">ConnectStatus</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>information</strong></td>
<td valign="top">[<a href="#energyconnectinformation">EnergyConnectInformation</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>analysis</strong></td>
<td valign="top">[<a href="#energyconnectanalysis">EnergyConnectAnalysis</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>detail</strong></td>
<td valign="top">[<a href="#energydisconnecttime">EnergyDisconnectTime</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### EnergyConsumptionValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>lastValue</strong></td>
<td valign="top"><a href="#energylastvalue">EnergyLastValue</a>!</td>
<td>

The Consumption last value.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>currentValue</strong></td>
<td valign="top"><a href="#energylastvalue">EnergyLastValue</a>!</td>
<td>

The Consumption current value.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">dayOrMonth</td>
<td valign="top"><a href="#dayormonth">DayOrMonth</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalValue</strong></td>
<td valign="top">[<a href="#energylastvalue">EnergyLastValue</a>!]!</td>
<td>

The Consumption total value.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeType</td>
<td valign="top"><a href="#timetype">TimeType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalList</strong></td>
<td valign="top">[<a href="#energylastvalue">EnergyLastValue</a>!]!</td>
<td>

The Consumption total list value.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeType</td>
<td valign="top"><a href="#timetype">TimeType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeShift</td>
<td valign="top"><a href="#timeshift">TimeShift</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineList</strong></td>
<td valign="top">[<a href="#energymachinetotallistvalue">EnergyMachineTotalListValue</a>!]!</td>
<td>

The Consumption machine total lists value.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeShift</td>
<td valign="top"><a href="#timeshift">TimeShift</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeType</td>
<td valign="top"><a href="#timetype">TimeType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>listByUsage</strong></td>
<td valign="top">[<a href="#energylastlistbyusage">EnergyLastListByUsage</a>!]!</td>
<td>

The Consumption list by usage value.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeType</td>
<td valign="top"><a href="#timetype">TimeType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalListByPeak</strong></td>
<td valign="top">[<a href="#energylastvaluewithpeak">EnergyLastValueWithPeak</a>!]!</td>
<td>

The Consumption total list value by period type.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeType</td>
<td valign="top"><a href="#timetype">TimeType</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyDemandMachineValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>totalList</strong></td>
<td valign="top">[<a href="#energylastvalue">EnergyLastValue</a>!]!</td>
<td>

The Demand total list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeType</td>
<td valign="top"><a href="#timetype">TimeType</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyDemandValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>lastValue</strong></td>
<td valign="top"><a href="#energylastvalue">EnergyLastValue</a>!</td>
<td>

The Demand last Value.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastRate</strong></td>
<td valign="top"><a href="#energylastrate">EnergyLastRate</a>!</td>
<td>

The Demand last Rate.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalValue</strong></td>
<td valign="top">[<a href="#energytotalvalue">EnergyTotalValue</a>!]!</td>
<td>

The Demand total value.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeType</td>
<td valign="top"><a href="#timetype">TimeType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalList</strong></td>
<td valign="top">[<a href="#energylastvalue">EnergyLastValue</a>!]!</td>
<td>

The Demand total list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeType</td>
<td valign="top"><a href="#timetype">TimeType</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyDevice

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The kind of Energy Device.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The usage of Energy Device.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isExclude</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

The Energy Device is excluded for its group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Energy Device created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Energy Device updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#machine">Machine</a></td>
<td></td>
</tr>
</tbody>
</table>

### EnergyDeviceKind

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The key of energy device kind.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The name of energy device kind.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#ehstranslation">EhsTranslation</a>!]!</td>
<td>

The energy device kind name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether the energy device kind is default.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usages</strong></td>
<td valign="top">[<a href="#energyusage">EnergyUsage</a>!]!</td>
<td>

Usages of energy device kind.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>relationKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The relation key of energy device kind (for Generic KPI).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The energy device kind created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The energy device kind updated at.

</td>
</tr>
</tbody>
</table>

### EnergyDisconnectTime

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyKpi

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The kind of Energy KPI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandJan</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandFeb</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandMar</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandApr</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandMay</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandJun</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandJul</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandAug</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandSep</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandOct</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandNov</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandDec</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionJan</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionFeb</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionMar</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionApr</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionMay</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionJun</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionJul</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionAug</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionSep</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionOct</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionNov</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionDec</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandJan</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandFeb</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandMar</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandApr</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandMay</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandJun</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandJul</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandAug</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandSep</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandOct</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandNov</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandDec</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionJan</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionFeb</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionMar</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionApr</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionMay</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionJun</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionJul</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionAug</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionSep</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionOct</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionNov</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionDec</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Energy KPI created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Energy KPI updated at.

</td>
</tr>
</tbody>
</table>

### EnergyKpiAlert

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The kind of Energy KPI Alert.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The usage of Energy KPI Alert.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpi</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

The KPI value.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpiKind</strong></td>
<td valign="top"><a href="#generickpialertkind">GenericKpiAlertKind</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

The value.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>logTime</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td>

The log time.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Energy KPI Alert created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Energy KPI Alert updated at.

</td>
</tr>
</tbody>
</table>

### EnergyKpiAlertMachineValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>count</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>list</strong></td>
<td valign="top">[<a href="#energykpialert">EnergyKpiAlert</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyKpiAlertValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>count</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>list</strong></td>
<td valign="top">[<a href="#energykpialert">EnergyKpiAlert</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyKpiDataOfUsage

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>consumption</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpi</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alertCount</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
</tbody>
</table>

### EnergyKpiReportByUsage

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dataOfUsages</strong></td>
<td valign="top">[<a href="#energykpidataofusage">EnergyKpiDataOfUsage</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyKpiSavingRate

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>achievingRate</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
</tbody>
</table>

### EnergyKpiSavingRateValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>rate</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>list</strong></td>
<td valign="top">[<a href="#energykpisavingrate">EnergyKpiSavingRate</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>monthlyReport</strong></td>
<td valign="top">[<a href="#energykpisavingreportbyusage">EnergyKpiSavingReportByUsage</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">index</td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### EnergyKpiSavingReport

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>savingRate</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pastValue</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>currentValue</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpi</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
</tbody>
</table>

### EnergyKpiSavingReportByUsage

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reports</strong></td>
<td valign="top">[<a href="#energykpisavingreport">EnergyKpiSavingReport</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyLastListByUsage

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kindUsage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyLastRate

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>rate</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyLastValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyLastValueWithPeak

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>periodType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### EnergyMachineTotalListValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalList</strong></td>
<td valign="top">[<a href="#energylastvalue">EnergyLastValue</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyMachineValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyConnect</strong></td>
<td valign="top"><a href="#energyconnectmachinevalue">EnergyConnectMachineValue</a>!</td>
<td>

The Energy Device Connect Status.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyConsumption</strong></td>
<td valign="top"><a href="#energyconsumptionvalue">EnergyConsumptionValue</a>!</td>
<td>

The Energy Consumption.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">energyDeviceKind</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usages</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

The device kind usage at.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isPrediction</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, use prediction data.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyKpiAlert</strong></td>
<td valign="top"><a href="#energykpialertmachinevalue">EnergyKpiAlertMachineValue</a>!</td>
<td>

The Energy KPI Alert Data.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">energyDeviceKind</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usages</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

The device kind usage at.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kpiKind</td>
<td valign="top"><a href="#generickpialertkind">GenericKpiAlertKind</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isPrediction</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, use prediction data.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyDemand</strong></td>
<td valign="top"><a href="#energydemandmachinevalue">EnergyDemandMachineValue</a>!</td>
<td>

The Energy Demand.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">energyDeviceKind</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usages</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

The device kind usage at.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isPrediction</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, use prediction data.

</td>
</tr>
</tbody>
</table>

### EnergyPeakValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>totalListSumByPeak</strong></td>
<td valign="top">[<a href="#energytotallistitembypeak">EnergyTotalListItemByPeak</a>!]!</td>
<td>

The Consumption list by Peak value.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeType</td>
<td valign="top"><a href="#timetype">TimeType</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyShareTimelineSum

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sum</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

Summation of Energy Share Ratio.

</td>
</tr>
</tbody>
</table>

### EnergyShareUsageSum

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The kind of Energy Share Ratio.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sum</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

Summation of Energy Share Ratio.

</td>
</tr>
</tbody>
</table>

### EnergyShareUsageTimeline

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The kind of Energy Share Ratio.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeline</strong></td>
<td valign="top">[<a href="#energysharetimelinesum">EnergyShareTimelineSum</a>!]!</td>
<td>

Summation of Energy Share Ratio in timeline.

</td>
</tr>
</tbody>
</table>

### EnergyShareValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>sum</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

Summation of Energy Share (Usages are ignored).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sumByUsages</strong></td>
<td valign="top">[<a href="#energyshareusagesum">EnergyShareUsageSum</a>!]!</td>
<td>

Summation of Energy Share Ratio that group by usage.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeline</strong></td>
<td valign="top">[<a href="#energysharetimelinesum">EnergyShareTimelineSum</a>!]!</td>
<td>

Summation of Energy Share Ratio in timeline.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timelineByUsages</strong></td>
<td valign="top">[<a href="#energyshareusagetimeline">EnergyShareUsageTimeline</a>!]!</td>
<td>

Summation of Energy Share Ratio in timeline that group by usage.

</td>
</tr>
</tbody>
</table>

### EnergySplit

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The kind of Energy Split.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulaId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Split Formula id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operands</strong></td>
<td valign="top">[<a href="#energysplitoperand">EnergySplitOperand</a>!]!</td>
<td>

The Split operands.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Split created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Split updated at.

</td>
</tr>
</tbody>
</table>

### EnergySplitOperand

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>variable</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ratio</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Machine id to calculate Split.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usage</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#machine">Machine</a></td>
<td></td>
</tr>
</tbody>
</table>

### EnergyTotalListItemByPeak

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>periodTypeInfo</strong></td>
<td valign="top"><a href="#periodtypeinfo">PeriodTypeInfo</a>!</td>
<td>

PeriodType that this Time-of-Use period Related to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>peakValueData</strong></td>
<td valign="top">[<a href="#peakvaluedata">PeakValueData</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyTotalValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxPower</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### EnergyUsage

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The parent key of energy usage.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The key of energy usage.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The name of energy usage.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#ehstranslation">EhsTranslation</a>!]!</td>
<td>

The energy usage name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usageType</strong></td>
<td valign="top"><a href="#energyusagetype">EnergyUsageType</a>!</td>
<td>

The type of energy usage.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether the energy usage is default.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>relationKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The relation key of energy usage (for Generic KPI).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The energy usage created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The energy usage updated at.

</td>
</tr>
</tbody>
</table>

### EnergyUsageInterface

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The energy usage interface name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usages</strong></td>
<td valign="top">[<a href="#energyusageinterfaceusage">EnergyUsageInterfaceUsage</a>!]!</td>
<td>

The energy usages of the energy usage interface.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The energy usage interface created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The energy usage interface updated at.

</td>
</tr>
</tbody>
</table>

### EnergyUsageInterfaceUsage

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyUsage</strong></td>
<td valign="top"><a href="#energyusage">EnergyUsage</a></td>
<td></td>
</tr>
</tbody>
</table>

### EnergyValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyDemand</strong></td>
<td valign="top"><a href="#energydemandvalue">EnergyDemandValue</a>!</td>
<td>

The Energy Demand.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">energyDeviceKind</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usages</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

The device kind usage at.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isPrediction</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, use prediction data.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyConsumption</strong></td>
<td valign="top"><a href="#energyconsumptionvalue">EnergyConsumptionValue</a>!</td>
<td>

The Energy Consumption.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">energyDeviceKind</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usages</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

The device kind usage at.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isPrediction</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, use prediction data.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyAlert</strong></td>
<td valign="top"><a href="#energyalertvalue">EnergyAlertValue</a>!</td>
<td>

The Energy Alert Data.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">energyAlertTypes</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isPrediction</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, use prediction data.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyPeak</strong></td>
<td valign="top"><a href="#energypeakvalue">EnergyPeakValue</a>!</td>
<td>

The Energy Peak Data.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">energyDeviceKind</td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The device kind at.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usages</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

The device kind usage at.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isPrediction</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, use prediction data.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyConnect</strong></td>
<td valign="top"><a href="#energyconnectvalue">EnergyConnectValue</a>!</td>
<td>

The Energy Device Connect Status.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">energyDeviceKind</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usages</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

The device kind usage at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyKpiAlert</strong></td>
<td valign="top"><a href="#energykpialertvalue">EnergyKpiAlertValue</a>!</td>
<td>

The Energy KPI Alert Data.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">energyDeviceKind</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usages</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

The device kind usage at.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kpiKind</td>
<td valign="top"><a href="#generickpialertkind">GenericKpiAlertKind</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isPrediction</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, use prediction data.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>savingRate</strong></td>
<td valign="top"><a href="#energykpisavingratevalue">EnergyKpiSavingRateValue</a>!</td>
<td>

The Energy KPI Saving Rate Data.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">energyDeviceKind</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reportByUsage</strong></td>
<td valign="top">[<a href="#energykpireportbyusage">EnergyKpiReportByUsage</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">index</td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### EventConnection

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>nodes</strong></td>
<td valign="top">[<a href="#event">Event</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### Formula

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isRoot</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

The formula is root.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rootFormulaId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the formula root formula.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_rootFormulaId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the formula root formula.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rootFormula</strong></td>
<td valign="top"><a href="#formula">Formula</a></td>
<td>

The formula root formula.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operator</strong></td>
<td valign="top"><a href="#operator">Operator</a>!</td>
<td>

The formula operator.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandLKind</strong></td>
<td valign="top"><a href="#operandkind">OperandKind</a>!</td>
<td>

The formula left operand kind.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandLConstant</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

The formula left operand constant.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandLFormulaId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the formula left operand formula.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_operandLFormulaId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the formula left operand formula.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandLFormula</strong></td>
<td valign="top"><a href="#formula">Formula</a></td>
<td>

The formula left operand formula.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandLVar</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The formula left operand variable.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandLVars</strong></td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

The formula left operand variables.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandRKind</strong></td>
<td valign="top"><a href="#operandkind">OperandKind</a>!</td>
<td>

The formula right operand kind.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandRConstant</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

The formula right operand constant.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandRFormulaId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the formula right operand formula.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_operandRFormulaId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the formula right operand formula.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandRFormula</strong></td>
<td valign="top"><a href="#formula">Formula</a></td>
<td>

The formula right operand formula.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandRVar</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The formula right operand variable.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandRVars</strong></td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

The formula right operand variables.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

The formula is a preset default, which could not be removed.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>detail</strong></td>
<td valign="top"><a href="#formuladetail">FormulaDetail</a></td>
<td></td>
</tr>
</tbody>
</table>

### FormulaDetail

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>formulaText</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>variableList</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### FormulasList

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>formulas</strong></td>
<td valign="top">[<a href="#formula">Formula</a>!]!</td>
<td>

The formulas.

</td>
</tr>
</tbody>
</table>

### FullOutboundIfpCfgPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>coolDownCountdown</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Time to wait before next call.

</td>
</tr>
</tbody>
</table>

### Gateway

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Nickname of the Gateway

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>username</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Username of the Gateway to Login MQTT Server

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Password of the Gateway to Login MQTT Server

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#gatewaykind">GatewayKind</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mqttHost</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The Host of MQTT Server (for Cloud)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mqttProtocols</strong></td>
<td valign="top">[<a href="#inboundprotocol">InboundProtocol</a>!]!</td>
<td>

The Protocols of MQTT Server

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>httpHost</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The Host of HTTP Inbound Server (for Cloud)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>httpProtocols</strong></td>
<td valign="top">[<a href="#inboundprotocol">InboundProtocol</a>!]!</td>
<td>

The Protocols of HTTP Inbound Server

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The ScadaId of the Gateway

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicLevel</strong></td>
<td valign="top"><a href="#topiclevel">TopicLevel</a>!</td>
<td>

The TopicLevel of the Gateway

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mqttTopic</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The MQTT Topic of the Gateway

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deviceId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The DeviceId of the Gateway

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastStatus</strong></td>
<td valign="top"><a href="#devicestatus">DeviceStatus</a></td>
<td>

Last Device Status

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusInRange</strong></td>
<td valign="top">[<a href="#devicestatus">DeviceStatus</a>!]!</td>
<td>

Device Status in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>currentStatus</strong></td>
<td valign="top"><a href="#devicestatus">DeviceStatus</a></td>
<td>

Device Current Status

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tags</strong></td>
<td valign="top">[<a href="#tag">Tag</a>!]!</td>
<td>

The Tags of the Gateway

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">searchTagId</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Fuzzy Search of TagId

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">valueTypes</td>
<td valign="top">[<a href="#tagvaluetype">TagValueType</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">tagIds</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tag</strong></td>
<td valign="top"><a href="#tag">Tag</a></td>
<td>

The TagId Specified Tag of the Gateway

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">tagId</td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

TagId to Find

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagCount</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

The Number of Tags of the Gateway

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Gateway Created at

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Gateway Updated at

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>associatedGroup</strong></td>
<td valign="top"><a href="#group">Group</a></td>
<td>

Associated Group of the Gateway

</td>
</tr>
</tbody>
</table>

### GenerateEHSKpiConfigPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>statusMessage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The status of generating EHS KPI configuration.

</td>
</tr>
</tbody>
</table>

### GenericKpi

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>relationKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Generic KPI relation key.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#generickpikind">GenericKpiKind</a>!</td>
<td>

The kind of Generic KPI, kind `Unlimited` year set to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>year</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

The year of Generic KPI, kind `Unlimited` year set to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>bind</strong></td>
<td valign="top"><a href="#generickpibind">GenericKpiBind</a>!</td>
<td>

The Bind of Generic KPI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hour</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td>

The hour of Generic KPI, only applicable to `Hour...` kinds.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpi</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Generic KPI created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Generic KPI updated at.

</td>
</tr>
</tbody>
</table>

### GenericKpiAlert

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>relationKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Generic KPI Alert relation key.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#generickpialertkind">GenericKpiAlertKind</a>!</td>
<td>

The kind of Generic KPI Alert, kind `Unlimited` year set to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>year</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

The year of Generic KPI Alert, kind `Unlimited` year set to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>bind</strong></td>
<td valign="top"><a href="#generickpibind">GenericKpiBind</a>!</td>
<td>

The Bind of Generic KPI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alert</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

The Generic KPI Alert is enabled.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Generic KPI Alert created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Generic KPI Alert updated at.

</td>
</tr>
</tbody>
</table>

### GenericKpiHoliday

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isoDate</strong></td>
<td valign="top"><a href="#date">Date</a>!</td>
<td>

The full date of Generic KPI Holiday.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>year</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

The year of Generic KPI Holiday.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>month</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

The month of Generic KPI Holiday, 0 ~ 11.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>date</strong></td>
<td valign="top"><a href="#positiveint">PositiveInt</a>!</td>
<td>

The date of Generic KPI Holiday, 1 ~ 31.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Generic KPI Holiday created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Generic KPI Holiday updated at.

</td>
</tr>
</tbody>
</table>

### Group

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The group name without translation.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The group name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The group name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group description.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>descriptions</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]</td>
<td>

The group description translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeZone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group timezone.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeZoneInfo</strong></td>
<td valign="top"><a href="#timezoneinfo">TimeZoneInfo</a></td>
<td>

The group timezone Information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the group parent.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_parentId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the group parent.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parent</strong></td>
<td valign="top"><a href="#group">Group</a></td>
<td>

The parent of the group

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ancestorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ancestors</strong></td>
<td valign="top">[<a href="#group">Group</a>!]!</td>
<td>

The ancestor groups of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>depth</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>children</strong></td>
<td valign="top">[<a href="#group">Group</a>!]!</td>
<td>

The sub-groups of that group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isTenant</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, list only Tenant Group; If false, list only non-Tenant Group; If null, list both.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>descendants</strong></td>
<td valign="top">[<a href="#group">Group</a>!]!</td>
<td>

The descendant groups of the group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isTenant</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If true, list only Tenant Group; If false, list only non-Tenant Group; If null, list both.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machines</strong></td>
<td valign="top">[<a href="#machine">Machine</a>!]</td>
<td>

The machines of the group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isStation</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Only list Station or only list Machine, omit it for Machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineCount</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

The number of the machines of the group and its children and descendants.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isStation</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Only list Station or only list Machine, omit it for Machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]</td>
<td>

The machines id list of the group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isStation</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Only list Station or only list Machine, omit it for Machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>coordinate</strong></td>
<td valign="top"><a href="#coordinate">Coordinate</a></td>
<td>

The longitude and latitude of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isTenant</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

This Group is a Tenant Group or not.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantQuotas</strong></td>
<td valign="top">[<a href="#tenantquota">TenantQuota</a>!]!</td>
<td>

List TenantQuota of this Tenant Group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">names</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filter TenantQuota list by names.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>owner</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group owner.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group phone.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phoneCountryCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group phone country code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#emailaddress">EmailAddress</a></td>
<td>

The group email.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group address.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>city</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group city.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>state</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group state.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>postalCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group postal code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group country code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gatewayId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The associated gateway id of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inboundAssociation</strong></td>
<td valign="top"><a href="#inboundassociation">InboundAssociation</a></td>
<td>

The inbound association of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterInterfaces</strong></td>
<td valign="top">[<a href="#parameterinterface">ParameterInterface</a>!]</td>
<td>

The parameter interfaces related to the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterInterfaceByIndex</strong></td>
<td valign="top"><a href="#parameterinterface">ParameterInterface</a></td>
<td>

The parameter interface related to the group by index.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">index</td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

index of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The group created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The group updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>productionKpiList</strong></td>
<td valign="top">[<a href="#productionkpi">ProductionKpi</a>!]</td>
<td>

The Production KPI of the group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">years</td>
<td valign="top">[<a href="#positiveint">PositiveInt</a>!]!</td>
<td>

Which years to list.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>genericKpiAlertList</strong></td>
<td valign="top">[<a href="#generickpialert">GenericKpiAlert</a>!]</td>
<td>

The Generic KPI Alert of the group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">relationKeys</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Which relationKeys to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">years</td>
<td valign="top">[<a href="#nonnegativeint">NonNegativeInt</a>!]!</td>
<td>

Which years to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kinds</td>
<td valign="top">[<a href="#generickpialertkind">GenericKpiAlertKind</a>!]!</td>
<td>

Which kinds to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>genericKpiList</strong></td>
<td valign="top">[<a href="#generickpi">GenericKpi</a>!]</td>
<td>

The Generic KPI of the group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">relationKeys</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Which relationKeys to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">years</td>
<td valign="top">[<a href="#nonnegativeint">NonNegativeInt</a>!]!</td>
<td>

Which years to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kinds</td>
<td valign="top">[<a href="#generickpikind">GenericKpiKind</a>!]!</td>
<td>

Which kinds to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>genericKpiListByDate</strong></td>
<td valign="top">[<a href="#generickpi">GenericKpi</a>!]</td>
<td>

The Generic KPI of the group and certain date.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">relationKeys</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Which relationKeys to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kinds</td>
<td valign="top">[<a href="#generickpialertkind">GenericKpiAlertKind</a>!]!</td>
<td>

Which kinds to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isoDate</td>
<td valign="top"><a href="#date">Date</a>!</td>
<td>

The full date on Generic KPI Calendar.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupUsers</strong></td>
<td valign="top">[<a href="#groupuser">GroupUser</a>!]</td>
<td>

The GroupUsers bound on this Group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>myGroupUser</strong></td>
<td valign="top"><a href="#groupuser">GroupUser</a></td>
<td>

The GroupUser bound to The signed-in User on this Group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupClient</strong></td>
<td valign="top"><a href="#client">Client</a></td>
<td>

The Client bound to The signed-in Client on this Group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

ID of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupUsersByUserIds</strong></td>
<td valign="top">[<a href="#groupuser">GroupUser</a>]</td>
<td>

The GroupUsers bound to The specific Users on this Group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">userIds</td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupUserByUserId</strong></td>
<td valign="top"><a href="#groupuser">GroupUser</a></td>
<td>

The GroupUser bound to The specific User on this Group.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">userId</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machinesByIndexes</strong></td>
<td valign="top">[<a href="#machine">Machine</a>]!</td>
<td>

A list of machines.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isStation</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Only list Station or only list Machine, omit it for Machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">indexes</td>
<td valign="top">[<a href="#int">Int</a>!]!</td>
<td>

indexes of some objects.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineByIndex</strong></td>
<td valign="top"><a href="#machine">Machine</a></td>
<td>

Fetches a machine by its index.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isStation</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Only list Station or only list Machine, omit it for Machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">index</td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

index of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inboundConnector</strong></td>
<td valign="top"><a href="#gateway">Gateway</a></td>
<td>

Inbound Connector of the Group

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inboundConnectorName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Inbound Connector Name of the Group

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energy</strong></td>
<td valign="top"><a href="#energyvalue">EnergyValue</a>!</td>
<td>

The Energy Demand & Consumption & Alert.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyShare</strong></td>
<td valign="top"><a href="#energysharevalue">EnergyShareValue</a>!</td>
<td>

The Energy Share.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeType</td>
<td valign="top"><a href="#timetype">TimeType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kind</td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usages</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyDevices</strong></td>
<td valign="top">[<a href="#energydevice">EnergyDevice</a>!]!</td>
<td>

The machine Energy Devices.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kind</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Filter by the kind of Energy Device.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">usages</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filter by the usages of kind of Energy Device.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isExclude</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyKpiList</strong></td>
<td valign="top">[<a href="#energykpi">EnergyKpi</a>!]!</td>
<td>

The group Energy KPI list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kind</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Filter by the kind of Energy Settings.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energySplitList</strong></td>
<td valign="top">[<a href="#energysplit">EnergySplit</a>!]!</td>
<td>

The group Energy Split list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kind</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Filter by the kind of Energy Settings.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeOfUsePeriods</strong></td>
<td valign="top">[<a href="#timeofuseperiod">TimeOfUsePeriod</a>!]!</td>
<td>

The Time-of-Use period list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">energyDeviceKind</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Filter by the kind of Energy Device.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">periodType</td>
<td valign="top"><a href="#periodtype">PeriodType</a></td>
<td>

Filter by the period type.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">day</td>
<td valign="top"><a href="#dayofweek">DayOfWeek</a></td>
<td>

Filter by the day of week.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyUsageInterfaces</strong></td>
<td valign="top">[<a href="#energyusageinterface">EnergyUsageInterface</a>!]</td>
<td>

The energy usage interface list.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyUsageInterfaceByIndex</strong></td>
<td valign="top"><a href="#energyusageinterface">EnergyUsageInterface</a></td>
<td>

The energy usage interface related to the group by index.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">index</td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

index of the object.

</td>
</tr>
</tbody>
</table>

### GroupEnergyUsageInterfaceRelation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_energyUsageInterfaceIds</strong></td>
<td valign="top">[<a href="#objectid">ObjectID</a>!]!</td>
<td>

The MongoDB IDs of the energy usage interfaces.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyUsageInterfaceIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

The Node IDs of the energy usage interfaces.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The group energy usage interface relation created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The group energy usage interface relation updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#group">Group</a></td>
<td></td>
</tr>
</tbody>
</table>

### GroupParameterInterfaceRelation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_groupId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#group">Group</a>!</td>
<td>

The group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_parameterInterfaceIds</strong></td>
<td valign="top">[<a href="#objectid">ObjectID</a>!]!</td>
<td>

The MongoDB IDs of the parameter interfaces.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterInterfaceIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

The Node IDs of the parameter interfaces.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The group parameter interface relation created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The group parameter interface relation updated at.

</td>
</tr>
</tbody>
</table>

### GroupUser

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_groupId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#group">Group</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_userId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roles</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roleDetails</strong></td>
<td valign="top">[<a href="#role">Role</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>orgAcls</strong></td>
<td valign="top">[<a href="#orgacl">OrgAcl</a>!]!</td>
<td>

Authorized Organizer ACL entries.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dhAcls</strong></td>
<td valign="top">[<a href="#dhaclentry">DHAclEntry</a>!]!</td>
<td>

Authorized DataHub ACL entries.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reAcls</strong></td>
<td valign="top">[<a href="#reaclentry">REAclEntry</a>!]!</td>
<td>

Authorized RuleEngine ACL entries.

</td>
</tr>
</tbody>
</table>

### HighLowEvent

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Name of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Description of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Machine of the Parameter of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Parameter of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#eventkind">EventKind</a>!</td>
<td>

The Kind of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The event created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The event updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>thresholdRules</strong></td>
<td valign="top">[<a href="#thresholdrule">ThresholdRule</a>!]!</td>
<td>

The Threshold Rules of the High-Low Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastLog</strong></td>
<td valign="top"><a href="#highloweventlog">HighLowEventLog</a></td>
<td>

Last High-Low Event Log, If It is Not Over

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>logsInRange</strong></td>
<td valign="top">[<a href="#highloweventlog">HighLowEventLog</a>!]!</td>
<td>

High-Low Event Logs in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">alarmLevelLevels</td>
<td valign="top">[<a href="#int">Int</a>!]</td>
<td>

If this argument is set and not an empty array, logs will be filtered by alarmLevels.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a></td>
<td>

The Parameter of the Event

</td>
</tr>
</tbody>
</table>

### HighLowEventLog

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelLevel</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>thresholdType</strong></td>
<td valign="top"><a href="#thresholdtype">ThresholdType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>thresholdValue</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagValue</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmMessage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>elapsed</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
</tbody>
</table>

### IApp

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The I.App name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The I.App name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>link</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The I.App link.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iconUrl</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The I.App icon URL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>thumbnailUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The I.App thumbnail URL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>headerLogoUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The I.App header logo URL.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>headerLogoUrls</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The I.App header logo URL translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>headerTitle</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The I.App header title.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>headerTitles</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The I.App header title translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>display</strong></td>
<td valign="top"><a href="#iappdisplay">IAppDisplay</a>!</td>
<td>

The I.App display option.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>index</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The I.App index.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>defaultKey</strong></td>
<td valign="top"><a href="#defaultiappkey">DefaultIAppKey</a></td>
<td>

The I.App default key.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the I.App Group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_groupId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the I.App Group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#iappgroup">IAppGroup</a></td>
<td>

The group of I.App.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>menuItems</strong></td>
<td valign="top">[<a href="#iappmenuitem">IAppMenuItem</a>!]!</td>
<td>

The I.App menu items.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The I.App created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The I.App updated at.

</td>
</tr>
</tbody>
</table>

### IAppGroup

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The I.App Group name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The I.App Group name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>display</strong></td>
<td valign="top"><a href="#iappdisplay">IAppDisplay</a>!</td>
<td>

The I.App Group display option.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>index</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The I.App Group index.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

The I.App Group is a preset default, which could not be removed.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iApps</strong></td>
<td valign="top">[<a href="#iapp">IApp</a>!]!</td>
<td>

The IApps of that group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The I.App Group created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The I.App Group updated at.

</td>
</tr>
</tbody>
</table>

### IAppMenuItem

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The I.App menu item name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The I.App menu item name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>link</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The I.App menu item link.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>display</strong></td>
<td valign="top"><a href="#iappdisplay">IAppDisplay</a>!</td>
<td>

The I.App menu item display option.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>index</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The I.App menu item index.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isHome</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

The home item of I.App menu.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iAppId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_iAppId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID of the I.App.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iApp</strong></td>
<td valign="top"><a href="#iapp">IApp</a>!</td>
<td>

The I.App.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the I.App menu item parent.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_parentId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the I.App Group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parent</strong></td>
<td valign="top"><a href="#iappmenuitem">IAppMenuItem</a></td>
<td>

The parent of I.App menu item.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>children</strong></td>
<td valign="top">[<a href="#iappmenuitem">IAppMenuItem</a>!]!</td>
<td>

The children of that I.App menu item.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The I.App menu item created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The I.App menu item updated at.

</td>
</tr>
</tbody>
</table>

### IfpProduct

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>products</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

iFactory Products.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userManagement</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cmdc</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>station</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>apiAgent</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

IFP Agent API URL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appIFactory</strong></td>
<td valign="top"><a href="#appifactory">AppIFactory</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appMaxnerva</strong></td>
<td valign="top"><a href="#appmaxnerva">AppMaxnerva</a></td>
<td></td>
</tr>
</tbody>
</table>

### InboundProtocol

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>port</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ssl</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### InfluxInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>online</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rtt</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>version</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### KpiMetric

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The KPI metric name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The KPI metric name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulaId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The KPI metric formula id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>defaultFormulaId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The KPI metric default formula id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schedule</strong></td>
<td valign="top"><a href="#pointschedule">PointSchedule</a>!</td>
<td>

The schedule to run calculation.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dailyHour</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td>

The hour to run with daily schedule, 0 ~ 23.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The KPI metric created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The KPI metric updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formula</strong></td>
<td valign="top"><a href="#formula">Formula</a></td>
<td>

The formula

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>defaultFormula</strong></td>
<td valign="top"><a href="#formula">Formula</a></td>
<td>

The default formula

</td>
</tr>
</tbody>
</table>

### KpiParameterUsage

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>depth</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The kpi parameter usage depth.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The kpi parameter usage name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The kpi parameter usage name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>icon</strong></td>
<td valign="top"><a href="#kpiparameterusageicon">KpiParameterUsageIcon</a>!</td>
<td>

The kpi parameter usage icon.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>index</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The kpi parameter usage index.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

The kpi parameter usage is a preset default, which could not be removed.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the kpi parameter usage parent.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_parentId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the kpi parameter usage parent.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parent</strong></td>
<td valign="top"><a href="#kpiparameterusage">KpiParameterUsage</a></td>
<td>

The parent of the kpi parameter usage.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>children</strong></td>
<td valign="top">[<a href="#kpiparameterusage">KpiParameterUsage</a>!]!</td>
<td>

The children of that kpi parameter usage.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterRelations</strong></td>
<td valign="top">[<a href="#kpiparameterusagerelation">KpiParameterUsageRelation</a>!]!</td>
<td>

Parameters applied this kpi parameter usage.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The kpi parameter usage created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The kpi parameter usage updated at.

</td>
</tr>
</tbody>
</table>

### KpiParameterUsageRelation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usageId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the kpi parameter usage.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_usageId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID of the kpi parameter usage.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usage</strong></td>
<td valign="top"><a href="#kpiparameterusage">KpiParameterUsage</a>!</td>
<td>

The kpi parameter usage.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_parameterId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID of the parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a>!</td>
<td>

The parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The kpi parameter usage created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The kpi parameter usage updated at.

</td>
</tr>
</tbody>
</table>

### Language

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nativeName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### License

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sign</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The license signature.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>data</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The license data.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parsedData</strong></td>
<td valign="top"><a href="#jsonobject">JSONObject</a>!</td>
<td>

Data structure parsed from data as JSON string.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>verified</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Is data verified against signature.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>verifiedData</strong></td>
<td valign="top"><a href="#jsonobject">JSONObject</a></td>
<td>

Data structure parsed with verified data as JSON string.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>validatedData</strong></td>
<td valign="top"><a href="#licensedata">LicenseData</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>systemMatched</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The license created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The license updated at.

</td>
</tr>
</tbody>
</table>

### LicenseData

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>model</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>serial</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>systemUuid</strong></td>
<td valign="top"><a href="#uuid">UUID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dumpedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>licenseUuid</strong></td>
<td valign="top"><a href="#uuid">UUID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pnList</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### Machine

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the machine group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_groupId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the machine group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#group">Group</a>!</td>
<td>

The group of the machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The machine name without translation.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The machine name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The machine name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>index</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The machine index.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine description.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>descriptions</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]</td>
<td>

The machine description translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>imageUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine image URL.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>imageUrls</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]</td>
<td>

The machine image URL translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>criticality</strong></td>
<td valign="top"><a href="#machinecriticality">MachineCriticality</a></td>
<td>

The machine criticality.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>owner</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine owner.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>purchaseDate</strong></td>
<td valign="top"><a href="#date">Date</a></td>
<td>

The machine purchase date.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>purchaseCurrency</strong></td>
<td valign="top"><a href="#currency">Currency</a></td>
<td>

The machine purchase currency.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>purchaseAmount</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td>

The machine purchase amount.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>onlineDate</strong></td>
<td valign="top"><a href="#date">Date</a></td>
<td>

The machine online date.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>manufacturer</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine manufacturer.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine model name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>contactPersonName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine contact person name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>supportPhone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine support phone.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>supportPhoneCountryCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine support phone country code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>supportEmail</strong></td>
<td valign="top"><a href="#emailaddress">EmailAddress</a></td>
<td>

The machine support email.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineType</strong></td>
<td valign="top"><a href="#machinetypefield">MachineTypeField</a></td>
<td>

The machine type.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>serialNumber</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine serial number.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameters</strong></td>
<td valign="top"><a href="#parameterconnection">ParameterConnection</a></td>
<td>

The machine parameters.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">orderBy</td>
<td valign="top"><a href="#parameterorder">ParameterOrder</a></td>
<td>

Ordering of the returned items.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">after</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Returns the elements in the list that come after the specified cursor.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">before</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Returns the elements in the list that come before the specified cursor.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">first</td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Returns the first _n_ elements from the list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">last</td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Returns the last _n_ elements from the list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">search</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Query to search projects by, currently only searching by name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">valueTypes</td>
<td valign="top">[<a href="#tagvaluetype">TagValueType</a>!]</td>
<td>

A list of value types to filter the parameters by.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kinds</td>
<td valign="top">[<a href="#parameterkind">ParameterKind</a>]</td>
<td>

A list of parameter kinds to filter the parameters by.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gatewayId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The associated gateway id of the machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isStation</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

This is a station.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The machine created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The machine updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>productionKpiList</strong></td>
<td valign="top">[<a href="#productionkpi">ProductionKpi</a>!]</td>
<td>

The Production KPI of the machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">years</td>
<td valign="top">[<a href="#positiveint">PositiveInt</a>!]!</td>
<td>

Which years to list.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>genericKpiAlertList</strong></td>
<td valign="top">[<a href="#generickpialert">GenericKpiAlert</a>!]</td>
<td>

The Generic KPI Alert of the machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">relationKeys</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Which relationKeys to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">years</td>
<td valign="top">[<a href="#nonnegativeint">NonNegativeInt</a>!]!</td>
<td>

Which years to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kinds</td>
<td valign="top">[<a href="#generickpialertkind">GenericKpiAlertKind</a>!]!</td>
<td>

Which kinds to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>genericKpiList</strong></td>
<td valign="top">[<a href="#generickpi">GenericKpi</a>!]</td>
<td>

The Generic KPI of the machine.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">relationKeys</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Which relationKeys to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">years</td>
<td valign="top">[<a href="#nonnegativeint">NonNegativeInt</a>!]!</td>
<td>

Which years to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kinds</td>
<td valign="top">[<a href="#generickpikind">GenericKpiKind</a>!]!</td>
<td>

Which kinds to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>genericKpiListByDate</strong></td>
<td valign="top">[<a href="#generickpi">GenericKpi</a>!]</td>
<td>

The Generic KPI of the machine and certain date.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">relationKeys</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Which relationKeys to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kinds</td>
<td valign="top">[<a href="#generickpialertkind">GenericKpiAlertKind</a>!]!</td>
<td>

Which kinds to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isoDate</td>
<td valign="top"><a href="#date">Date</a>!</td>
<td>

The full date on Generic KPI Calendar.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parametersByNames</strong></td>
<td valign="top">[<a href="#parameter">Parameter</a>]!</td>
<td>

A list of parameters.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">names</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

names of some objects.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterByName</strong></td>
<td valign="top"><a href="#parameter">Parameter</a></td>
<td>

Fetches a parameter by its name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">name</td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

name of the object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inboundConnector</strong></td>
<td valign="top"><a href="#gateway">Gateway</a></td>
<td>

Inbound Connector of the Machine

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inboundConnectorName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Inbound Connector Name of the Group

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energy</strong></td>
<td valign="top"><a href="#energymachinevalue">EnergyMachineValue</a>!</td>
<td>

The Energy Machine Connect.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyDevice</strong></td>
<td valign="top"><a href="#energydevice">EnergyDevice</a></td>
<td>

The machine Energy Device.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energySplit</strong></td>
<td valign="top"><a href="#energysplit">EnergySplit</a></td>
<td>

The machine Energy Split.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>events</strong></td>
<td valign="top"><a href="#eventconnection">EventConnection</a>!</td>
<td>

Events of the Machine

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>highLowEvents</strong></td>
<td valign="top">[<a href="#highlowevent">HighLowEvent</a>!]!</td>
<td>

High-Low Events of the Machine

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmCodeEvents</strong></td>
<td valign="top">[<a href="#alarmcodeevent">AlarmCodeEvent</a>!]!</td>
<td>

Alarm Code Events of the Machine

</td>
</tr>
</tbody>
</table>

### MachineStatus

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>depth</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The machine status depth.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>index</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The machine status index.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The machine status name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The machine status name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>color</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The machine status color.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

The machine status is a preset default, which could not be removed.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the machine status parent.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_parentId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the machine status parent.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parent</strong></td>
<td valign="top"><a href="#machinestatus">MachineStatus</a></td>
<td>

The parent of the machine status.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>children</strong></td>
<td valign="top">[<a href="#machinestatus">MachineStatus</a>!]!</td>
<td>

The children of that machine status.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>layer1</strong></td>
<td valign="top"><a href="#machinestatus">MachineStatus</a>!</td>
<td>

The layer 1 of the machine status chain.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The machine status created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The machine status updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codes</strong></td>
<td valign="top">[<a href="#parametermappingcode">ParameterMappingCode</a>!]!</td>
<td>

Parameter mapping codes indicate this machine status.

</td>
</tr>
</tbody>
</table>

### MachineType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The parent key of machine type.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The key of machine type.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The machine type created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The machine type updated at.

</td>
</tr>
</tbody>
</table>

### MachineTypeCategory

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The key of machine type category.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineTypes</strong></td>
<td valign="top">[<a href="#machinetype">MachineType</a>!]!</td>
<td>

Types of machine type category.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The machine type category created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The machine type category updated at.

</td>
</tr>
</tbody>
</table>

### MachineTypeField

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### MarqueeAction

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rule</strong></td>
<td valign="top"><a href="#rule">Rule</a>!</td>
<td>

Rule that this Action Belongs to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether this Action is Active

</td>
</tr>
</tbody>
</table>

### Me

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>requestId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The request ID.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

The signed-in User.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>client</strong></td>
<td valign="top"><a href="#client">Client</a></td>
<td>

The accessing Client.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>orgAcls</strong></td>
<td valign="top">[<a href="#orgacl">OrgAcl</a>!]!</td>
<td>

Authorized Organizer ACL entries.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dhAcls</strong></td>
<td valign="top">[<a href="#dhaclentry">DHAclEntry</a>!]!</td>
<td>

Authorized DataHub ACL entries.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reAcls</strong></td>
<td valign="top">[<a href="#reaclentry">REAclEntry</a>!]!</td>
<td>

Authorized RuleEngine ACL entries.

</td>
</tr>
</tbody>
</table>

### ModbusTag

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>serverId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dataType</strong></td>
<td valign="top"><a href="#urmodbusdatatype">URModbusDataType</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### ModbusValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>str</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>savedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>serverNodeId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dataType</strong></td>
<td valign="top"><a href="#urmodbusdatatype">URModbusDataType</a></td>
<td></td>
</tr>
</tbody>
</table>

### MoveMachinePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#machine">Machine</a>!</td>
<td>

The machine object.

</td>
</tr>
</tbody>
</table>

### NotificationAction

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rule</strong></td>
<td valign="top"><a href="#rule">Rule</a>!</td>
<td>

Rule that this Action Belongs to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether this Action is Active

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Group ID of the Notification Action

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subject</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Mail Subject of the Notification Action

</td>
</tr>
</tbody>
</table>

### NotificationGroup

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The ID of the Notification Center Group

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Name of the Notification Center Group

</td>
</tr>
</tbody>
</table>

### OpcUaTag

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>serverId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subscriptionTagId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### OpcUaValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>str</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>savedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>serverNodeId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingElementNodeId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subscriptionTagNodeId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### OrgAcl

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Role about this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scope</strong></td>
<td valign="top"><a href="#orgaclscope">OrgAclScope</a>!</td>
<td>

The scope of this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mode</strong></td>
<td valign="top"><a href="#aclmode">AclMode</a>!</td>
<td>

The mode of this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The ACL created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The ACL updated at.

</td>
</tr>
</tbody>
</table>

### OrganizerApiInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Constant string ID.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App short name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App description.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>version</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App version.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>author</strong></td>
<td valign="top"><a href="#author">Author</a>!</td>
<td>

The App author.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mongoVersion</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The MongoDB version.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td>

The App server time.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isEnSaaS</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that is running on WISE-PaaS/EnSaaS 4.0.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isIfpApp</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that query is performed by iFactory+ App.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ifpAppSecretReady</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that iFactory+ App Secret is ready.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sid</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Service Register metadata sid.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usersIsEmpty</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Determine that users is empty

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>marketplaceUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The marketplace URL for Command Center.

</td>
</tr>
</tbody>
</table>

### Outbound

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Outbound name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>transport</strong></td>
<td valign="top"><a href="#outboundtransport">OutboundTransport</a>!</td>
<td>

The Outbound transport.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>url</strong></td>
<td valign="top"><a href="#url">URL</a>!</td>
<td>

The Outbound url.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>allowUnauthorized</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Allow unauthorized server certificate.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>active</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Only active Outbound will connect.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Outbound sourceId.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>format</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The Outbound format.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Outbound created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Outbound updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>connected</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

The Outbound transport is connected or not.

</td>
</tr>
</tbody>
</table>

### PageInfo

Information about pagination in a connection.

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>endCursor</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

When paginating forwards, the cursor to continue.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasNextPage</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

When paginating forwards, are there more items?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasPreviousPage</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

When paginating backwards, are there more items?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startCursor</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

When paginating backwards, the cursor to continue.

</td>
</tr>
</tbody>
</table>

### Parameter

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#machine">Machine</a></td>
<td>

Which machine the parameter belongs to.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_machineId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the parameter machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mapping</strong></td>
<td valign="top"><a href="#parametermappingrule">ParameterMappingRule</a></td>
<td>

The parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_mappingId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingRuleName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The parameter mapping rule name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingCodes</strong></td>
<td valign="top">[<a href="#parametermappingcode">ParameterMappingCode</a>!]</td>
<td>

The parameter mapping codes.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The parameter name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The parameter description.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>descriptions</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]</td>
<td>

The parameter description translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The parameter scada id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The parameter tag id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>valueType</strong></td>
<td valign="top"><a href="#tagvaluetype">TagValueType</a></td>
<td>

The parameter value type.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gatewayId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The associated gateway id of the parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isStation</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

This is a station parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#parameterkind">ParameterKind</a></td>
<td>

The parameter kind.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>associatedParameters</strong></td>
<td valign="top">[<a href="#parameter">Parameter</a>!]</td>
<td>

Associated parameters reference this parameter as source.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>metricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_metricId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>opcUaTag</strong></td>
<td valign="top"><a href="#opcuatag">OpcUaTag</a></td>
<td>

The OPC UA tag bound to the parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>modbusTag</strong></td>
<td valign="top"><a href="#modbustag">ModbusTag</a></td>
<td>

The Modbus tag bound to the parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The parameter created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The parameter updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>productionKpiList</strong></td>
<td valign="top">[<a href="#productionkpi">ProductionKpi</a>!]</td>
<td>

The Production KPI of the parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">years</td>
<td valign="top">[<a href="#positiveint">PositiveInt</a>!]!</td>
<td>

Which years to list.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>genericKpiAlertList</strong></td>
<td valign="top">[<a href="#generickpialert">GenericKpiAlert</a>!]</td>
<td>

The Generic KPI Alert of the parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">relationKeys</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Which relationKeys to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">years</td>
<td valign="top">[<a href="#nonnegativeint">NonNegativeInt</a>!]!</td>
<td>

Which years to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kinds</td>
<td valign="top">[<a href="#generickpialertkind">GenericKpiAlertKind</a>!]!</td>
<td>

Which kinds to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>genericKpiList</strong></td>
<td valign="top">[<a href="#generickpi">GenericKpi</a>!]</td>
<td>

The Generic KPI of the parameter.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">relationKeys</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Which relationKeys to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">years</td>
<td valign="top">[<a href="#nonnegativeint">NonNegativeInt</a>!]!</td>
<td>

Which years to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kinds</td>
<td valign="top">[<a href="#generickpikind">GenericKpiKind</a>!]!</td>
<td>

Which kinds to list, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>genericKpiListByDate</strong></td>
<td valign="top">[<a href="#generickpi">GenericKpi</a>!]</td>
<td>

The Generic KPI of the parameter and certain date.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">relationKeys</td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

Which relationKeys to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">kinds</td>
<td valign="top">[<a href="#generickpialertkind">GenericKpiAlertKind</a>!]!</td>
<td>

Which kinds to list.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">isoDate</td>
<td valign="top"><a href="#date">Date</a>!</td>
<td>

The full date on Generic KPI Calendar.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gateway</strong></td>
<td valign="top"><a href="#gateway">Gateway</a></td>
<td>

The Gateway of the Scada ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagValueType</strong></td>
<td valign="top"><a href="#tagvaluetype">TagValueType</a></td>
<td>

Parameter Value Type

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastValue</strong></td>
<td valign="top"><a href="#commonvalue">CommonValue</a></td>
<td>

Last Parameter Value

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">before</td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

If set, the field will be the last value before this timestamp.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">after</td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

If set, the field will be the last value after this timestamp.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">includeBefore</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If set, the time range when searching value include before timestamp.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">includeAfter</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If set, the time range when searching value include after timestamp.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastBadValue</strong></td>
<td valign="top"><a href="#badtagvalue">BadTagValue</a></td>
<td>

Last Bad Parameter Value(\*)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>valuesInRange</strong></td>
<td valign="top">[<a href="#commonvalue">CommonValue</a>!]!</td>
<td>

Parameter Values in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>limitToNthValues</strong></td>
<td valign="top">[<a href="#commonvalue">CommonValue</a>!]!</td>
<td>

Last or First n Parameter Values.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">n</td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Positive n number for First n, Negative n number for Last n.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>downSampledInRange</strong></td>
<td valign="top">[<a href="#downsampledcommonvalue">DownSampledCommonValue</a>!]!</td>
<td>

Down-Sampled Parameter Values in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">maxDataPoints</td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Maximum Number of Data Points will be Returned

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">fillOption</td>
<td valign="top"><a href="#filloption">FillOption</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>badValuesInRange</strong></td>
<td valign="top">[<a href="#badtagvalue">BadTagValue</a>!]!</td>
<td>

Bad Parameter Values(\*) in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>calculatedValue</strong></td>
<td valign="top"><a href="#downsampledcommonvalue">DownSampledCommonValue</a></td>
<td>

Calculated Parameter Value

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">fn</td>
<td valign="top"><a href="#calculationfn">CalculationFn</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>valuesByTimeInterval</strong></td>
<td valign="top">[<a href="#downsampledcommonvalue">DownSampledCommonValue</a>!]!</td>
<td>

Parameter Values between Specific Time Interval in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeInterval</td>
<td valign="top"><a href="#timeinterval">TimeInterval</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">samplingMethod</td>
<td valign="top"><a href="#samplingmethod">SamplingMethod</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">timeZone</td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inboundConnector</strong></td>
<td valign="top"><a href="#gateway">Gateway</a></td>
<td>

Inbound Connector of the Parameter

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inboundConnectorName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Inbound Connector Name of the Group

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>transformationPoint</strong></td>
<td valign="top"><a href="#transformationpoint">TransformationPoint</a></td>
<td>

Transformation Point of Transformation kind Parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>calculationPoint</strong></td>
<td valign="top"><a href="#calculationpoint">CalculationPoint</a></td>
<td>

Calculation Point of Calculation kind Parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>event</strong></td>
<td valign="top"><a href="#event">Event</a></td>
<td>

Event of the Parameter

</td>
</tr>
</tbody>
</table>

### ParameterConnection

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>edges</strong></td>
<td valign="top">[<a href="#parameteredge">ParameterEdge</a>!]</td>
<td>

A list of edges.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nodes</strong></td>
<td valign="top">[<a href="#parameter">Parameter</a>!]</td>
<td>

A list of nodes.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#pageinfo">PageInfo</a>!</td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Identifies the total count of items in the connection.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>offset</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

First item offset.

</td>
</tr>
</tbody>
</table>

### ParameterEdge

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>cursor</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

A cursor for use in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>node</strong></td>
<td valign="top"><a href="#parameter">Parameter</a></td>
<td>

The item at the end of the edge.

</td>
</tr>
</tbody>
</table>

### ParameterInterface

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The parameter interface name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterNames</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

The parameter names of the parameter interface.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The primary key of the parameter interface, used for vertical report.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterNameCount</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

The number of parameter names of the parameter interface.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>calculationSettings</strong></td>
<td valign="top">[<a href="#calculationsetting">CalculationSetting</a>!]!</td>
<td>

The calculation settings of the parameter interface.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The parameter interface updated at.

</td>
</tr>
</tbody>
</table>

### ParameterMappingCode

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rule</strong></td>
<td valign="top"><a href="#parametermappingrule">ParameterMappingRule</a>!</td>
<td>

Which parameter mapping rule the parameter mapping code belongs to.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ruleId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_ruleId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID of the parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#machinestatus">MachineStatus</a></td>
<td>

The machine status.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the machine status.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_statusId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the machine status.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The parameter mapping code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>message</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The parameter mapping code message.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>messages</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The parameter mapping code message translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The parameter mapping code created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The parameter mapping code updated at.

</td>
</tr>
</tbody>
</table>

### ParameterMappingRule

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The parameter mapping rule name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pType</strong></td>
<td valign="top"><a href="#tagvaluetype">TagValueType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The parameter mapping rule created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The parameter mapping rule updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codes</strong></td>
<td valign="top">[<a href="#parametermappingcode">ParameterMappingCode</a>!]!</td>
<td>

The parameter mapping codes.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameters</strong></td>
<td valign="top">[<a href="#parameter">Parameter</a>!]!</td>
<td>

Parameters applied this mapping rule.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>profileParameters</strong></td>
<td valign="top">[<a href="#profileparameter">ProfileParameter</a>!]!</td>
<td>

Profile parameters applied this mapping rule.

</td>
</tr>
</tbody>
</table>

### PeakValueData

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### PeriodTypeInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The period type name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#ehstranslation">EhsTranslation</a>!]!</td>
<td>

The group name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>periodType</strong></td>
<td valign="top"><a href="#periodtype">PeriodType</a>!</td>
<td>

The period type.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

The period type is a preset default, which could not be removed.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The period type created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The period type updated at.

</td>
</tr>
</tbody>
</table>

### PointValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>str</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>savedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterNodeId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pointId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>detail</strong></td>
<td valign="top"><a href="#jsonobject">JSONObject</a></td>
<td></td>
</tr>
</tbody>
</table>

### ProductionKpi

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>year</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The year of Production KPI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#productionkpikind">ProductionKpiKind</a>!</td>
<td>

The kind of Production KPI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the parameter.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Jan</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Feb</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Mar</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Apr</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>May</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Jun</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Jul</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Aug</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Sep</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Oct</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Nov</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Dec</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>avg</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Production KPI created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Production KPI updated at.

</td>
</tr>
</tbody>
</table>

### ProfileMachine

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The machine name.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>names</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The machine name translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The machine description.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>descriptions</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The machine description translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>imageUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine image URL.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>imageUrls</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The machine image URL translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameters</strong></td>
<td valign="top">[<a href="#profileparameter">ProfileParameter</a>!]!</td>
<td>

The machine parameters.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The number of the parameters of the machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The machine created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The machine updated at.

</td>
</tr>
</tbody>
</table>

### ProfileParameter

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#profilemachine">ProfileMachine</a></td>
<td>

Which machine the parameter belongs to.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the parameter machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_machineId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the parameter machine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mapping</strong></td>
<td valign="top"><a href="#parametermappingrule">ParameterMappingRule</a></td>
<td>

The parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_mappingId</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a></td>
<td>

The MongoDB ID of the parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingRuleName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The parameter mapping rule name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The parameter name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The parameter description.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>descriptions</strong></td>
<td valign="top">[<a href="#translation">Translation</a>!]!</td>
<td>

The parameter description translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>valueType</strong></td>
<td valign="top"><a href="#tagvaluetype">TagValueType</a>!</td>
<td>

The parameter value type.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The parameter created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The parameter updated at.

</td>
</tr>
</tbody>
</table>

### PublicUser

Virtual Type from User

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The name for display.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>username</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The username used for login.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>active</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Only active user can sign-in.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupUser</strong></td>
<td valign="top"><a href="#groupuser">GroupUser</a></td>
<td>

The GroupUser this user bound to.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">groupId</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Fetches a group user by its groupId.

</td>
</tr>
</tbody>
</table>

### REAcl

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Role about this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scope</strong></td>
<td valign="top"><a href="#reaclscope">REAclScope</a>!</td>
<td>

The scope of this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mode</strong></td>
<td valign="top"><a href="#aclmode">AclMode</a>!</td>
<td>

The mode of this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The ACL created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The ACL updated at.

</td>
</tr>
</tbody>
</table>

### REAclEntry

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Role about this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scope</strong></td>
<td valign="top"><a href="#reaclscope">REAclScope</a>!</td>
<td>

The scope of this ACL.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mode</strong></td>
<td valign="top"><a href="#aclmode">AclMode</a>!</td>
<td>

The mode of this ACL.

</td>
</tr>
</tbody>
</table>

### RETranslation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RegisterInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The register name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>version</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The register version.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sid</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The register info serial id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>metadata</strong></td>
<td valign="top">[<a href="#registerinfometadata">RegisterInfoMetadata</a>!]!</td>
<td>

A set of data that describes and gives information about this register.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">keys</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Keys of the key-value store.

</td>
</tr>
</tbody>
</table>

### RegisterInfoMetadata

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The metadata key.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>string</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The metadata value as a string.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

The metadata value as a number.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>json</strong></td>
<td valign="top"><a href="#json">JSON</a></td>
<td>

The metadata value as json parsed.

</td>
</tr>
</tbody>
</table>

### RemoveAlarmCodeEventPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveAlarmLevelPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveAlarmLevelTranslationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmLevel</strong></td>
<td valign="top"><a href="#alarmlevel">AlarmLevel</a>!</td>
<td>

The AlarmLevel Object

</td>
</tr>
</tbody>
</table>

### RemoveCalculationPointPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulaId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Calculation Formula id.

</td>
</tr>
</tbody>
</table>

### RemoveClientPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveCodeRulePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveCodeRulesByCodeIdsPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveEhsUsagePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveEnergyDeviceKindPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveEnergyDeviceKindTranslationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyDeviceKind</strong></td>
<td valign="top"><a href="#energydevicekind">EnergyDeviceKind</a>!</td>
<td>

The EnergyDeviceKind Object

</td>
</tr>
</tbody>
</table>

### RemoveEnergyUsageInterfacePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveEnergyUsagePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveEnergyUsageTranslationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyUsage</strong></td>
<td valign="top"><a href="#energyusage">EnergyUsage</a>!</td>
<td>

The EnergyUsage Object

</td>
</tr>
</tbody>
</table>

### RemoveEventByParameterPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveFormulaPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveGatewayPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveGenericKpiListPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveGroupPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveGroupTranslationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#group">Group</a>!</td>
<td>

The group object.

</td>
</tr>
</tbody>
</table>

### RemoveHighLowEventPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveHolidaysPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveIAppGroupPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveIAppGroupTranslationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iAppGroup</strong></td>
<td valign="top"><a href="#iappgroup">IAppGroup</a>!</td>
<td>

The I.App Group object.

</td>
</tr>
</tbody>
</table>

### RemoveIAppMenuItemPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveIAppMenuItemTranslationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iAppMenuItem</strong></td>
<td valign="top"><a href="#iappmenuitem">IAppMenuItem</a>!</td>
<td>

The I.App menu item object.

</td>
</tr>
</tbody>
</table>

### RemoveIAppPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveIAppTranslationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iApp</strong></td>
<td valign="top"><a href="#iapp">IApp</a>!</td>
<td>

The I.App object.

</td>
</tr>
</tbody>
</table>

### RemoveKpiMetricPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveKpiMetricTranslationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpiMetric</strong></td>
<td valign="top"><a href="#kpimetric">KpiMetric</a>!</td>
<td>

The kpi metric object.

</td>
</tr>
</tbody>
</table>

### RemoveKpiParameterUsagePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveKpiParameterUsageRelationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveLicensePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveMachinePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveMachineStatusPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveMachineTranslationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#machine">Machine</a>!</td>
<td>

The machine object.

</td>
</tr>
</tbody>
</table>

### RemoveMachineTypeCategoryPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveMachineTypePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveMarqueeActionPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveNotificationActionPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveOutboundPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveParameterInterfacePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveParameterMappingCodePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveParameterMappingRulePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveParameterPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveParameterTranslationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a>!</td>
<td>

The parameter object.

</td>
</tr>
</tbody>
</table>

### RemoveRolePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveThresholdRulePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveThresholdRuleTranslationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rule</strong></td>
<td valign="top"><a href="#thresholdrule">ThresholdRule</a>!</td>
<td>

The ThresholdRule Object

</td>
</tr>
</tbody>
</table>

### RemoveTimeOfUsePeriodPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveTransformationPointPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### RemoveTranslationLangPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The removed count of items.

</td>
</tr>
</tbody>
</table>

### RemoveTranslationsByLangPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>removedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Removed Count of Items

</td>
</tr>
</tbody>
</table>

### Role

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The key of Role.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The name of Role.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The description of Role.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether the Role is default.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Role created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Role updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usersCount</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

How many users belong to this Role.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>orgAcls</strong></td>
<td valign="top">[<a href="#orgacl">OrgAcl</a>!]!</td>
<td>

Organizer ACL entries about this Role.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dhAcls</strong></td>
<td valign="top">[<a href="#dhacl">DHAcl</a>!]!</td>
<td>

DataHub ACL entries about this Role.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reAcls</strong></td>
<td valign="top">[<a href="#reacl">REAcl</a>!]!</td>
<td>

RuleEngine ACL entries about this Role.

</td>
</tr>
</tbody>
</table>

### RuleEngineApiInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Constant string ID.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App short name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App description.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>version</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The App version.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>author</strong></td>
<td valign="top"><a href="#author">Author</a>!</td>
<td>

The App author.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mongoVersion</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The MongoDB version.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>influxInfo</strong></td>
<td valign="top">[<a href="#influxinfo">InfluxInfo</a>!]!</td>
<td>

The InfluxDB Information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNotificationReady</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that Notification Center is ready.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td>

The App server time.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isEnSaaS</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that is running on WISE-PaaS/EnSaaS 4.0.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isIfpApp</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that query is performed by iFactory+ App.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ifpAppSecretReady</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Determine that iFactory+ App Secret is ready.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sid</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Service Register metadata sid.

</td>
</tr>
</tbody>
</table>

### SetAsHolidaysPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>holidays</strong></td>
<td valign="top">[<a href="#generickpiholiday">GenericKpiHoliday</a>!]!</td>
<td>

List Generic KPI Holidays.

</td>
</tr>
</tbody>
</table>

### SetCalculationPointPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>calculationPoint</strong></td>
<td valign="top"><a href="#calculationpoint">CalculationPoint</a>!</td>
<td>

The calculation point object.

</td>
</tr>
</tbody>
</table>

### SetConstantParameterValuesPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### SetDHAclPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>acl</strong></td>
<td valign="top"><a href="#dhacl">DHAcl</a>!</td>
<td>

The ACL object.

</td>
</tr>
</tbody>
</table>

### SetElectricityRateContractPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>electricityRateContract</strong></td>
<td valign="top"><a href="#electricityratecontract">ElectricityRateContract</a></td>
<td>

The Electricity Rate Contract object.

</td>
</tr>
</tbody>
</table>

### SetEnergyDevicePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyDevice</strong></td>
<td valign="top"><a href="#energydevice">EnergyDevice</a></td>
<td>

The Energy Device object.

</td>
</tr>
</tbody>
</table>

### SetEnergyKpiPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyKpi</strong></td>
<td valign="top"><a href="#energykpi">EnergyKpi</a></td>
<td>

The Energy KPI object.

</td>
</tr>
</tbody>
</table>

### SetEnergySplitPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energySplit</strong></td>
<td valign="top"><a href="#energysplit">EnergySplit</a></td>
<td>

The Energy Split object.

</td>
</tr>
</tbody>
</table>

### SetGenericKpiListPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kpiList</strong></td>
<td valign="top">[<a href="#generickpi">GenericKpi</a>!]!</td>
<td>

The Generic KPI List.

</td>
</tr>
</tbody>
</table>

### SetGroupEnergyUsageInterfaceRelationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>relation</strong></td>
<td valign="top"><a href="#groupenergyusageinterfacerelation">GroupEnergyUsageInterfaceRelation</a></td>
<td>

The group energy usage interface relation object.

</td>
</tr>
</tbody>
</table>

### SetGroupParameterInterfaceRelationPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>relation</strong></td>
<td valign="top"><a href="#groupparameterinterfacerelation">GroupParameterInterfaceRelation</a></td>
<td>

The group parameter interface relation object.

</td>
</tr>
</tbody>
</table>

### SetGroupUserPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupUser</strong></td>
<td valign="top"><a href="#groupuser">GroupUser</a></td>
<td>

The GroupUser object.

</td>
</tr>
</tbody>
</table>

### SetI18nLangPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The target i18n language code, e.g., "en-US" and "zh-TW"

</td>
</tr>
</tbody>
</table>

### SetMyTenantPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>tenantGroup</strong></td>
<td valign="top"><a href="#group">Group</a></td>
<td>

The Tenant Group.

</td>
</tr>
</tbody>
</table>

### SetOrgAclPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>acl</strong></td>
<td valign="top"><a href="#orgacl">OrgAcl</a>!</td>
<td>

The ACL object.

</td>
</tr>
</tbody>
</table>

### SetProductionKpiPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>productionKpi</strong></td>
<td valign="top"><a href="#productionkpi">ProductionKpi</a></td>
<td>

The Production KPI object.

</td>
</tr>
</tbody>
</table>

### SetREAclPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>acl</strong></td>
<td valign="top"><a href="#reacl">REAcl</a>!</td>
<td>

The ACL object.

</td>
</tr>
</tbody>
</table>

### SetStatusExpirationPeriodPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>statusExpirationPeriod</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
</tbody>
</table>

### SetTenantQuotaPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>tenantQuota</strong></td>
<td valign="top"><a href="#tenantquota">TenantQuota</a>!</td>
<td>

The TenantQuota object.

</td>
</tr>
</tbody>
</table>

### SetTransformationPointPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>transformationPoint</strong></td>
<td valign="top"><a href="#transformationpoint">TransformationPoint</a>!</td>
<td>

The transformation point object.

</td>
</tr>
</tbody>
</table>

### SetUserPasswordPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a>!</td>
<td>

The User Object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Password of the User.

</td>
</tr>
</tbody>
</table>

### SignInPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a>!</td>
<td>

The Signed In User.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ifpToken</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The IFPToken. 代表使用者身分的 token，期限為八小時

</td>
</tr>
</tbody>
</table>

### Tag

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gateway</strong></td>
<td valign="top"><a href="#gateway">Gateway</a>!</td>
<td>

Gateway that this Tag Belongs to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>valueType</strong></td>
<td valign="top"><a href="#tagvaluetype">TagValueType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isAcquiring</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether Values of this Tag will be Acquired

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>saveToHistory</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Whether Values of this Tag will be Saved Longer

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>connectionStatus</strong></td>
<td valign="top"><a href="#connectionstatus">ConnectionStatus</a></td>
<td>

Connection Status of this Tag

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastValue</strong></td>
<td valign="top"><a href="#tagvalue">TagValue</a></td>
<td>

Last Tag Value

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">before</td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

If set, the field will be the last value before this timestamp.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">after</td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

If set, the field will be the last value after this timestamp.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">includeBefore</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If set, the time range when searching value include before timestamp.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">includeAfter</td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

If set, the time range when searching value include after timestamp.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastBadValue</strong></td>
<td valign="top"><a href="#badtagvalue">BadTagValue</a></td>
<td>

Last Bad Tag Value(\*)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>valuesInRange</strong></td>
<td valign="top">[<a href="#tagvalue">TagValue</a>!]!</td>
<td>

Tag Values in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>downSampledInRange</strong></td>
<td valign="top">[<a href="#downsampledtagvalue">DownSampledTagValue</a>!]!</td>
<td>

Down-Sampled Tag Values in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">maxDataPoints</td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Maximum Number of Data Points will be Returned

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">fillOption</td>
<td valign="top"><a href="#filloption">FillOption</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>badValuesInRange</strong></td>
<td valign="top">[<a href="#badtagvalue">BadTagValue</a>!]!</td>
<td>

Bad Tag Values(\*) in Time Range

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">to</td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Tag Created at

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Tag Updated at

</td>
</tr>
</tbody>
</table>

### TagValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>str</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>savedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deviceId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingCodeId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the parameter mapping code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingCode</strong></td>
<td valign="top"><a href="#parametermappingcode">ParameterMappingCode</a></td>
<td>

The parameter mapping code.

</td>
</tr>
</tbody>
</table>

### TenantQuota

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantGroup</strong></td>
<td valign="top"><a href="#group">Group</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The TenantQuota name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>limit</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

Set by the Tenant Admin of **This** Tenant Group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>quota</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

Set by the Tenant Admin of **Parent** Tenant Group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
</tbody>
</table>

### ThresholdRule

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>event</strong></td>
<td valign="top"><a href="#highlowevent">HighLowEvent</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmLevel</strong></td>
<td valign="top"><a href="#alarmlevel">AlarmLevel</a>!</td>
<td>

AlarmLevel that this Rule Related to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>actions</strong></td>
<td valign="top">[<a href="#action">Action</a>!]!</td>
<td>

The Actions of the Rule

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

Value of the Threshold Rule

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether this Threshold Rule is Active

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmMessage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Alarm Message of the Threshold Rule

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#string">String</a></td>
<td>

If set, the field will be a translation of the language(if it exist).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmMessages</strong></td>
<td valign="top">[<a href="#retranslation">RETranslation</a>!]!</td>
<td>

The alarm message translations.

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">langs</td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Filters the field translations by this langs.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#thresholdtype">ThresholdType</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### TimeOfUsePeriod

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyDeviceKind</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The kind of Time-of-Use period.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the group.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>periodType</strong></td>
<td valign="top"><a href="#periodtype">PeriodType</a>!</td>
<td>

The period type.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>periodTypeInfo</strong></td>
<td valign="top"><a href="#periodtypeinfo">PeriodTypeInfo</a>!</td>
<td>

PeriodType that this Time-of-Use period Related to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>day</strong></td>
<td valign="top"><a href="#dayofweek">DayOfWeek</a>!</td>
<td>

The day of week of Time-of-Use period.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startTime</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Time-of-Use period start time.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>endTime</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Time-of-Use period end time.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Time-of-Use period created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Time-of-Use period updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startTimeString</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Time-of-Use period start time (hh:mm).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>endTimeString</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Time-of-Use period end time (hh:mm).

</td>
</tr>
</tbody>
</table>

### TimeZoneInfo

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>timeZone</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>label</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>utcOffset</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### TopicLevel

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>levels</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scadaIndex</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
</tbody>
</table>

### TransformationDiffRule

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>nonNegative</strong></td>
<td valign="top"><a href="#diffnonnegativecorrection">DiffNonNegativeCorrection</a></td>
<td>

The Correction to apply on Non Negative Diff.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>positivePercentThreshold</strong></td>
<td valign="top"><a href="#positiveint">PositiveInt</a></td>
<td>

The acceptable increment threshold in percent.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>negativePercentThreshold</strong></td>
<td valign="top"><a href="#positiveint">PositiveInt</a></td>
<td>

The acceptable decrement threshold in percent.

</td>
</tr>
</tbody>
</table>

### TransformationPoint

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Transformation Parameter id.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceParameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The source Parameter id to transform.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schedule</strong></td>
<td valign="top"><a href="#pointschedule">PointSchedule</a>!</td>
<td>

The schedule to run transformation.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dailyHour</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td>

The hour to run with daily schedule, 0 ~ 23.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fn</strong></td>
<td valign="top"><a href="#transformationpointfn">TransformationPointFn</a>!</td>
<td>

The Transformation function.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>diffRule</strong></td>
<td valign="top"><a href="#transformationdiffrule">TransformationDiffRule</a></td>
<td>

The Transformation Diff function rules.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The point created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The point updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceParameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a></td>
<td>

The source parameter to transform.

</td>
</tr>
</tbody>
</table>

### TranslateAlarmLevelPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>alarmLevel</strong></td>
<td valign="top"><a href="#alarmlevel">AlarmLevel</a>!</td>
<td>

The AlarmLevel Object

</td>
</tr>
</tbody>
</table>

### TranslateEnergyDeviceKindPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyDeviceKind</strong></td>
<td valign="top"><a href="#energydevicekind">EnergyDeviceKind</a>!</td>
<td>

The EnergyDeviceKind Object

</td>
</tr>
</tbody>
</table>

### TranslateEnergyUsagePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyUsage</strong></td>
<td valign="top"><a href="#energyusage">EnergyUsage</a>!</td>
<td>

The EnergyUsage Object

</td>
</tr>
</tbody>
</table>

### TranslateGroupPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#group">Group</a>!</td>
<td>

The group object.

</td>
</tr>
</tbody>
</table>

### TranslateIAppGroupPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iAppGroup</strong></td>
<td valign="top"><a href="#iappgroup">IAppGroup</a>!</td>
<td>

The I.App Group object.

</td>
</tr>
</tbody>
</table>

### TranslateIAppMenuItemPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iAppMenuItem</strong></td>
<td valign="top"><a href="#iappmenuitem">IAppMenuItem</a>!</td>
<td>

The I.App object.

</td>
</tr>
</tbody>
</table>

### TranslateIAppPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iApp</strong></td>
<td valign="top"><a href="#iapp">IApp</a>!</td>
<td>

The I.App object.

</td>
</tr>
</tbody>
</table>

### TranslateKpiMetricPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kpiMetric</strong></td>
<td valign="top"><a href="#kpimetric">KpiMetric</a>!</td>
<td>

The KPI metric object.

</td>
</tr>
</tbody>
</table>

### TranslateMachinePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#machine">Machine</a>!</td>
<td>

The machine object.

</td>
</tr>
</tbody>
</table>

### TranslateMachineStatusPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machineStatus</strong></td>
<td valign="top"><a href="#machinestatus">MachineStatus</a>!</td>
<td>

The machine status object.

</td>
</tr>
</tbody>
</table>

### TranslateParameterMappingCodePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>mappingCode</strong></td>
<td valign="top"><a href="#parametermappingcode">ParameterMappingCode</a>!</td>
<td>

The parameter mapping code object.

</td>
</tr>
</tbody>
</table>

### TranslateParameterPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a>!</td>
<td>

The parameter object.

</td>
</tr>
</tbody>
</table>

### TranslatePeriodTypeInfoPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>periodTypeInfo</strong></td>
<td valign="top"><a href="#periodtypeinfo">PeriodTypeInfo</a>!</td>
<td>

The period type object.

</td>
</tr>
</tbody>
</table>

### TranslateProfileMachinePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>profileMachine</strong></td>
<td valign="top"><a href="#profilemachine">ProfileMachine</a>!</td>
<td>

The profile machine object.

</td>
</tr>
</tbody>
</table>

### TranslateProfileParameterPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>profileParameter</strong></td>
<td valign="top"><a href="#profileparameter">ProfileParameter</a>!</td>
<td>

The profile parameter object.

</td>
</tr>
</tbody>
</table>

### TranslateThresholdRulePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>rule</strong></td>
<td valign="top"><a href="#thresholdrule">ThresholdRule</a>!</td>
<td>

The ThresholdRule Object

</td>
</tr>
</tbody>
</table>

### Translation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Language code, e.g., "en-US" and "zh-TW"

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Translated text

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Translation created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The Translation updated at.

</td>
</tr>
</tbody>
</table>

### TranslationLang

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Language code, e.g., "en-US" and "zh-TW"

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Language name, e.g., "English" and "繁體中文"

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Indicates whether the translation language is default language

</td>
</tr>
</tbody>
</table>

### TurnOffAcquiringPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>tag</strong></td>
<td valign="top"><a href="#tag">Tag</a>!</td>
<td>

The Tag Object

</td>
</tr>
</tbody>
</table>

### TurnOffSaveToHistoryPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>tag</strong></td>
<td valign="top"><a href="#tag">Tag</a>!</td>
<td>

The Tag Object

</td>
</tr>
</tbody>
</table>

### TurnOnAcquiringPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>tag</strong></td>
<td valign="top"><a href="#tag">Tag</a>!</td>
<td>

The Tag Object

</td>
</tr>
</tbody>
</table>

### UnbindGatewayPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>unboundCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The unbound count of items.

</td>
</tr>
</tbody>
</table>

### UnbindModbusParameterDataSourcePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>unboundCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The unbound count of items.

</td>
</tr>
</tbody>
</table>

### UnbindOpcUaParameterDataSourceByServerPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>unboundCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The unbound count of items.

</td>
</tr>
</tbody>
</table>

### UnbindOpcUaParameterDataSourceBySubscriptionTagPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>unboundCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The unbound count of items.

</td>
</tr>
</tbody>
</table>

### UnbindParametersByTagPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>unboundCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The unbound count of items.

</td>
</tr>
</tbody>
</table>

### UnbindParametersMappingPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameters</strong></td>
<td valign="top">[<a href="#parameter">Parameter</a>!]!</td>
<td>

The parameters.

</td>
</tr>
</tbody>
</table>

### UpdateAlarmCodeEventPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>event</strong></td>
<td valign="top"><a href="#alarmcodeevent">AlarmCodeEvent</a>!</td>
<td>

The AlarmCodeEvent Object

</td>
</tr>
</tbody>
</table>

### UpdateAlarmLevelPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>alarmLevel</strong></td>
<td valign="top"><a href="#alarmlevel">AlarmLevel</a>!</td>
<td>

The AlarmLevel Object

</td>
</tr>
</tbody>
</table>

### UpdateClientPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>client</strong></td>
<td valign="top"><a href="#client">Client</a>!</td>
<td>

The Client Object.

</td>
</tr>
</tbody>
</table>

### UpdateCodeRulePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>rule</strong></td>
<td valign="top"><a href="#coderule">CodeRule</a>!</td>
<td>

The CodeRule Object

</td>
</tr>
</tbody>
</table>

### UpdateEnergyDeviceKindPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyDeviceKind</strong></td>
<td valign="top"><a href="#energydevicekind">EnergyDeviceKind</a>!</td>
<td>

The energy device kind object.

</td>
</tr>
</tbody>
</table>

### UpdateEnergyUsageInterfaceNamePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>interface</strong></td>
<td valign="top"><a href="#energyusageinterface">EnergyUsageInterface</a>!</td>
<td>

The energy usage interface.

</td>
</tr>
</tbody>
</table>

### UpdateEnergyUsageInterfaceUsagesPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>interface</strong></td>
<td valign="top"><a href="#energyusageinterface">EnergyUsageInterface</a>!</td>
<td>

The energy usage interface.

</td>
</tr>
</tbody>
</table>

### UpdateEnergyUsagePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyUsage</strong></td>
<td valign="top"><a href="#energyusage">EnergyUsage</a>!</td>
<td>

The energy usage object.

</td>
</tr>
</tbody>
</table>

### UpdateGatewayPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>gateway</strong></td>
<td valign="top"><a href="#gateway">Gateway</a>!</td>
<td>

The Gateway Object

</td>
</tr>
</tbody>
</table>

### UpdateGroupPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#group">Group</a>!</td>
<td>

The group object.

</td>
</tr>
</tbody>
</table>

### UpdateHighLowEventPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>event</strong></td>
<td valign="top"><a href="#highlowevent">HighLowEvent</a>!</td>
<td>

The HighLowEvent Object

</td>
</tr>
</tbody>
</table>

### UpdateIAppGroupIndexesPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iAppGroups</strong></td>
<td valign="top">[<a href="#iappgroup">IAppGroup</a>!]!</td>
<td>

The updated I.App Group objects.

</td>
</tr>
</tbody>
</table>

### UpdateIAppGroupPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iAppGroup</strong></td>
<td valign="top"><a href="#iappgroup">IAppGroup</a>!</td>
<td>

The I.App Group object.

</td>
</tr>
</tbody>
</table>

### UpdateIAppIndexesPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iApps</strong></td>
<td valign="top">[<a href="#iapp">IApp</a>!]!</td>
<td>

The updated I.App objects.

</td>
</tr>
</tbody>
</table>

### UpdateIAppMenuItemIndexesPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iAppMenuItems</strong></td>
<td valign="top">[<a href="#iappmenuitem">IAppMenuItem</a>!]!</td>
<td>

The updated I.App menu item objects.

</td>
</tr>
</tbody>
</table>

### UpdateIAppMenuItemPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iAppMenuItem</strong></td>
<td valign="top"><a href="#iappmenuitem">IAppMenuItem</a>!</td>
<td>

The I.App menu item object.

</td>
</tr>
</tbody>
</table>

### UpdateIAppPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>iApp</strong></td>
<td valign="top"><a href="#iapp">IApp</a>!</td>
<td>

The I.App object.

</td>
</tr>
</tbody>
</table>

### UpdateKpiMetricPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kpiMetric</strong></td>
<td valign="top"><a href="#kpimetric">KpiMetric</a>!</td>
<td>

The KPI metric object.

</td>
</tr>
</tbody>
</table>

### UpdateKpiParameterUsagePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kpiParameterUsage</strong></td>
<td valign="top"><a href="#kpiparameterusage">KpiParameterUsage</a>!</td>
<td>

The kpi parameter usage object.

</td>
</tr>
</tbody>
</table>

### UpdateMachinePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#machine">Machine</a>!</td>
<td>

The machine object.

</td>
</tr>
</tbody>
</table>

### UpdateMachineStatusPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machineStatus</strong></td>
<td valign="top"><a href="#machinestatus">MachineStatus</a>!</td>
<td>

The machine status object.

</td>
</tr>
</tbody>
</table>

### UpdateMachineTypeCategoryPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machineTypeCategory</strong></td>
<td valign="top"><a href="#machinetypecategory">MachineTypeCategory</a>!</td>
<td>

The machine type category object.

</td>
</tr>
</tbody>
</table>

### UpdateMachineTypePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machineType</strong></td>
<td valign="top"><a href="#machinetype">MachineType</a>!</td>
<td>

The machine type object.

</td>
</tr>
</tbody>
</table>

### UpdateMarqueeActionPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>action</strong></td>
<td valign="top"><a href="#marqueeaction">MarqueeAction</a>!</td>
<td>

The MarqueeAction Object

</td>
</tr>
</tbody>
</table>

### UpdateMePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a>!</td>
<td>

The User Object.

</td>
</tr>
</tbody>
</table>

### UpdateNotificationActionPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>action</strong></td>
<td valign="top"><a href="#notificationaction">NotificationAction</a>!</td>
<td>

The NotificationAction Object

</td>
</tr>
</tbody>
</table>

### UpdateOutboundPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>outbound</strong></td>
<td valign="top"><a href="#outbound">Outbound</a>!</td>
<td>

The Outbound object.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>error</strong></td>
<td valign="top"><a href="#jsonobject">JSONObject</a></td>
<td></td>
</tr>
</tbody>
</table>

### UpdateParameterDataSourcePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>updatedCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The Updated Count of Items

</td>
</tr>
</tbody>
</table>

### UpdateParameterInterfaceCalculationSettingsPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>interface</strong></td>
<td valign="top"><a href="#parameterinterface">ParameterInterface</a>!</td>
<td>

The parameter interface.

</td>
</tr>
</tbody>
</table>

### UpdateParameterInterfaceNamePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>interface</strong></td>
<td valign="top"><a href="#parameterinterface">ParameterInterface</a>!</td>
<td>

The parameter interface.

</td>
</tr>
</tbody>
</table>

### UpdateParameterInterfaceParameterNamesAndKeyPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>interface</strong></td>
<td valign="top"><a href="#parameterinterface">ParameterInterface</a>!</td>
<td>

The parameter interface.

</td>
</tr>
</tbody>
</table>

### UpdateParameterMappingCodePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>mappingCode</strong></td>
<td valign="top"><a href="#parametermappingcode">ParameterMappingCode</a>!</td>
<td>

The parameter mapping code.

</td>
</tr>
</tbody>
</table>

### UpdateParameterMappingRulePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>mappingRule</strong></td>
<td valign="top"><a href="#parametermappingrule">ParameterMappingRule</a>!</td>
<td>

The parameter mapping rule.

</td>
</tr>
</tbody>
</table>

### UpdateParameterPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#parameter">Parameter</a>!</td>
<td>

The parameter object.

</td>
</tr>
</tbody>
</table>

### UpdateProfileMachinePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>profileMachine</strong></td>
<td valign="top"><a href="#profilemachine">ProfileMachine</a>!</td>
<td>

The profile machine object.

</td>
</tr>
</tbody>
</table>

### UpdateProfileParameterPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>profileParameter</strong></td>
<td valign="top"><a href="#profileparameter">ProfileParameter</a>!</td>
<td>

The profile parameter object.

</td>
</tr>
</tbody>
</table>

### UpdateRolePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#role">Role</a>!</td>
<td>

The Role object.

</td>
</tr>
</tbody>
</table>

### UpdateThresholdRulePayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>rule</strong></td>
<td valign="top"><a href="#thresholdrule">ThresholdRule</a>!</td>
<td>

The ThresholdRule Object

</td>
</tr>
</tbody>
</table>

### UpdateTimeOfUsePeriodPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>timeOfUsePeriod</strong></td>
<td valign="top"><a href="#timeofuseperiod">TimeOfUsePeriod</a>!</td>
<td>

The Time-of-Use Period object.

</td>
</tr>
</tbody>
</table>

### UpdateTranslationLangPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>translationLang</strong></td>
<td valign="top"><a href="#translationlang">TranslationLang</a>!</td>
<td>

The translation lang object.

</td>
</tr>
</tbody>
</table>

### UpdateUserPayload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a>!</td>
<td>

The User Object.

</td>
</tr>
</tbody>
</table>

### User

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The name for display.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>username</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The username used for login.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roles</strong></td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

The roles key this user belongs to.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roleDetails</strong></td>
<td valign="top">[<a href="#role">Role</a>!]!</td>
<td>

The roles this user belongs to.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupUsers</strong></td>
<td valign="top">[<a href="#groupuser">GroupUser</a>!]!</td>
<td>

The GroupUsers this user bound to.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tenantGroups</strong></td>
<td valign="top">[<a href="#group">Group</a>!]!</td>
<td>

The Tenant Groups this user bound to.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>active</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Only active user can sign-in.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>loggedInAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The timestamp when this user account last logged in.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>logInFailedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The timestamp when this user account last log in failed.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The user created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The user updated at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>wisePaasUserId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The WISE-PaaS SSO user id.

</td>
</tr>
</tbody>
</table>

## Inputs

### AddAlarmCodeEventInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Name of the Alarm Code Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Description of the Alarm Code Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Machine of the Parameter

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Parameter to Bind

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codeRules</strong></td>
<td valign="top">[<a href="#coderuleinput">CodeRuleInput</a>!]!</td>
<td>

The Code Rules of the Alarm Code Event

</td>
</tr>
</tbody>
</table>

### AddAlarmLevelInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Name of the AlarmLevel

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>level</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

The Priority of the AlarmLevel, Larger Means Higher

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>color</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Color of the AlarmLevel

</td>
</tr>
</tbody>
</table>

### AddClientInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roles</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>active</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### AddCodeRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

AlarmLevel that this Code Rule Related to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codeId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Code Node ID that this Code Rule Belongs to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>eventId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Alarm Code Event

</td>
</tr>
</tbody>
</table>

### AddEhsUsageInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usageType</strong></td>
<td valign="top"><a href="#ehsusagetype">EhsUsageType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceParameterId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddEnergyDeviceKindInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### AddEnergyUsageInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### AddEnergyUsageInterfaceInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usages</strong></td>
<td valign="top">[<a href="#energyusageinterfaceusageinput">EnergyUsageInterfaceUsageInput</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### AddFormulaEntry

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>isRoot</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rootFormulaId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operator</strong></td>
<td valign="top"><a href="#operator">Operator</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandLKind</strong></td>
<td valign="top"><a href="#operandkind">OperandKind</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandLConstant</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandLFormulaId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandLVar</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandLVars</strong></td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandRKind</strong></td>
<td valign="top"><a href="#operandkind">OperandKind</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandRConstant</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandRFormulaId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandRVar</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operandRVars</strong></td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td></td>
</tr>
</tbody>
</table>

### AddFormulasInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>formulas</strong></td>
<td valign="top">[<a href="#addformulaentry">AddFormulaEntry</a>]!</td>
<td>

The formulas to be added with binary tree array representation.

</td>
</tr>
</tbody>
</table>

### AddGatewayInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Nickname of the Gateway

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#gatewaykind">GatewayKind</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### AddGroupInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeZone</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>coordinate</strong></td>
<td valign="top"><a href="#coordinateinput">CoordinateInput</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isTenant</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

This Group is a Tenant Group or not.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>owner</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group owner.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group phone.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phoneCountryCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group phone country code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#emailaddress">EmailAddress</a></td>
<td>

The group email.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group address.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>city</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group city.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>state</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group state.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>postalCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group postal code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group country code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddHighLowEventInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Name of the High-Low Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Description of the High-Low Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Machine of the Parameter

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Parameter to Bind

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>thresholdRules</strong></td>
<td valign="top">[<a href="#thresholdruleinput">ThresholdRuleInput</a>!]!</td>
<td>

The Threshold Rules of the High-Low Event

</td>
</tr>
</tbody>
</table>

### AddIAppGroupInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>display</strong></td>
<td valign="top"><a href="#iappdisplay">IAppDisplay</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### AddIAppInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>link</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iconUrl</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>thumbnailUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>headerLogoUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>headerTitle</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>display</strong></td>
<td valign="top"><a href="#iappdisplay">IAppDisplay</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>defaultKey</strong></td>
<td valign="top"><a href="#defaultiappkey">DefaultIAppKey</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddIAppMenuItemInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>link</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>display</strong></td>
<td valign="top"><a href="#iappdisplay">IAppDisplay</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iAppId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddKpiMetricInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulaId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>defaultFormulaId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schedule</strong></td>
<td valign="top"><a href="#pointschedule">PointSchedule</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dailyHour</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td>

The hour to run with daily schedule, 0 ~ 23.

</td>
</tr>
</tbody>
</table>

### AddKpiParameterUsageInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>index</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddKpiParameterUsageRelationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>usageId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### AddLicenseInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>sign</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>data</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### AddMachineInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>imageUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>criticality</strong></td>
<td valign="top"><a href="#machinecriticality">MachineCriticality</a></td>
<td>

The machine criticality.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>owner</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine owner.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>purchaseDate</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine purchase date.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>purchaseCurrency</strong></td>
<td valign="top"><a href="#currency">Currency</a></td>
<td>

The machine purchase currency.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>purchaseAmount</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td>

The machine purchase amount.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>onlineDate</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine online date.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>manufacturer</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine manufacturer.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine model name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>contactPersonName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine contact person name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>supportPhone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine support phone.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>supportPhoneCountryCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine support phone country code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>supportEmail</strong></td>
<td valign="top"><a href="#emailaddress">EmailAddress</a></td>
<td>

The machine support email.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineType</strong></td>
<td valign="top"><a href="#machinetypeinput">MachineTypeInput</a></td>
<td>

The machine type.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>serialNumber</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The machine serial number.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isStation</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Add as Station or as Machine, omit it for Machine.

</td>
</tr>
</tbody>
</table>

### AddMachineStatusInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>index</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>color</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddMachineTypeCategoryInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### AddMachineTypeInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### AddMarqueeActionInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>ruleId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Rule

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether this Marquee Action is Active

</td>
</tr>
</tbody>
</table>

### AddNotificationActionInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>ruleId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Rule

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether this Notification Action is Active

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Group ID of the Notification Action

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subject</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Mail Subject of the Notification Action

</td>
</tr>
</tbody>
</table>

### AddOutboundInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>url</strong></td>
<td valign="top"><a href="#url">URL</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>allowUnauthorized</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>active</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>format</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddParameterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>valueType</strong></td>
<td valign="top"><a href="#tagvaluetype">TagValueType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#parameterkind">ParameterKind</a></td>
<td>

The parameter kind.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>metricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the parameter metric.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>opcUaTag</strong></td>
<td valign="top"><a href="#opcuataginput">OpcUaTagInput</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>modbusTag</strong></td>
<td valign="top"><a href="#modbustaginput">ModbusTagInput</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddParameterInterfaceInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterNames</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>calculationSettings</strong></td>
<td valign="top">[<a href="#calculationsettinginput">CalculationSettingInput</a>!]</td>
<td></td>
</tr>
</tbody>
</table>

### AddParameterMappingCodesEntry

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>message</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>translations</strong></td>
<td valign="top">[<a href="#parametermappingcodetranslationentry">ParameterMappingCodeTranslationEntry</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddParameterMappingCodesInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>ruleId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codes</strong></td>
<td valign="top">[<a href="#addparametermappingcodesentry">AddParameterMappingCodesEntry</a>!]!</td>
<td>

The parameter mapping codes.

</td>
</tr>
</tbody>
</table>

### AddParameterMappingRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pType</strong></td>
<td valign="top"><a href="#tagvaluetype">TagValueType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codes</strong></td>
<td valign="top">[<a href="#addparametermappingcodesentry">AddParameterMappingCodesEntry</a>!]</td>
<td>

The parameter mapping codes.

</td>
</tr>
</tbody>
</table>

### AddProfileMachineInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>imageUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddRoleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### AddThresholdRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

AlarmLevel that this Threshold Rule Related to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

Value of the Threshold Rule

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether this Threshold Rule is Active

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmMessage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Alarm Message of the Threshold Rule

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#thresholdtype">ThresholdType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>eventId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the High-Low Event

</td>
</tr>
</tbody>
</table>

### AddTimeOfUsePeriodInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>energyDeviceKind</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>periodType</strong></td>
<td valign="top"><a href="#periodtype">PeriodType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>day</strong></td>
<td valign="top"><a href="#dayofweek">DayOfWeek</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startTime</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>endTime</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### AddTranslationLangInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Only first translation lang would be true.

</td>
</tr>
</tbody>
</table>

### AddUserInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Name for display.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>username</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Specify password, or leave unspecified/empty to auto-generate.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roles</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>active</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Defaults to true.

</td>
</tr>
</tbody>
</table>

### ApplyParameterMappingToParametersInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter mapping rule to apply.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

The Node ID of parameters to apply.

</td>
</tr>
</tbody>
</table>

### AssociateInboundGroupInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gatewayId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the associated gateway.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inboundAssociation</strong></td>
<td valign="top"><a href="#inboundassociation">InboundAssociation</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### BulkSetGenericKpiEntry

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>relationKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#generickpikind">GenericKpiKind</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>year</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupNodeId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineNodeId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterNodeId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpi</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hour</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td></td>
</tr>
</tbody>
</table>

### BulkSetGenericKpiListInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>list</strong></td>
<td valign="top">[<a href="#bulksetgenerickpientry">BulkSetGenericKpiEntry</a>!]!</td>
<td>

The Generic KPI List to set.

</td>
</tr>
</tbody>
</table>

### CalculationPointOperandInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>variable</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceParameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The source Parameter id to calculate.

</td>
</tr>
</tbody>
</table>

### CalculationSettingInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>function</strong></td>
<td valign="top"><a href="#calculationsettingfunction">CalculationSettingFunction</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### ChangeMyPasswordInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Provide original password.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>newPassword</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Specify new password.

</td>
</tr>
</tbody>
</table>

### CodeRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

AlarmLevel that this Code Rule Related to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>codeId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Code Node ID that this Code Rule Belongs to

</td>
</tr>
</tbody>
</table>

### CoordinateInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>longitude</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>latitude</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### CreateFirstAdminInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Name for display.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>username</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Specify password, or leave unspecified/empty to auto-generate.

</td>
</tr>
</tbody>
</table>

### DeleteValuesInRangeInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>from</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>to</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### DisableAlertsInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alerts</strong></td>
<td valign="top">[<a href="#generickpialertentry">GenericKpiAlertEntry</a>!]!</td>
<td>

The Generic KPI Alerts to enable or disable.

</td>
</tr>
</tbody>
</table>

### DropConstantParameterValuesInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### DropPredictionPointValuesInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>pointObjectId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### DryRunTransformationPointInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

If the Transformation Parameter not yet created, fill a zero-length empty string here.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceParameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schedule</strong></td>
<td valign="top"><a href="#pointschedule">PointSchedule</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dailyHour</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td>

The hour to run with daily schedule, 0 ~ 23.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fn</strong></td>
<td valign="top"><a href="#transformationpointfn">TransformationPointFn</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>diffRule</strong></td>
<td valign="top"><a href="#transformationdiffruleinput">TransformationDiffRuleInput</a></td>
<td>

The Transformation Diff function rules.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>now</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td>

Specify moment on timeline to dry run.

</td>
</tr>
</tbody>
</table>

### EnableAlertsInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alerts</strong></td>
<td valign="top">[<a href="#generickpialertentry">GenericKpiAlertEntry</a>!]!</td>
<td>

The Generic KPI Alerts to enable or disable.

</td>
</tr>
</tbody>
</table>

### EnergySplitOperandEntry

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>variable</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ratio</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Machine id to calculate Split.

</td>
</tr>
</tbody>
</table>

### EnergyUsageInterfaceUsageInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### FullOutboundIfpCfgInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>outboundIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]</td>
<td></td>
</tr>
</tbody>
</table>

### GenerateEHSKpiConfigData

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]</td>
<td></td>
</tr>
</tbody>
</table>

### GenerateEHSKpiConfigInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>relationKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Set relationKey on EHS KPI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kinds</strong></td>
<td valign="top">[<a href="#generateehskpiconfigkind">GenerateEHSKpiConfigKind</a>!]!</td>
<td>

The kind of EHS KPI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>year</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

The year of EHS KPI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>data</strong></td>
<td valign="top"><a href="#generateehskpiconfigdata">GenerateEHSKpiConfigData</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dataSource</strong></td>
<td valign="top"><a href="#generateehskpiconfigdatasource">GenerateEHSKpiConfigDataSource</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeRange</strong></td>
<td valign="top">[<a href="#nonnegativeint">NonNegativeInt</a>!]!</td>
<td>

Which years to generate.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>from</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>to</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dataFunc</strong></td>
<td valign="top"><a href="#generateehskpiconfigdatafunction">GenerateEHSKpiConfigDataFunction</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### GenericKpiAlertEntry

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>relationKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Set relationKey on Generic KPI and Alert.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>year</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

The year of Generic KPI and Alert, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#generickpialertkind">GenericKpiAlertKind</a>!</td>
<td>

The kind of Generic KPI Alert, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
</tbody>
</table>

### GroupCollection

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#ifpcfgitem">IfpCfgItem</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removed</strong></td>
<td valign="top">[<a href="#ifpcfgitem">IfpCfgItem</a>!]</td>
<td></td>
</tr>
</tbody>
</table>

### IfpCfgCollection

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#ifpcfgitem">IfpCfgItem</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removed</strong></td>
<td valign="top">[<a href="#ifpcfgitem">IfpCfgItem</a>!]</td>
<td></td>
</tr>
</tbody>
</table>

### IfpCfgItem

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>item</strong></td>
<td valign="top"><a href="#jsonobject">JSONObject</a></td>
<td></td>
</tr>
</tbody>
</table>

### InboundIfpCfgInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>gatewayId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#ifpcfgcollection">IfpCfgCollection</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#ifpcfgcollection">IfpCfgCollection</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#ifpcfgcollection">IfpCfgCollection</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### MachineCollection

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#ifpcfgitem">IfpCfgItem</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removed</strong></td>
<td valign="top">[<a href="#ifpcfgitem">IfpCfgItem</a>!]</td>
<td></td>
</tr>
</tbody>
</table>

### MachineTypeInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### ModbusTagInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>serverId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dataType</strong></td>
<td valign="top"><a href="#urmodbusdatatype">URModbusDataType</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### MoveMachineInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the machine to move.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the target group to be moved.

</td>
</tr>
</tbody>
</table>

### OpcUaTagInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>serverId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subscriptionTagId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### OutboundIfpCfgInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>outboundIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#groupcollection">GroupCollection</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machine</strong></td>
<td valign="top"><a href="#machinecollection">MachineCollection</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameter</strong></td>
<td valign="top"><a href="#parametercollection">ParameterCollection</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### ParameterCollection

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#parameteritem">ParameterItem</a>!]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removed</strong></td>
<td valign="top">[<a href="#parameteritem">ParameterItem</a>!]</td>
<td></td>
</tr>
</tbody>
</table>

### ParameterItem

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>item</strong></td>
<td valign="top"><a href="#jsonobject">JSONObject</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>valueType</strong></td>
<td valign="top"><a href="#tagvaluetype">TagValueType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceTag</strong></td>
<td valign="top"><a href="#sourcetag">SourceTag</a></td>
<td></td>
</tr>
</tbody>
</table>

### ParameterMappingCodeTranslationEntry

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The translation language.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>message</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The parameter mapping code message translation. If not set or empty string, the existing parameter mapping code message translation will be deleted.

</td>
</tr>
</tbody>
</table>

### ParameterOrder

Ordering options for parameter connections.

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>direction</strong></td>
<td valign="top"><a href="#orderdirection">OrderDirection</a>!</td>
<td>

A cursor for use in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>field</strong></td>
<td valign="top"><a href="#parameterorderfield">ParameterOrderField</a></td>
<td>

The field to order items by.

</td>
</tr>
</tbody>
</table>

### PointValueItem

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>str</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>detail</strong></td>
<td valign="top"><a href="#jsonobject">JSONObject</a></td>
<td></td>
</tr>
</tbody>
</table>

### RemoveAlarmCodeEventInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Alarm Code Event to Remove

</td>
</tr>
</tbody>
</table>

### RemoveAlarmLevelInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the AlarmLevel to Remove

</td>
</tr>
</tbody>
</table>

### RemoveAlarmLevelTranslationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Alarm Level to Modify

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveCalculationPointInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveClientInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Client to remove.

</td>
</tr>
</tbody>
</table>

### RemoveCodeRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Code Rule to Remove

</td>
</tr>
</tbody>
</table>

### RemoveCodeRulesByCodeIdsInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>codeIds</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

The Mapping Code Node IDs of the Code Rules to Remove

</td>
</tr>
</tbody>
</table>

### RemoveEhsUsageInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the usage to modify.

</td>
</tr>
</tbody>
</table>

### RemoveEnergyDeviceKindInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveEnergyDeviceKindTranslationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Energy Device Kind to Modify

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveEnergyUsageInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveEnergyUsageInterfaceInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the energy usage interface to remove.

</td>
</tr>
</tbody>
</table>

### RemoveEnergyUsageTranslationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Energy Usage to Modify

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveEventByParameterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Parameter Node ID of the Event to Remove

</td>
</tr>
</tbody>
</table>

### RemoveFormulaInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the formula to modify.

</td>
</tr>
</tbody>
</table>

### RemoveGatewayInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Gateway to Remove

</td>
</tr>
</tbody>
</table>

### RemoveGenericKpiEntry

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>relationKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Set relationKey on Generic KPI and Alert.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>year</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

The year of Generic KPI and Alert, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#generickpikind">GenericKpiKind</a>!</td>
<td>

The kind of Generic KPI, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hours</strong></td>
<td valign="top">[<a href="#nonnegativeint">NonNegativeInt</a>!]</td>
<td>

For `Hour...` kinds, fill this field if you just want to remove certain hour, omit this field will remove all hours.

</td>
</tr>
</tbody>
</table>

### RemoveGenericKpiListInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpiList</strong></td>
<td valign="top">[<a href="#removegenerickpientry">RemoveGenericKpiEntry</a>!]!</td>
<td>

The Generic KPI List to remove.

</td>
</tr>
</tbody>
</table>

### RemoveGroupInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the group to modify.

</td>
</tr>
</tbody>
</table>

### RemoveGroupTranslationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the group to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveHighLowEventInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the High-Low Event to Remove

</td>
</tr>
</tbody>
</table>

### RemoveHolidaysInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>isoDates</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

The full date of Generic KPI Holiday.

</td>
</tr>
</tbody>
</table>

### RemoveIAppGroupInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App Group to modify.

</td>
</tr>
</tbody>
</table>

### RemoveIAppGroupTranslationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App Group to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveIAppInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App to modify.

</td>
</tr>
</tbody>
</table>

### RemoveIAppMenuItemInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App menu item to modify.

</td>
</tr>
</tbody>
</table>

### RemoveIAppMenuItemTranslationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App menu item to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveIAppTranslationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveKpiMetricInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the KPI metric to modify.

</td>
</tr>
</tbody>
</table>

### RemoveKpiMetricTranslationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the kpi metric to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveKpiParameterUsageInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the kpi parameter usage to modify.

</td>
</tr>
</tbody>
</table>

### RemoveKpiParameterUsageRelationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>usageId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveLicenseInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the license to remove.

</td>
</tr>
</tbody>
</table>

### RemoveMachineInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the machine to modify.

</td>
</tr>
</tbody>
</table>

### RemoveMachineStatusInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the machine status to remove.

</td>
</tr>
</tbody>
</table>

### RemoveMachineTranslationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the machine to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveMachineTypeCategoryInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveMachineTypeInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveMarqueeActionInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Marquee Action to Remove

</td>
</tr>
</tbody>
</table>

### RemoveNotificationActionInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Notification Action to Remove

</td>
</tr>
</tbody>
</table>

### RemoveOutboundInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Outbound to remove.

</td>
</tr>
</tbody>
</table>

### RemoveParameterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter to modify.

</td>
</tr>
</tbody>
</table>

### RemoveParameterInterfaceInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter interface to remove.

</td>
</tr>
</tbody>
</table>

### RemoveParameterMappingCodeInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter mapping code to remove.

</td>
</tr>
</tbody>
</table>

### RemoveParameterMappingRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter mapping rule to remove.

</td>
</tr>
</tbody>
</table>

### RemoveParameterTranslationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveRoleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveThresholdRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Threshold Rule to Remove

</td>
</tr>
</tbody>
</table>

### RemoveThresholdRuleTranslationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Threshold Rule to Modify

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveTimeOfUsePeriodInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Time-of-Use period to modify.

</td>
</tr>
</tbody>
</table>

### RemoveTransformationPointInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### RemoveTranslationLangInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the translation lang to modify.

</td>
</tr>
</tbody>
</table>

### RemoveTranslationsByLangInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Language of Translations to Remove

</td>
</tr>
</tbody>
</table>

### SetAsHolidaysInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>isoDates</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td>

The full date of Generic KPI Holiday.

</td>
</tr>
</tbody>
</table>

### SetCalculationPointInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulaId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operands</strong></td>
<td valign="top">[<a href="#calculationpointoperandinput">CalculationPointOperandInput</a>!]!</td>
<td>

The Calculation operands.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schedule</strong></td>
<td valign="top"><a href="#pointschedule">PointSchedule</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dailyHour</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td>

The hour to run with daily schedule, 0 ~ 23.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>initialValue</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
</tbody>
</table>

### SetConstantParameterValuesInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pointValues</strong></td>
<td valign="top">[<a href="#pointvalueitem">PointValueItem</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### SetDHAclInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scope</strong></td>
<td valign="top"><a href="#dhaclscope">DHAclScope</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mode</strong></td>
<td valign="top"><a href="#aclmode">AclMode</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### SetElectricityRateContractInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>country</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The country of Electricity Rate Contract.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>province</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The province of Electricity Rate Contract.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#electricityratekind">ElectricityRateKind</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>classification</strong></td>
<td valign="top"><a href="#electricityrateclassification">ElectricityRateClassification</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startTime</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#jsonobject">JSONObject</a></td>
<td></td>
</tr>
</tbody>
</table>

### SetEnergyDeviceInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usage</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isExclude</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
</tbody>
</table>

### SetEnergyKpiInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandJan</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandFeb</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandMar</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandApr</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandMay</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandJun</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandJul</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandAug</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandSep</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandOct</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandNov</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxDemandDec</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionJan</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionFeb</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionMar</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionApr</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionMay</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionJun</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionJul</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionAug</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionSep</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionOct</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionNov</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxConsumptionDec</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandJan</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandFeb</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandMar</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandApr</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandMay</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandJun</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandJul</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandAug</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandSep</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandOct</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandNov</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedDemandDec</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionJan</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionFeb</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionMar</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionApr</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionMay</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionJun</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionJul</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionAug</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionSep</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionOct</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionNov</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAllowedConsumptionDec</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
</tbody>
</table>

### SetEnergySplitInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulaId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>operands</strong></td>
<td valign="top">[<a href="#energysplitoperandentry">EnergySplitOperandEntry</a>!]</td>
<td>

The Split operands.

</td>
</tr>
</tbody>
</table>

### SetGenericKpiEntry

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>relationKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Set relationKey on Generic KPI and Alert.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>year</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

The year of Generic KPI and Alert, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#generickpikind">GenericKpiKind</a>!</td>
<td>

The kind of Generic KPI, for kind `Unlimited` please set `year` to zero.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpi</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

Fill this field for non `Hour...` kinds, Omit this field for `Hour...` kinds.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>byHours</strong></td>
<td valign="top">[<a href="#setgenerickpientrybyhour">SetGenericKpiEntryByHour</a>!]</td>
<td>

Fill this field for `Hour...` kinds, will be ignored if non `Hour...` kinds `kpi` field is filled.

</td>
</tr>
</tbody>
</table>

### SetGenericKpiEntryByHour

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>hour</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td>

The hour of Generic KPI, only applicable to `Hour...` kinds.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpi</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### SetGenericKpiListInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpiList</strong></td>
<td valign="top">[<a href="#setgenerickpientry">SetGenericKpiEntry</a>!]!</td>
<td>

The Generic KPI List to set.

</td>
</tr>
</tbody>
</table>

### SetGroupEnergyUsageInterfaceRelationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyUsageInterfaceIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]</td>
<td></td>
</tr>
</tbody>
</table>

### SetGroupParameterInterfaceRelationInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterInterfaceIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]</td>
<td></td>
</tr>
</tbody>
</table>

### SetGroupUserInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roles</strong></td>
<td valign="top">[<a href="#string">String</a>!]</td>
<td>

Omit roles to remove GroupUser.

</td>
</tr>
</tbody>
</table>

### SetI18nLangInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The target i18n language code, e.g., "en-US" and "zh-TW"

</td>
</tr>
</tbody>
</table>

### SetMyTenantInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

ID of the Tenant Group. Omit to clear the Tenant Group selection.

</td>
</tr>
</tbody>
</table>

### SetOrgAclInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scope</strong></td>
<td valign="top"><a href="#orgaclscope">OrgAclScope</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mode</strong></td>
<td valign="top"><a href="#aclmode">AclMode</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### SetProductionKpiInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>year</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

The year of Production KPI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Jan</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Feb</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Mar</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Apr</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>May</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Jun</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Jul</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Aug</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Sep</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Oct</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Nov</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>Dec</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>avg</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td></td>
</tr>
</tbody>
</table>

### SetREAclInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>scope</strong></td>
<td valign="top"><a href="#reaclscope">REAclScope</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mode</strong></td>
<td valign="top"><a href="#aclmode">AclMode</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### SetStatusExpirationPeriodInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>statusExpirationPeriod</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Gateway Current Status Expiration Period in Seconds

</td>
</tr>
</tbody>
</table>

### SetTenantQuotaLimitInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>limit</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### SetTenantQuotaQuotaInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>tenantId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>quota</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### SetTransformationPointInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceParameterId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schedule</strong></td>
<td valign="top"><a href="#pointschedule">PointSchedule</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dailyHour</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td>

The hour to run with daily schedule, 0 ~ 23.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fn</strong></td>
<td valign="top"><a href="#transformationpointfn">TransformationPointFn</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>diffRule</strong></td>
<td valign="top"><a href="#transformationdiffruleinput">TransformationDiffRuleInput</a></td>
<td>

The Transformation Diff function rules.

</td>
</tr>
</tbody>
</table>

### SetUserPasswordInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the user to set password.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Specify new password, or leave unspecified/empty to auto-generate.

</td>
</tr>
</tbody>
</table>

### SignInInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>username</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### SourceTag

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### ThresholdRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

AlarmLevel that this Threshold Rule Related to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

Value of the Threshold Rule

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether this Threshold Rule is Active

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmMessage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Alarm Message of the Threshold Rule

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#thresholdtype">ThresholdType</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### TransformationDiffRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>nonNegative</strong></td>
<td valign="top"><a href="#diffnonnegativecorrection">DiffNonNegativeCorrection</a></td>
<td>

The Correction to apply on Non Negative Diff.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>positivePercentThreshold</strong></td>
<td valign="top"><a href="#positiveint">PositiveInt</a></td>
<td>

The acceptable increment threshold in percent.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>negativePercentThreshold</strong></td>
<td valign="top"><a href="#positiveint">PositiveInt</a></td>
<td>

The acceptable decrement threshold in percent.

</td>
</tr>
</tbody>
</table>

### TranslateAlarmLevelInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Alarm Level to Modify

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### TranslateEnergyDeviceKindInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Energy Device Kind to Modify

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### TranslateEnergyUsageInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Energy Usage to Modify

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### TranslateGroupInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the group to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The translation language.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group name translation. If not set or empty string, the existing group name translation will be deleted.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group description translation. If not set or empty string, the existing group description translation will be deleted.

</td>
</tr>
</tbody>
</table>

### TranslateIAppGroupInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App Group to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The translation language.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The I.App group name translation. If not set or empty string, the existing I.App group name translation will be deleted.

</td>
</tr>
</tbody>
</table>

### TranslateIAppInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The translation language.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The I.App name translation. If not set or empty string, the existing I.App name translation will be deleted.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>headerLogoUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The I.App header logo URL translation. If not set or empty string, the existing I.App header logo URL translation will be deleted.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>headerTitle</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The I.App header title translation. If not set or empty string, the existing I.App header title translation will be deleted.

</td>
</tr>
</tbody>
</table>

### TranslateIAppMenuItemInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App menu item to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The translation language.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The I.App menu item name translation. If not set or empty string, the existing I.App menu item name translation will be deleted.

</td>
</tr>
</tbody>
</table>

### TranslateKpiMetricInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the KPI metric to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The translation language.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The KPI metric name translation. If not set or empty string, the existing KPI metric name translation will be deleted.

</td>
</tr>
</tbody>
</table>

### TranslateMachineInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the machine to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The translation language.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine name translation. If not set or empty string, the existing machine name translation will be deleted.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine description translation. If not set or empty string, the existing machine description translation will be deleted.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>imageUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine image URL translation. If not set or empty string, the existing machine image URL translation will be deleted.

</td>
</tr>
</tbody>
</table>

### TranslateMachineStatusInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the machine status to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The machine status name translation. If not set or empty string, the existing machine status name translation will be deleted.

</td>
</tr>
</tbody>
</table>

### TranslateParameterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The translation language.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The parameter description translation. If not set or empty string, the existing parameter description translation will be deleted.

</td>
</tr>
</tbody>
</table>

### TranslateParameterMappingCodeInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The translation language.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>message</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The parameter mapping code message translation. If not set or empty string, the existing parameter mapping code message translation will be deleted.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter mapping code to modify.

</td>
</tr>
</tbody>
</table>

### TranslatePeriodTypeInfoInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the period type to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The period type name translation. If not set or empty string, the existing period type name translation will be deleted.

</td>
</tr>
</tbody>
</table>

### TranslateThresholdRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Threshold Rule to Modify

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmMessage</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### TurnOffAcquiringInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### TurnOffSaveToHistoryInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### TurnOnAcquiringInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>saveToHistory</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Pass `true` to turn on saveToHistory.

</td>
</tr>
</tbody>
</table>

### UnbindGatewayInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>gatewayId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UnbindModbusParameterDataSourceInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>serverId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UnbindOpcUaParameterDataSourceByServerInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>serverId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UnbindOpcUaParameterDataSourceBySubscriptionTagInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>subscriptionTagId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UnbindParametersByTagInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### UnbindParametersMappingInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterIds</strong></td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

The Node ID of parameters to unbind parameter mapping rule.

</td>
</tr>
</tbody>
</table>

### UpdateAlarmCodeEventInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Alarm Code Event to Update

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateAlarmLevelInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the AlarmLevel to Update

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>color</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateClientInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roles</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>active</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateCodeRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Code Rule to Update

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateDHRetentionPolicyInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#dhretentionpolicykind">DHRetentionPolicyKind</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>duration</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateEnergyDeviceKindInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateEnergyUsageInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateEnergyUsageInterfaceNameInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateEnergyUsageInterfaceUsagesInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usages</strong></td>
<td valign="top">[<a href="#energyusageinterfaceusageinput">EnergyUsageInterfaceUsageInput</a>!]!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateGatewayInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Gateway to Update

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateGroupInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the group to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>timeZone</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>coordinate</strong></td>
<td valign="top"><a href="#coordinateinput">CoordinateInput</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>owner</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group owner.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group phone.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phoneCountryCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group phone country code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#emailaddress">EmailAddress</a></td>
<td>

The group email.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group address.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>city</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group city.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>state</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group state.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>postalCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group postal code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The group country code.

</td>
</tr>
</tbody>
</table>

### UpdateHighLowEventInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the High-Low Event to Update

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateIAppGroupIndexesInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>ids</strong></td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

The Node IDs of the I.App groups to modify.

</td>
</tr>
</tbody>
</table>

### UpdateIAppGroupInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App Group to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>display</strong></td>
<td valign="top"><a href="#iappdisplay">IAppDisplay</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateIAppIndexesInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>ids</strong></td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

The Node IDs of the I.Apps to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### UpdateIAppInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>link</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>iconUrl</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>thumbnailUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>headerLogoUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>headerTitle</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>display</strong></td>
<td valign="top"><a href="#iappdisplay">IAppDisplay</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>defaultKey</strong></td>
<td valign="top"><a href="#defaultiappkey">DefaultIAppKey</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### UpdateIAppMenuItemIndexesInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>ids</strong></td>
<td valign="top">[<a href="#id">ID</a>!]!</td>
<td>

The Node IDs of the I.App menu items to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### UpdateIAppMenuItemInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the I.App menu item to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>link</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>display</strong></td>
<td valign="top"><a href="#iappdisplay">IAppDisplay</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isHome</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### UpdateKpiMetricInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the KPI metric to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>formulaId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>defaultFormulaId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schedule</strong></td>
<td valign="top"><a href="#pointschedule">PointSchedule</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dailyHour</strong></td>
<td valign="top"><a href="#nonnegativeint">NonNegativeInt</a></td>
<td>

The hour to run with daily schedule, 0 ~ 23.

</td>
</tr>
</tbody>
</table>

### UpdateKpiParameterUsageInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the kpi parameter usage to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>index</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateMachineInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the machine to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>imageUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>criticality</strong></td>
<td valign="top"><a href="#machinecriticality">MachineCriticality</a></td>
<td>

The machine criticality.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>owner</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine owner.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>purchaseDate</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine purchase date.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>purchaseCurrency</strong></td>
<td valign="top"><a href="#currency">Currency</a></td>
<td>

The machine purchase currency.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>purchaseAmount</strong></td>
<td valign="top"><a href="#positivefloat">PositiveFloat</a></td>
<td>

The machine purchase amount.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>onlineDate</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine online date.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>manufacturer</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine manufacturer.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine model name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>contactPersonName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine contact person name.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>supportPhone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine support phone.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>supportPhoneCountryCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

The machine support phone country code.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>supportEmail</strong></td>
<td valign="top"><a href="#emailaddress">EmailAddress</a></td>
<td>

The machine support email.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineType</strong></td>
<td valign="top"><a href="#machinetypeinput">MachineTypeInput</a></td>
<td>

The machine type.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>serialNumber</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The machine serial number.

</td>
</tr>
</tbody>
</table>

### UpdateMachineStatusInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the machine status to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>color</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateMachineTypeCategoryInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>oldKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>newKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateMachineTypeInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>oldKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>newKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentKey</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateMarqueeActionInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Marquee Action to Update

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateMeInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Name for display.

</td>
</tr>
</tbody>
</table>

### UpdateNotificationActionInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Notification Action to Update

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subject</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateOutboundInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Outbound to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>url</strong></td>
<td valign="top"><a href="#url">URL</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>allowUnauthorized</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>active</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sourceId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateParameterDataSourceInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Parameter Node ID to Update

</td>
</tr>
</tbody>
</table>

### UpdateParameterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>valueType</strong></td>
<td valign="top"><a href="#tagvaluetype">TagValueType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mappingId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

The Node ID of the parameter mapping rule.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>opcUaTag</strong></td>
<td valign="top"><a href="#opcuataginput">OpcUaTagInput</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>modbusTag</strong></td>
<td valign="top"><a href="#modbustaginput">ModbusTagInput</a></td>
<td></td>
</tr>
</tbody>
</table>

### UpdateParameterInterfaceCalculationSettingsInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>calculationSettings</strong></td>
<td valign="top">[<a href="#calculationsettinginput">CalculationSettingInput</a>!]</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateParameterInterfaceNameInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateParameterInterfaceParameterNamesAndKeyInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterNames</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### UpdateParameterMappingCodeInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter mapping code to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>message</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### UpdateParameterMappingRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter mapping rule to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateProfileMachineInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the machine to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>imageUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### UpdateRoleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### UpdateThresholdRuleInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Threshold Rule to Update

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmLevelId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmMessage</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateTimeOfUsePeriodInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the Time-of-Use period to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>energyDeviceKind</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>groupId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>periodType</strong></td>
<td valign="top"><a href="#periodtype">PeriodType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>day</strong></td>
<td valign="top"><a href="#dayofweek">DayOfWeek</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startTime</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>endTime</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateTranslationLangInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the translation lang to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### UpdateUserInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Name for display.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the user to modify.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roles</strong></td>
<td valign="top">[<a href="#string">String</a>!]!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>active</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### WriteParameterValueInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID of the parameter to write.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### WriteTagValueInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>scadaId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tagId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

## Enums

### AclMode

The Mode of ACL Permission

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>None</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Manage</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>View</strong></td>
<td></td>
</tr>
</tbody>
</table>

### CalculationFn

The function to calculate parameter values.

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Max</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Min</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Sum</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Mean</strong></td>
<td></td>
</tr>
</tbody>
</table>

### CalculationSettingFunction

The parameter of the parameter interface calculation setting function

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Add</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Subtract</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Multiply</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Divide</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ConnectStatus

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Connect</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Disconnect</strong></td>
<td></td>
</tr>
</tbody>
</table>

### DHAclScope

The Scope of ACL Permission

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>InOutbound</strong></td>
<td>

In-Outbound Settings

</td>
</tr>
<tr>
<td valign="top"><strong>Point</strong></td>
<td>

Point (from OrgAclScope GMP)

</td>
</tr>
<tr>
<td valign="top"><strong>Permission</strong></td>
<td>

Permission (from OrgAclScope Permission)

</td>
</tr>
<tr>
<td valign="top"><strong>Others</strong></td>
<td></td>
</tr>
</tbody>
</table>

### DHRetentionPolicyKind

The kind of retention policy

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>DeviceStatus</strong></td>
<td>

The inbound connector connection status

</td>
</tr>
<tr>
<td valign="top"><strong>TagValue</strong></td>
<td>

The primitive parameter values

</td>
</tr>
<tr>
<td valign="top"><strong>HistoryTagValue</strong></td>
<td>

The station primitive parameter values

</td>
</tr>
<tr>
<td valign="top"><strong>PointValue</strong></td>
<td>

The phase 2 parameter values

</td>
</tr>
</tbody>
</table>

### DayOfWeek

The Day of Week

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Sun</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Mon</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Tue</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Wed</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Thu</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Fri</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Sat</strong></td>
<td></td>
</tr>
</tbody>
</table>

### DayOrMonth

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Day</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Month</strong></td>
<td></td>
</tr>
</tbody>
</table>

### DefaultIAppKey

The default key of I.App

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>OEE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>EHS</strong></td>
<td></td>
</tr>
</tbody>
</table>

### DeviceStatusEvent

The Event Type of Device Status

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Con</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DsC</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>UeD</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Hbt</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Val</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DRec</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Predict</strong></td>
<td></td>
</tr>
</tbody>
</table>

### DiffNonNegativeCorrection

The Correction to apply on Non Negative Diff Transformation.

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>FillZero</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>SubtractZero</strong></td>
<td></td>
</tr>
</tbody>
</table>

### EhsUsageType

The Type of EHS Parameter usage

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>EnergyDemand</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>EnergyConsumption</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>EnergyDemandPrediction</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>EnergyConsumptionPrediction</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ElectricityRateClassification

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>HighVoltagePowerSupplies</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ElectricityRateContractedCapacityStatus

Electricity Rate Contracted Capacity Status

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Processing</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ElectricityRateKind

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>TOU2</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>TOU3Fixed</strong></td>
<td></td>
</tr>
</tbody>
</table>

### EnergyUsageType

Type of Energy Usage

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Consumption</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Demand</strong></td>
<td></td>
</tr>
</tbody>
</table>

### EventKind

The Kind of Event

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>HighLow</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>AlarmCode</strong></td>
<td></td>
</tr>
</tbody>
</table>

### FillOption

The Option to Fill Data Points

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>None</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Linear</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Previous</strong></td>
<td></td>
</tr>
</tbody>
</table>

### GatewayKind

The Kind of Gateway

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>WebAccess</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>EdgeLink</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IfsGateway</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>WoAgent</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Prediction</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MachineCloud</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IotEdge</strong></td>
<td></td>
</tr>
</tbody>
</table>

### GenerateEHSKpiConfigDataFunction

The data function of generating EHS KPI configuration

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Average</strong></td>
<td></td>
</tr>
</tbody>
</table>

### GenerateEHSKpiConfigDataSource

The data source of generating EHS KPI configuration

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>HistoryData</strong></td>
<td></td>
</tr>
</tbody>
</table>

### GenerateEHSKpiConfigKind

The kind of generating EHS KPI configuration

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Year</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Month</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Day</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Hour</strong></td>
<td></td>
</tr>
</tbody>
</table>

### GenericKpiAlertKind

The Alert Kind of Generic KPI

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Unlimited</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Year</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Month</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Day</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Hour</strong></td>
<td></td>
</tr>
</tbody>
</table>

### GenericKpiBind

The Bind of Generic KPI

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Group</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Machine</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Parameter</strong></td>
<td></td>
</tr>
</tbody>
</table>

### GenericKpiKind

The Kind of Generic KPI

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Unlimited</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Year</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthJan</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthFeb</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthMar</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthApr</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthMay</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthJun</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthJul</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthAug</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthSep</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthOct</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthNov</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MonthDec</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DayWeekday</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DayHoliday</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>HourSun</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>HourMon</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>HourTue</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>HourWed</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>HourThu</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>HourFri</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>HourSat</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>HourHoliday</strong></td>
<td></td>
</tr>
</tbody>
</table>

### IAppDisplay

The display option of I.App

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Show</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Hide</strong></td>
<td></td>
</tr>
</tbody>
</table>

### InboundAssociation

The inbound association of the group.

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Receiving</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Paused</strong></td>
<td></td>
</tr>
</tbody>
</table>

### KpiParameterUsageIcon

The icon of kpi parameter usage

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Workforce</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Output</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Custom</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Parameter</strong></td>
<td></td>
</tr>
</tbody>
</table>

### MachineCriticality

The Criticality of Machine

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Normal</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Major</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Critical</strong></td>
<td></td>
</tr>
</tbody>
</table>

### OperandKind

The operand kind of formula

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Constant</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Formula</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Variable</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Sum</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>SumOfDiff</strong></td>
<td></td>
</tr>
</tbody>
</table>

### Operator

The operator of formula

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Add</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Subtract</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Multiply</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Divide</strong></td>
<td></td>
</tr>
</tbody>
</table>

### OrderDirection

Possible directions in which to order a list of items when provided an `orderBy` argument.

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>ASC</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DESC</strong></td>
<td></td>
</tr>
</tbody>
</table>

### OrgAclScope

The Scope of ACL Permission

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>GMP</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Profile</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MachineStatus</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MappingRule</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>License</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Cmdc</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>User</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Client</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Role</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Permission</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Others</strong></td>
<td></td>
</tr>
</tbody>
</table>

### OutboundTransport

The transport of Outbound

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Mqtt</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Http</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ParameterKind

The kind of the parameter.

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Constant</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Prediction</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Transformation</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Calculation</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Kpi</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>OpcUa</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Modbus</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ParameterOrderField

Properties by which parameter connections can be ordered.

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>CREATED_AT</strong></td>
<td></td>
</tr>
</tbody>
</table>

### PeriodType

The default period type

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>On</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Partial</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Off</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Custom1</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Custom2</strong></td>
<td></td>
</tr>
</tbody>
</table>

### PointSchedule

The Schedule to perform Point evaluation.

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Minute1</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Minute5</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Minute15</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Minute30</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Minute60</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Daily</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ProductionKpiKind

The Kind of Production KPI

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Group</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Machine</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Parameter</strong></td>
<td></td>
</tr>
</tbody>
</table>

### REAclScope

The Scope of ACL Permission

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>HighLowEvent</strong></td>
<td>

High-Low Event

</td>
</tr>
<tr>
<td valign="top"><strong>Permission</strong></td>
<td>

Permission (from OrgAclScope Permission)

</td>
</tr>
<tr>
<td valign="top"><strong>Others</strong></td>
<td></td>
</tr>
</tbody>
</table>

### SamplingMethod

The sampling method of parameter values

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Last</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Min</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Max</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Mean</strong></td>
<td></td>
</tr>
</tbody>
</table>

### TagValueType

The Value Type of Alarm Code Event Log

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Number</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>String</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Discrete</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ThresholdType

The Type of Threshold Rule

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>HH</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>H</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>L</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>LL</strong></td>
<td></td>
</tr>
</tbody>
</table>

### TimeInterval

The time interval between parameter values

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>QuarterHour</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>HalfHour</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Hour</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>TwoHours</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>FourHours</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>EightHours</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>HalfDay</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Day</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Week</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Month</strong></td>
<td></td>
</tr>
</tbody>
</table>

### TimeShift

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Current</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>OneWeek</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>OneMonth</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>OneYear</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>TwoYears</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ThreeYears</strong></td>
<td></td>
</tr>
</tbody>
</table>

### TimeType

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>RealTime</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Hour</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Day</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Week</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Month</strong></td>
<td></td>
</tr>
</tbody>
</table>

### TransformationPointFn

The Function to perform by Transformation Point.

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>Min</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Max</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Diff</strong></td>
<td></td>
</tr>
</tbody>
</table>

### URModbusDataType

The data type of the UR modbus server.

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>ToolOutputVoltage</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ControllerVersionHighNumber</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ControllerVersionLowNumber</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IsPowerOnRobot</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IsSecurityStopped</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IsEmergencyStopped</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IsTeachButtonPressed</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IsPowerButtonPressed</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IsSafetySignalSuchThatWeShouldStop</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>BaseJointCurrent</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ShoulderJointCurrent</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ElbowJointCurrent</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Wrist1JointCurrent</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Wrist2JointCurrent</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Wrist3JointCurrent</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>BaseJointTemperature</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ShoulderJointTemperature</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ElbowJointTemperature</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Wrist1JointTemperature</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Wrist2JointTemperature</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>Wrist3JointTemperature</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>RobotCurrent</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IOCurrent</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ToolState</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ToolTemperature</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ToolCurrent</strong></td>
<td></td>
</tr>
</tbody>
</table>

## Scalars

### Boolean

The `Boolean` scalar type represents `true` or `false`.

### Currency

A field whose value is a Currency: <https://en.wikipedia.org/wiki/ISO_4217>.

### Date

A date string, such as 2007-12-03, compliant with the `full-date` format outlined in section 5.6 of the RFC 3339 profile of the ISO 8601 standard for representation of dates and times using the Gregorian calendar.

### DateTime

The javascript `Date` as string. Type represents date and time as the ISO Date string.

### EmailAddress

A field whose value conforms to the standard internet email address format as specified in RFC822: <https://www.w3.org/Protocols/rfc822/>.

### Float

The `Float` scalar type represents signed double-precision fractional values as specified by [IEEE 754](https://en.wikipedia.org/wiki/IEEE_floating_point).

### ID

The `ID` scalar type represents a unique identifier, often used to refetch an object or as key for a cache. The ID type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"4"`) or integer (such as `4`) input value will be accepted as an ID.

### Int

The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1.

### JSON

The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf).

### JSONObject

The `JSONObject` scalar type represents JSON objects as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf).

### NonNegativeInt

Integers that will have a value of 0 or more.

### ObjectID

A field whose value conforms with the standard mongodb object ID as described here: <https://docs.mongodb.com/manual/reference/method/ObjectId/#ObjectId>. Example: 5e5677d71bdc2ae76344968c

### PositiveFloat

Floats that will have a value greater than 0.

### PositiveInt

Integers that will have a value greater than 0.

### String

The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text.

### URL

A field whose value conforms to the standard URL format as specified in RFC3986: <https://www.ietf.org/rfc/rfc3986.txt>.

### UUID

A field whose value is a generic Universally Unique Identifier: <https://en.wikipedia.org/wiki/Universally_unique_identifier>.

### Void

Represents NULL values

## Interfaces

### Action

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rule</strong></td>
<td valign="top"><a href="#rule">Rule</a>!</td>
<td>

Rule that this Action Belongs to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Whether this Action is Active

</td>
</tr>
</tbody>
</table>

### CommonValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>str</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>savedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
</tbody>
</table>

### DocumentNode

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
</tbody>
</table>

### DownSampledCommonValue

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>time</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>num</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>str</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### Event

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Name of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Description of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>machineId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Machine of the Parameter of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parameterId</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

The Node ID of the Parameter of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#eventkind">EventKind</a>!</td>
<td>

The Kind of the Event

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The event created at.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

The event updated at.

</td>
</tr>
</tbody>
</table>

### Node

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### Rule

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

The Node ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>_id</strong></td>
<td valign="top"><a href="#objectid">ObjectID</a>!</td>
<td>

The MongoDB ID

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>event</strong></td>
<td valign="top"><a href="#event">Event</a>!</td>
<td>

Event that this Rule Belongs to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>alarmLevel</strong></td>
<td valign="top"><a href="#alarmlevel">AlarmLevel</a>!</td>
<td>

AlarmLevel that this Rule Related to

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>actions</strong></td>
<td valign="top">[<a href="#action">Action</a>!]!</td>
<td>

The Actions of the Rule

</td>
</tr>
</tbody>
</table>
