# SSO Development Guide

## 版本歷史

| **日期**       | **版本** | **作者**      | **說明** |
| -------------- | -------- | ------------- | -------- |
| **2022-07-01** | 0.1.0    | Shouche Cheng | 初版     |

## 功能說明

### 1. 身份驗證

&emsp;&emsp;當 server 收到請求時，會根據請求內的資訊，去判斷是否要接受並處理該請求，因此，向 server 發起請求時，也需一併將用來辨別身份的資訊放入請求之中，關於如何將相關的資訊放入請求之中，步驟說明如下：

1. 透過 mutation `signIn` 進行登入，若能成功登入，則在回應的 header 中，會含有「set-cookie」，若是在**地端**的環境，只會有「IFPToken」，若是在**雲端**的環境，還會有「EIToken」

   - 若是在**地端**，預設的帳號為「ifs@advantech.com」，密碼為「password」
   - 若是在**雲端**，則使用和 Management Portal 相同的帳密進行登入

   ```graphql
   mutation {
     signIn(input: { username: "ifs@advantech.com", password: "password" }) {
       ifpToken
     }
   }
   ```

   ```json
   {
     "data": {
       "signIn": {
         "ifpToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJpZnAudXNy
         Iiwic3ViIjoiNjI5ZDc5NDJmNGJkOGEwMDA5YTVlODM1IiwiYXVkIjoidXNlciIsInVzZX
         JuYW1lIjoiaWZzQGFkdmFudGVjaC5jb20iLCJpYXQiOjE2NTY1ODAyNDMsImV4cCI6MTY1
         NjYwOTA0M30.9vH6gNhX1cwI0WgyoSZscXopNTYYhdcHGYHELQyn4D0"
       }
     }
   }
   ```

   ```http
   Set-Cookie: IFPToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJpZnAudX
   NyIiwic3ViIjoiNjI5ZDc5NDJmNGJkOGEwMDA5YTVlODM1IiwiYXVkIjoidXNlciIsInVzZXJuYW
   1lIjoiaWZzQGFkdmFudGVjaC5jb20iLCJpYXQiOjE2NTY1ODAyNDMsImV4cCI6MTY1NjYwOTA0M3
   0.9vH6gNhX1cwI0WgyoSZscXopNTYYhdcHGYHELQyn4D0; path=/; expires=Thu, 30 Jun 2
   022 17:10:43 GMT; httponly
   ```

1. 在後續的請求之中，需要將前一步驟所產生的 cookie 也一併帶入，以供 server 進行身份驗證

   ```http
   Cookie: IFPToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJpZnAudXNyIi
   wic3ViIjoiNjI5ZDc5NDJmNGJkOGEwMDA5YTVlODM1IiwiYXVkIjoidXNlciIsInVzZXJuYW1lIj
   oiaWZzQGFkdmFudGVjaC5jb20iLCJpYXQiOjE2NTY1ODAyNDMsImV4cCI6MTY1NjYwOTA0M30.9v
   H6gNhX1cwI0WgyoSZscXopNTYYhdcHGYHELQyn4D0
   ```

### 2. 取得登入網址

```graphql
query {
  iAppInfos(names: "RTM") {
    metadata(keys: "sigInUrl") {
      key
      string
    }
  }
}
```

```json
{
  "data": {
    "iAppInfos": [
      {
        "metadata": [
          {
            "key": "sigInUrl",
            "string": "http://${hostname}:10000/#/signIn?redirectTo=${REDIRECT_TO}"
          }
        ]
      }
    ]
  }
}
```

### 2. SSO 身分認證

1. 確認是否已經登入。需要認證身分的 API 當收到請求後，可以直接複製全部 Cookies Header 或是只複製 IFPToken、EIToken 和 EIName 三個 Cookies，然後攜帶它們打向 Desk API，成功代表身分認證成功

   ```graphql
   query me {
     me {
       user {
         id
         name
         username
       }
     }
   }
   ```

2. 當第一次進到網頁時，需要呼叫一次需要認證身分的 API，成功代表已登入，失敗的話需要轉址到同樣 HOSTNAME(地端的話)的 Desk 登入頁面，並附加登入成功後需要到達的網址，例如:
   `http://127.0.0.1:10000/#/signIn?redirectTo=http://127.0.0.1:25320`

3. 登入成功後，會轉址到該網址，然後在該網址再呼叫一次需要認證身分的 API。
4. 請記住如果 UI 直接呼叫 Desk API 可能會撞到 Same Origin Policy 的限制，所以建議的做法是在後端做個到 Desk API 的 Proxy。
