# React i18n 教學

## 內容

[版本](#版本歷史)\
[安裝與設定 react-i18next](#安裝與設定-react-i18next)\
[安裝與設定 VS Code Extension i18n Ally](#安裝與設定-VS-Code-Extension-i18n-Ally)\
[如果有用 ifp-ui Component Library](#_如果有用-ifp-ui-Component-Library)\
[挖洞教學](#_挖洞教學)\
[取得現在語言](#_取得現在語言)\
[iFrame postMessage](#_iFrame-postMessage)

## 版本歷史

| **日期** | **版本** | **作者** | **說明** |
| - | - | - | - |
|**2021-10-18** |v-0.1.0 |Shouche.Cheng|初版 |
|**2021-10-19** |v-0.2.0 |Shouche.Cheng|<p>修改  iFrame postMessage  程式碼 </p><p>- 修改與  iFractory Desk  溝通的格式</p> |
|||||

## 安裝與設定 react i18next

1. 安裝 react-i18next

```bash
npm install --save react-i18next i18next i18next-fetch-backend i18next-browser-languagedetector 
```

2. 新增翻譯檔案於  /public/locales/  底下，內容為空物件，<font color=red>其中  **ifp-predict-portal**  是  Namespace 名稱，需要改成專案名稱</font>

![React-i18n-Technical-Document_1.003.png](photos\React-i18n-Technical-Document_1.003.png)

```bash
//  內容 
{}
```

3. 新增  src/i18n.ts，<font color=red>記得要改  ns: ['ifp-predict-portal'],，將其改成專案名稱</font>

```js
import i18n from 'i18next';
import LanguageDetector, {
    DetectorOptions,
} from 'i18next-browser-languagedetector';
import Backend, { FetchOptions } from 'i18next-fetch-backend'; 
import { initReactI18next } from 'react-i18next';

i18n
  // load translation using fetch -> see /public/locales
  // learn more: <https://github.com/dotcore64/i18next-fetch-backend>
  .use(Backend)
  // detect user language
  // learn more: <https://github.com/i18next/i18next-browser-languageDetector>
  .use(LanguageDetector)
  // pass the i18n instance to the react-i18next components.
  // Alternative use the I18nextProvider: <https://react.i18next.com/latest/i18nextprovider>    
  .use(initReactI18next)
  // init i18next
  // for all options read: <https://www.i18next.com/overview/configuration-options>
  .init({
    // debug: true,
    backend: {
      loadPath: '/locales/{{lng}}/{{ns}}.json',
    // 如果有安裝 ifp-ui-\* Component Library
    // 他的翻譯檔會放在 /locales-libs/{{lng}}/{{ns}}.json底下
    // 所以要覆寫 i18next-fetch-backend 設定
    // 當Namespace是ifp-ui- 開頭時，改從/locales-libs/{{lng}}/{{ns}}.json讀取翻譯檔 
      fetch: function (url: string, requestOptions) {
        if (/^\/locales\/[A-Za-z-]+\/ifp-ui-\S+/.test(url)) {
          url = url.replace('locales', 'locales-libs');
        }
        return fetch(url, requestOptions);
      },
    } as FetchOptions,

    detection: {
    // 順序1 Query String
    // 順序2 Cookie
    // 順序3 瀏覽器語言
      order: ['querystring', 'cookie', 'navigator'], lookupQuerystring: 'IFPLang', lookupCookie: 'IFPLang',
    // 關閉Cache
      caches: [],
    } as DetectorOptions,      
    // 語言白名單
    supportedLngs: ['en-US', 'zh-TW', 'zh-CN'],
    // 預設語言為en-US
    fallbackLng: 'en-US',
    // 讓他只讀取en-US，不會去讀取en
    load: 'currentOnly',
    // 如果翻譯結果是空字串的話，會Fallback回預設語言en-US的翻譯
    returnEmptyString: false,
    // 預先載入的  Namespace，需要改成專案名稱
    ns: ['ifp-predict-portal'],
    // 分隔符從預設的 ":" 改成  "."，為了與VS Code Extension i18n Ally有更好的體驗      
    nsSeparator: '.',
    interpolation: {
    // not needed for react as it escapes by default        
      escapeValue: false,
    },
    react: {
    // useSuspense: false,
    },
  });
export default i18n;
```

4. 在  src/index.tsx  匯入  i18n.ts

```bash
import './i18n';
```

5. 在  src/index.ts  新增 Suspense

```js
ReactDOM.render(
  <React.StrictMode>
    <Suspense fallback={<Fragment />}>        
      <App />
    </Suspense>
  </React.StrictMode>,
  document.getElementById('root'),
);
```

## 安裝與設定 VS Code Extension i18n Ally

1. 經由 VS Code Extensions 安裝  i18n Ally

![React-i18n-Technical-Document\_1.009.jpeg](photos\React-i18n-Technical-Document\_1.009.jpeg)

1. 新增 .vscode/settings.json，<font color=red> 記得要改
<strong style= "background:Yellow" >"i18n-ally.extract.keyPrefix": "ifp-predict-portal.",</strong>，改成專案名稱再加個 "."</font>

```js
{
  "i18n-ally.annotationInPlace": false,
  "i18n-ally.displayLanguage": "en-US",
  "i18n-ally.enabledFrameworks": ["react"],
  // 這裡改成跟Namespace一樣，照之前的做法的話會是專案名稱，後面在加的"." 
  "i18n-ally.extract.keyPrefix": "ifp-predict-portal.",
  "i18n-ally.keepFulfilled": true,
  "i18n-ally.keystyle": "flat",
  "i18n-ally.localesPaths": ["public/locales"],
  "i18n-ally.namespace": true,
  "i18n-ally.pathMatcher": "{locale}/{namespace}.{ext}",    
  "i18n-ally.sortKeys": true,
  "i18n-ally.sourceLanguage": "en-US",
  "i18n-ally.usage.derivedKeyRules": ["{key}\_other"]
}
```

## 如果有用 ifp-ui Component Library

因為 ifp-ui 裡有翻譯檔，所以需要將這些翻譯檔複製出來

1. 安裝  copy

```bash
npm install -D copy @types/copy
```

2. 新增  scripts/installLocales.js

```js
const { resolve } = require('path'); ![](React-i18n-Technical-Document\_1.010.png)const { promisify } = require('util'); const copy = require('copy');

const copyAsync = promisify(copy);

//  複製的語言
const LANG\_CODES = ['en-US', 'zh-TW', 'zh-CN']; //  有哪些  ifp-ui Libraries
const LIB\_NAMES = ['ifp-ui-core', 'ifp-ui-enabler'];
//  翻譯檔的目標路徑，記得要跟專案的翻譯檔要放在不同位置，避免  VS Code i18n Ally  讀取到
const LOCALES\_PATH = resolve(\_\_dirname, '../public/locales-libs/');

// ifp-ui Library 翻譯檔資料夾路徑
function getLocalesPath(libName) {
  const localesPath =
  require( @advifactory/${libName}/scripts/localesPath.js ).default;

  return localesPath(); 
}

async function main() {
  Promise.all([
    LIB_NAMES.map(libName => {        
      // 取得翻譯檔資料夾路徑
      const libLocalesPath = getLocalesPath(libName);        
      return Promise.all(
        // 針對各個語言
        LANG_CODES.map(async langCode => {
          console.log( Install Locale ${langCode} - ${libName} );
          // 複製翻譯檔
          return copyAsync(
            `${libLocalesPath}/${langCode}/\*.json` ,              
            `${LOCALES\_PATH}/${langCode}/` ,
          );       
        }),        
      );
    }),
  ]);
}

main();   
```

3. 新增  npm hook postinstall

```bash
"postinstall": "node ./scripts/installLocales.js" 
```

## 挖洞教學

1. 在Component開頭取得 react-i18next 函式

```bash
const { t } = useTranslation();
```

2. 如果不是預設的  Namespace  的話，要記得提供  Namespace  名稱

```bash
const { t } = useTranslation(['other-namespace']); 
```

3. 接下來選中要翻譯的文字

![React-i18n-Technical-Document\_2.004.png](photos\React-i18n-Technical-Document\_2.004.png)

4. 按下快速鍵  Ctrl + .  ，或是左邊的燈泡
5. 如果沒有翻譯過的話，直接選擇  **Extract text into i18n messages**

![React-i18n-Technical-Document\_2.005.png](photos\React-i18n-Technical-Document\_2.005.png)

6. 之後上方會出現提示框，會預先帶入預設的  key，可以去修改它，不過要記得前面的  Namespace ifp-predict-portal. (通常會是專案名稱)  一定要保留

![React-i18n-Technical-Document\_2.006.png](photos\React-i18n-Technical-Document\_2.006.png)

![React-i18n-Technical-Document\_2.007.png](photos\React-i18n-Technical-Document\_2.007.png)

7. 下一步選擇使用的方式

![React-i18n-Technical-Document\_2.008.png](photos\React-i18n-Technical-Document\_2.008.png)

8. 如果你有多個  Namespace  接下來還會要你選擇哪一個翻譯檔案

![React-i18n-Technical-Document\_2.009.png](photos\React-i18n-Technical-Document\_2.009.png)

9. 如果之前有翻譯過，那就選擇你想使用的方式

![React-i18n-Technical-Document\_2.010.png](photos\React-i18n-Technical-Document\_2.010.png)

## 取得現在語言

現在語言放在 Cookie  裡，名稱為 IFPLang。 如果有按照之前的方式新增 src/i18n.ts，裡面就已經處理初始化的部分

```js
detection: { 
  // 順序 1 Query String        
  // 順序 2 Cookie
  // 順序 3 瀏覽器語言
  order: ['querystring', 'cookie', 'navigator'], lookupQuerystring: 'IFPLang', lookupCookie: 'IFPLang',
  // 關閉  Cache
  caches: [],
} as DetectorOptions,
```

如果想要在 iFactory/Desk 改變語言時，實時修改本身的語言有以下一種方法

## iFrame postMessage

新增 React Hook，並在 App Component 使用它

```js
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

/**
*註冊 message 事件，如果有changeLanguage動作，修改現在語言
*/
export function useReceiveLanguageFromMessage() {
  const { i18n } = useTranslation();
  useEffect(() => {
    const onMessage = (event: MessageEvent) => {        
    //  將 message 字串轉成 action 物件
      const action = deserializeIAction(event.data);
      if (isIChangeLanguageAction(action)) {
        if (action.data.language) {
          i18n.changeLanguage(action.data.language);          
        }
      } else {
      // 不認識的事件，顯示警告訊息
        console.error( Unknown event: ${event.data} );
      }
    };
    // 註冊 message 事件 
    window.addEventListener('message', onMessage);
    return () => {
    //  離開時，註銷 message 事件
      window.removeEventListener('message', onMessage);      
    };
  }, [i18n]);
}

interface IAction {    
  name: string;
  data?: unknown; 
}

/**
* 判斷是否為 IAction
* @param source
* @returns
*/
function isIAction(source: any): source is IAction {
  return (
    source && 'name' in source && source.name && typeof source.name === 'string'    
  );
}

interface IChangeLanguageAction extends IAction {    
  name: 'changeLanguage';
  data: {
    language: string;
  };
}

/**
* 判斷是否為 IChangeLanguageAction
* @param source
* @returns
*/
function isIChangeLanguageAction(source: any): source is IChangeLanguageAction {
  return (
    isIAction(source) &&
    source.name === 'changeLanguage' &&
    source.data &&
    'language' in (source.data as any) &&
    (source.data as any).language &&
    typeof (source.data as any).language === 'string'    );
}

/**
* 將 JSON 字串轉回 Action 物件
* 格式 JSON.stringify(IAction)
* @param source
* @returns
*/
function deserializeIAction(source: unknown): IAction | null {    
  // 只允許字串
  if (typeof source !== 'string') {      
    return null;
  }
  // JSON 字串轉回 Javascript 變數
  const action = (() => {
    try {
      return JSON.parse(source);
    } catch (error) {
      console.error( Parse ${source} action failed: ${error.message} );      
    }
  })();
  
  return isIAction(action) ? action : null; 
}

function App() {
  //  從 iFactory/Desk 實時接收並改變現在語言  
  useReceiveLanguageFromMessage();

  return (      
    <div>        
     ...
    </div>    
  );
}
```
