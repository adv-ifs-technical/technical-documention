#

## 版本歷史

| **日期** | **版本** | **作者** | **說明** |
| - | - | - | - |
|**2022-06-08**|0.1.0|Shouche Cheng| 初版 |

## 功能說明

## Tenant Quota API (暫)

## Interface

```typescript
interface TenantQuota {
  // 同一種類型的料號組
  pns: string[];
  // 可用配額
  quota: number;
  // 已使用數量
  count: number;
}
```

## Stories (Web)

### Get current tenant license quotas

```graphql
query {
  me {
    tenant {
      id
      name
      tenantQuotas(pnsList: [["XXXX"], ["YYYY"]]) {
        pns
        quota
        count
      }
    }
  }
}
```

## Stories (Internal Services)

### Call ifp-api-hub authorization

- HTTP Header: `X-Ifp-Service-Name` (Required)
  - from iApp Service Register
- HTTP Header: `X-Ifp-App-Secret` (Required)
  - from iApp Service Register

### Get all tenant license quotas

```graphql
query {
  allTenant {
    id
    name
    tenantQuotas(pnsList: [["XXXX"], ["YYYY"]]) {
      pns
      quota
      count
    }
  }
}
```

### Get tenant license quotas by tenant Ids

```graphql
query {
  tenantsByIds(ids: ["XXX", "YYY"]) {
    id
    name
    tenantQuotas(pnsList: [["XXXX"], ["YYYY"]]) {
      pns
      quota
      count
    }
  }
}
```

### Set current tenant license used count

```graphql
mutation {
  setTenantQuotaCount(input: { tenantId: "XXXX", pns: ["XXXX"], count: 80 }) {
    tenantQuota {
      pns
      quota
      count
    }
  }
}
```
