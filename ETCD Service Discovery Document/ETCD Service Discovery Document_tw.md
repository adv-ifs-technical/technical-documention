#

## 版本歷史

| **日期** | **版本** | **作者** | **說明** |
| - | - | - | - |
|**2022-03-01** |1.0.0 |Cillin Kao|初版 |
|**2022-03-29** |1.1.0 |Cillin Kao|增加 License ETCD  解說 |

## 功能說明

使用 etcd3 建構 iApp 服務的註冊與探知，並可以給 i.App 放置應用程式的資訊，讓 Desk/Enable 能清楚的知道 App 的資訊，包含連線資訊、App 圖示、App 解說、License 用量 ….. 等資訊，方便快速完成首頁的圖示串接。

### 1. etcd3 是什麼？

etcd 是一種 Key-Value Store 資料庫，有一整個扁平的 Key space。 我們使用 v3 版的 API (gRPC based) 故稱為 etcd3

- 1.1 [Libraries and tools](https://etcd.io/docs/v3.5/integrations/)
  - [ETCD Manager](https://etcdmanager.io/)  
  - [etcd3](https://github.com/microsoft/etcd3)
  - [etcd/client/v3](https://github.com/etcd-io/etcd/tree/main/client/v3)

- 1.2 [Lease](https://etcd.io/docs/v3.5/learning/api/#lease-api)

可用來偵測 metadata 註冊者的 iApp/Service 是否還維持連線。

取得 Lease 時要指定 TTL ，並在每個週期 TTL 到期前 keepAlive ，持續發送 keepAlive 的行為會由 client library 預設自動進行。

如果 iApp/Service 停止運作或與 etcd 中斷連線，又或是停止持續發送 keepAlive ， TTL 到期後 Lease 就會被撤銷， Lease 也可以由 client 主動 revoke。

Lease 可在 Put Key 時指定，以物件封裝的 library 可以看成使用 Lease instance 來 put key ，這樣該 Key 就跟這個 Lease 建立關聯。

一個 Lease 可以關聯多個 Key ，但一個 Key 只能被一個 Lease 關聯， Put Key 會複寫該 Key 原本的 Lease 關聯。

Lease 撤銷時，所有相關聯的 Key 也會同時被刪除。

- 1.3 [Election](https://microsoft.github.io/etcd3/classes/election.html)

由 client library 封裝的功能，底層也有使用到Lease ，所以建議Election 的 ttl 選項與寫入 metadata 的 Lease 一致。

Election 機制有分兩種角色

     1. 參選者    
      a. 當選者 leader  
      b. 其他非當選者
     2. 觀察者  
      a. 取得當下情況  
      b. 監看情況變化

### 2. Key space 命名規則 (iApp)

- 2.1 填寫範例 (不含 [])  
  - [iApp name] : RTM  
  - [version] : 1.2.3  
  - [sid] : 在 [iApp name] 和 [version] 下唯一, 範例如下:  
    - 2021-06-30T02:48:07.249Z  
    - 1625021287249  
    - 60dbdbb7e49c20984a680913  
    - e494ba80-d94d-11eb-b8bc-0242ac130003  
- 2.2 Key directory 由 iApps 填入 (iApp info)
在 iApp 主服務退出前刪除，優先使用 Lease

  - iapps/  
    - [iApp name]/[version]/[sid]/  
      - portal : iframe 來源 或 href 鏈接的 URL  
      - licenseType : 字串
      - licenseUsageCount : 數字
      - licenseUsageLimit : 數字  
      - other...
- 2.3 Election 機制依照服務去運行
  - prefix : iapps/election
  - name : [iApp name]  
  - value : [version]/[sid]  
  - resign before iApp main service quit
  - 觸發 API Hub重新加載 iApp 資訊  

### 3. Key space 命名規則 (Services)

- 3.1 填寫範例 (不含 [])
  - [service name] : ifp-data-hub  
  - [version] : 1.2.3  
  - [sid] : 在 [service name] 和[version] 下唯一, 範例如下:  
    - 2021-06-30T02:48:07.249Z  
    - 1625021287249  
    - 60dbdbb7e49c20984a680913  
    - e494ba80-d94d-11eb-b8bc-0242ac130003
- 3.2 Key directory 由 services 填入 (service info)

  在 service 主服務退出前刪除，優先使用 Lease

  - services/
    - [service name]/[version]/[sid]/  
      - federation : Apollo Federation API URL位置
      - dependentServices : 相依的服務名稱，如: ["ifp-organizer", "ifp - data-hub"]  
      - other...
- 3.3 Election 機制依照服務去運行
  - prefix : services/election
  - name : [service name]
  - value : [version]/[sid]  
  - resign before service quit
  - 觸發 API Hub 重新加載 service 資訊

### 4 Register Flow

1. API Server 準備就緒並監聽特定 port  
1. 獲得 Lease 並放入 metadatas  
1. Election 活動  

### 5 Service Secrets

- 5.1 如何獲得
  1. Metadata 內容中的服務註冊資訊
  1. Election 活動當選後， API Hub將通過以下流程分配服務需要的密鑰
  - 5.1.1 Key directory fill by API Hub with Lease TTL
    - service-secrets/
    - [assignee service name]/[version]/[sid]/  
    - [assignee service name] : secret string
    - [dependent service name] : secret string
    - other [dependent service name] ...
  - 5.1.2 關於 service secrets 的重要說明
    - API Hub 將這些密鑰與 Lease TTL 約 30 秒，它不會被持久化在 etcd  
    - 服務應該像 API Hub 本身一樣在內存中保持服務機密，而不是存儲到文件、數據庫或 其他持久存儲中
    - 每個服務機密的生命週期都在 API Hub 和服務流程實例之間的關係內，即如果 API Hub 或服務重新啟動一個新實例， 將分配一個新的秘密，而之前的秘密已過時
    - 這可能是選定的服務實例和分配給指定密鑰的秘密之間的時間延遲，也可能為現有的 服務實例分配一個新的秘密（例如 API Hub 重新啟動）
    - 因此，Service 應該在所需的密鑰上設置一個 etcd Watcher，然後獲取密鑰一次作為 初始值，並在分配任何（新）秘密時保持更新
- 5.2 如何使用
  - 5.2.1 Use case 1: 調用 API Hub 的 API入口 (the merged Graph) 在請求中提供這些標頭 (不包含 < 和 >)

    - X-Ifp-Service-Name: < 這個服務的名稱 >  
    - X-Ifp-App-Secret: < 這個服務使用的密鑰 >  

  API Hub 將使用這些標頭對 isIfpApp 進行身份驗證，從而獲得訪問某些允許 isIfpApp 之 API權限

  - 5.2.2 Use case 2: authenticate API Hub sub-graph request if providing a federation GraphQL API

  API Hub 將提供與上述相同的 headers 來請求 sub-graph 服務，服務可以使用這些 headers 來驗證這個請求來自 API Hub，並且可以信任它提供的一些其他 headers

  - X-Is-Ifp-App:  true or false 如果 isIfpApp 從 API Hub 入口  
  - X-Ifp-User: 如果從 API Hub 入口訪問具有經過驗證的 IFPToken 的用戶，此標頭值 將是 JSON 和 URIComponent 編碼的用戶數據，請對其進行解碼，然後將 JSON 解析 為用戶身份驗證信息

### 6. Metadatas for Desk frontend (ifp-console)

- 6.1 RTM I.App
  - enablerLink  
    - 與 iAppLink 相同  
  - cmdcLink  
    - 與 iAppLink 相同  
  - userLink  
    - 與 iAppLink 相同  
  - enablerFeature
    - 與 iAppFeature 相同  
- 6.2 Other I.App
  - iAppLink : JSON Object  
    - url  
      - required  
    - iconUrl  
      - required  
      - size： 72 \* 72
    - iconUrlMap  
      - optional  
      - size： 72 \* 72  
      - fields  
        - disabled  
          - optional  
        - active  
          - optional  
        - hover  
          - optional  
      - titleText
        - required  
      - descriptionText
        - optional  
      - buttonText  
        - optional  
        - linkType=internal: "Configuration"
        - linkType=external: "Open"
        - linkType=iframe: "Configuration"
      - linkType  
        - internal: IFP 內部使用  
        - external: 開新分頁  
        - iframe:  使用  iframe 內嵌至 iFactory/Desk  
  - iAppFeature : JSON Object  
    - iconUrl  
      - required  
      - size： 24 \* 24  
    - titleText
      - required  
- 6.3 iApp Link  
![ETCD-v1.1.0-2.005.png](photos/ETCD-v1.1.0-2.005.png)

- 6.4 iApp License Info  
![ETCD-v1.1.0-2.006.png](photos/ETCD-v1.1.0-2.006.png)

### 7. Metadatas for L3 Pivot

- 7.1 L3EHS I.App
  - l3EHSLink : JSON Object
  - deviceType
    - optional
    - EHS(FEMS) use
  - parameterUsage
    - optional
    - EHS(FEMS) use
  - limitSettings
    - optional
    - EHS(FEMS) use
  - timeOfUsePeriod
    - optional
    - EHS(FEMS) use
  - energySplit
    - optional
    - EHS(FEMS) use
  - virtualMeter
    - optional
    - EHS(FEMS) use
  - taskManagement
    - optional
    - Maintenance use
  - metric
    - optional
    - RTM(KPI) use
  - kpiSettings
    - optional
    - EHS(FEMS) use
  - energyUsageInterface
    - optional
    - EHS(FEMS) use
  - energyUsageInterfaceBindingGroup
    - optional
    - EHS(FEMS) use
  - getMoreMetricDashboard
    - optional
    - RTM(KPI) use
  - predictions
    - optional
    - Predict use
  - modelTraining
    - optional
    - Predict use
  - spcChart
    - optional
    - Predict use
  - getMoreAiModel
    - optional
    - Predict use
  - assetSettings
    - optional
    - ???

- 7.1 L3OEEMaintenance I.App
  - l3OEEMLink : JSON Object
    - masterData
      - capacityPlanning
        - optional
    - kpi
      - parameterUsage
        - optional
    - metric
      - optional
    - settings
      - optional
    - getMoreMetricDashboard
      - optional
    - apqp
      - dailyTask
        - optional
    - projectPortfolio
      - optional
    - project
      - optional
    - getMoreTemplate
      - optional
    - predict
      - predictions
        - optional
      - modelTraining
        - optional
      - spcChart
        - optional
      - getMoreAiModel
        - optional
    - dataSettings
      - assetSettings
        - optional

### 8. Metadatas for License Quota (ifp-console)

- 8.1 Single Quota (Default)
  - licenseType
    - 字串格式
    - 顯示在畫面上的文字
  - LicenseUsageCount
    - 數字格式
    - 已使用的數量
  - LicenseUsageLimit
    - 數字格式
    - 可使用的 License 額度
- 8.2 Multiple Quotas (尚未實作)
  - LicenseTypeKeys
    - 英文字串陣列
  - license/&lt;licenseTypeKey&gt;/name
    - 顯示在畫面上的文字
  - license/&lt;licenseTypeKey&gt;/count
    - 已使用的數量
  - license/&lt;licenseTypeKey&gt;/limit
    - 可使用的 License 額度
  
### 9. PN inquiry

若 service 欲取得某個 PN的 license 時，須遵循以下流程：

1. 查看 Etcd 上，key「license/pn/&lt;part numbery&gt;」是否已經存在，若存在，value即為 LicenseQty

2. 若不存在，service 需寫入 key「license/pn/&lt;part numbery&gt;」，value 填入空字串

3. 當 fetcher 觀察到有人填入了新的 PN，且 value 為空字串，便會 call License API，嘗試取得該 PN的 LicenseQty

4. 若順利取得，則會將 value 覆寫為 LicenseQty，反之則直接刪除 key

fetcher 每 15 分鐘會再 call License API，確認已存在 Etcd 裡的 pn 的 license 狀態是否有 變化，並視情況更新或刪除。
