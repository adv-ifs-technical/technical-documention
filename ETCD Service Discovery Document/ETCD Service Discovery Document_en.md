#

## Version History

| **Date** | **Version** | **Author** | **Description** |
| - | - | - | - |
|**2022-03-01** |1.0.0 |Cillin Kao|First released |
|**2022-03-29** |1.1.0 |Cillin Kao|Add License ETCD Explanation |

## Function Description

Use etcd3 to construct the registration and detection of iApp services, and can place application information for i.App, so that Desk/Enable can clearly know the information of the App, including connection information, App icon, App description, License usage.... . and other information, it is convenient to quickly complete the icon serialization of the home page.

### 1. What is etcd3?

etcd is a Key-Value Store database with an entire flat Key space. We use the v3 version of the API (gRPC based) so it's called etcd3

- 1.1 [Libraries and tools](https://etcd.io/docs/v3.5/integrations/)
  - [ETCD Manager](https://etcdmanager.io/)  
  - [etcd3](https://github.com/microsoft/etcd3)
  - [etcd/client/v3](https://github.com/etcd-io/etcd/tree/main/client/v3)

- 1.2 [Lease](https://etcd.io/docs/v3.5/learning/api/#lease-api)

Can be used to detect if the iApp/Service of the metadata registrant is still connected.

When obtaining Lease, specify TTL, and keepAlive before each cycle TTL expires. The behavior of continuously sending keepAlive will be automatically performed by the client library preset.

If the iApp/Service stops working or disconnects from etcd, or stops sending keepAlive continuously, the Lease will be revoked after the TTL expires, and the Lease can also be actively revoke by the client.

Lease can be specified when Put Key. A library encapsulated by an object can be regarded as using Lease instance to put key, so that the Key is associated with this Lease.

A Lease can be associated with multiple Keys, but a Key can only be associated with one Lease. Put Key will overwrite the original Lease association of the Key.

When the lease is revoked, all associated keys will also be deleted at the same time.

- 1.3 [Election](https://microsoft.github.io/etcd3/classes/election.html)

The functions encapsulated by the client library also use Lease at the bottom layer, so it is recommended that the ttl option of Election is consistent with the Lease that writes metadata.

The Election mechanism has two roles

     1. Candidate    
      a. Elected leader  
      b. Other non-elected
     2. Observer  
      a. Get the current situation  
      b. Monitor situation changes

### 2. Key space naming convention (iApp)

- 2.1 Fill in the example (without [])  
  - [iApp name] : RTM  
  - [version] : 1.2.3  
  - [sid] : Unique under [iApp name] and [version], the example is as follows:  
    - 2021-06-30T02:48:07.249Z  
    - 1625021287249  
    - 60dbdbb7e49c20984a680913  
    - e494ba80-d94d-11eb-b8bc-0242ac130003  
- 2.2 Key directory is filled by iApps (iApp info)
Delete before iApp main service exits, use Lease first

  - iapps/  
    - [iApp name]/[version]/[sid]/  
      - portal : The URL of the iframe source or href link
      - licenseType : String
      - licenseUsageCount : Number
      - licenseUsageLimit : Number  
      - other...
- 2.3 The Election mechanism operates according to the service
  - prefix : iapps/election
  - name : [iApp name]  
  - value : [version]/[sid]  
  - resign before iApp main service quit
  - Trigger API Hub to reload iApp info  

### 3. Key space naming convention (Services)

- 3.1 Fill in the example (without [])
  - [service name] : ifp-data-hub  
  - [version] : 1.2.3  
  - [sid] : Unique under [service name] and [version], examples are as follows:  
    - 2021-06-30T02:48:07.249Z  
    - 1625021287249  
    - 60dbdbb7e49c20984a680913  
    - e494ba80-d94d-11eb-b8bc-0242ac130003
- 3.2 Key directory is filled by services (service info)

  Delete before the main service exits, use Lease first

  - services/
    - [service name]/[version]/[sid]/  
      - federation : Apollo Federation API URL Location
      - dependentServices : Dependent service names, eg: ["ifp-organizer", "ifp - data-hub"]  
      - other...
- 3.3 The Election mechanism operates according to the service
  - prefix : services/election
  - name : [service name]
  - value : [version]/[sid]  
  - resign before service quit
  - Trigger API Hub to reload service information

### 4 Register Flow

1. API Server is ready and listening on a specific port  
1. Get Lease and put metadatas
1. Election Events

### 5 Service Secrets

- 5.1 How to get
  1. Service Registration Information in Metadata Content
  1. Once the Election campaign is elected, API Hub will distribute the keys needed by the service through the following process
  - 5.1.1 Key directory fill by API Hub with Lease TTL
    - service-secrets/
    - [assignee service name]/[version]/[sid]/  
    - [assignee service name] : secret string
    - [dependent service name] : secret string
    - other [dependent service name] ...
  - 5.1.2 Important note about service secrets
    - API Hub associates these keys with Lease TTL about 30 seconds, it will not be persisted in etcd
    - Services should keep service secrets in memory like API Hub itself, rather than storing to files, databases or other persistent storage.
    - The lifetime of each service secret is within the relationship between the API Hub and the service process instance, i.e. if API Hub or the service restarts a new instance, a new secret will be assigned and the previous secret is out of date.
    - This could be the time delay between the selected service instance and the secret assigned to the specified key, or it could be an existing service instance assigned a new secret (e.g. API Hub restart).
    - Therefore, the Service should set up an etcd Watcher on the desired key, then get the key once as an initial value, and keep it updated when any (new) secrets are assigned.
- 5.2 How to use
  - 5.2.1 Use case 1: Call API Hub's API entry (the merged Graph) to provide these headers in the request (without < and >)

    - X-Ifp-Service-Name: < The name of this service >  
    - X-Ifp-App-Secret: < The key used by this service >  

  API Hub will use these headers to authenticate isIfpApp to gain access to certain APIs that allow isIfpApp.

  - 5.2.2 Use case 2: authenticate API Hub sub-graph request if providing a federation GraphQL API

  API Hub will provide the same headers as above to request the sub-graph service, the service can use these headers to verify that this request is from API Hub, and can trust some other headers it provides.

  - X-Is-Ifp-App:  true or false if isIfpApp is accessed from API Hub.  
  - X-Ifp-User: If accessing a user with an authenticated IFPToken from the API Hub portal, this header value will be JSON and URIComponent encoded user data, decode it, then parse the JSON into user authentication information.

### 6. Metadatas for Desk frontend (ifp-console)

- 6.1 RTM I.App
  - enablerLink  
    - Same as iAppLink  
  - cmdcLink  
    - Same as iAppLink  
  - userLink  
    - Same as iAppLink  
  - enablerFeature
    - Same as iAppFeature
- 6.2 Other I.App
  - iAppLink : JSON Object  
    - url  
      - required  
    - iconUrl  
      - required  
      - size： 72 \* 72
    - iconUrlMap  
      - optional  
      - size： 72 \* 72  
      - fields  
        - disabled  
          - optional  
        - active  
          - optional  
        - hover  
          - optional  
      - titleText
        - required  
      - descriptionText
        - optional  
      - buttonText  
        - optional  
        - linkType=internal: "Configuration"
        - linkType=external: "Open"
        - linkType=iframe: "Configuration"
      - linkType  
        - internal: Internal use of IFP  
        - external: Open new tab  
        - iframe:  Inline to iFactory/Desk using iframe
  - iAppFeature : JSON Object  
    - iconUrl  
      - required  
      - size： 24 \* 24  
    - titleText
      - required  
- 6.3 iApp Link  
![ETCD-v1.1.0-2.005.png](photos/ETCD-v1.1.0-2.005.png)

- 6.4 iApp License Info  
![ETCD-v1.1.0-2.006.png](photos/ETCD-v1.1.0-2.006.png)

### 7. Metadatas for L3 Pivot

- 7.1 L3EHS I.App
  - l3EHSLink : JSON Object
  - deviceType
    - optional
    - EHS(FEMS) use
  - parameterUsage
    - optional
    - EHS(FEMS) use
  - limitSettings
    - optional
    - EHS(FEMS) use
  - timeOfUsePeriod
    - optional
    - EHS(FEMS) use
  - energySplit
    - optional
    - EHS(FEMS) use
  - virtualMeter
    - optional
    - EHS(FEMS) use
  - taskManagement
    - optional
    - Maintenance use
  - metric
    - optional
    - RTM(KPI) use
  - kpiSettings
    - optional
    - EHS(FEMS) use
  - energyUsageInterface
    - optional
    - EHS(FEMS) use
  - energyUsageInterfaceBindingGroup
    - optional
    - EHS(FEMS) use
  - getMoreMetricDashboard
    - optional
    - RTM(KPI) use
  - predictions
    - optional
    - Predict use
  - modelTraining
    - optional
    - Predict use
  - spcChart
    - optional
    - Predict use
  - getMoreAiModel
    - optional
    - Predict use
  - assetSettings
    - optional
    - ???

- 7.1 L3OEEMaintenance I.App
  - l3OEEMLink : JSON Object
    - masterData
      - capacityPlanning
        - optional
    - kpi
      - parameterUsage
        - optional
    - metric
      - optional
    - settings
      - optional
    - getMoreMetricDashboard
      - optional
    - apqp
      - dailyTask
        - optional
    - projectPortfolio
      - optional
    - project
      - optional
    - getMoreTemplate
      - optional
    - predict
      - predictions
        - optional
      - modelTraining
        - optional
      - spcChart
        - optional
      - getMoreAiModel
        - optional
    - dataSettings
      - assetSettings
        - optional

### 8. Metadatas for License Quota (ifp-console)

- 8.1 Single Quota (Default)
  - licenseType
    - String format
    - Text displayed on the screen
  - LicenseUsageCount
    - Number format
    - Quantity used
  - LicenseUsageLimit
    - Number format
    - Available License Quota
- 8.2 Multiple Quotas (Not yet implemented)
  - LicenseTypeKeys
    - English string array
  - license/&lt;licenseTypeKey&gt;/name
    - Text displayed on the screen
  - license/&lt;licenseTypeKey&gt;/count
    - Quantity used
  - license/&lt;licenseTypeKey&gt;/limit
    - Available License Quota
  
### 9. PN inquiry

If the service wants to obtain a license for a PN, the following procedures must be followed:

1. Check whether the key「license/pn/&lt;part numbery&gt;」already exists on Etcd. If so, the value is LicenseQty.

2. If it does not exist, the service needs to write the key「license/pn/&lt;part numbery&gt;」and the value is filled with an empty string.

3. When the fetcher observes that someone has filled in a new PN, and the value is an empty string, it will call the License API and try to obtain the LicenseQty of the PN.

4. If it is successfully obtained, the value will be overwritten as LicenseQty, otherwise, the key will be deleted directly.

The fetcher will call the License API again every 15 minutes to check whether the license status of the pn existing in Etcd has changed, and update or delete it as appropriate.
