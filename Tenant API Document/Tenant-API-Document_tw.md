#

## 版本歷史

| **日期** | **版本** | **作者** | **說明** |
| - | - | - | - |
|**2022-06-08**|1.0.0|Shouche Cheng| 初版 |

## 功能說明

## Tenant API

- [版本歷史](#版本歷史)
- [功能說明](#功能說明)
- [Tenant API](#tenant-api)
- [Field](#field)
- [Tenant Type](#tenant-type)
  - [Root Tenant (Depth 0)](#root-tenant-depth-0)
  - [Tenant (Depth 1)](#tenant-depth-1)
  - [Tenant (Depth 2)](#tenant-depth-2)
- [Stories (Tenant)](#stories-tenant)
  - [Get available tenants of the current user](#get-available-tenants-of-the-current-user)
  - [Select Tenant](#select-tenant)
  - [Get current tenant](#get-current-tenant)
  - [Get Acls of the current user in the current tenant](#get-acls-of-the-current-user-in-the-current-tenant)
- [Stories (Manage Tenant)](#stories-manage-tenant)
  - [Get sub-tenants of the current tenant](#get-sub-tenants-of-the-current-tenant)
  - [Get sub-tenant of the current tenant by tenant ID](#get-sub-tenant-of-the-current-tenant-by-tenant-id)
  - [Create sub-tenant](#create-sub-tenant)
  - [Update sub-tenant](#update-sub-tenant)
  - [Remove sub-tenant](#remove-sub-tenant)
  - [Bind DeviceOn/BI to the current tenant](#bind-deviceonbi-to-the-current-tenant)
- [Stories (Users)](#stories-users)
  - [Get users in the current tenant](#get-users-in-the-current-tenant)
  - [Add non-existent user in the current tenant](#add-non-existent-user-in-the-current-tenant)
  - [Add existing user in the current tenant](#add-existing-user-in-the-current-tenant)
  - [Remove user from the current tenant](#remove-user-from-the-current-tenant)
- [Stories (Group)](#stories-group)
  - [Get groups in the current tenant](#get-groups-in-the-current-tenant)
- [Stories (Internal Services)](#stories-internal-services)
  - [Get Tenant ID from request](#get-tenant-id-from-request)
  - [Call ifp-api-hub by Tenant ID](#call-ifp-api-hub-by-tenant-id)
  - [Get root tenant](#get-root-tenant)
  - [Get all tenants](#get-all-tenants)
  - [Get tenants by Ids](#get-tenants-by-ids)

## Field

| Field               |  Type   | Description                     |
| :------------------ | :-----: | :------------------------------ |
| name\*              | String  | The tenant name.                |
| description         | String  | The tenant description.         |
| depth\*             | Integer | The tenant depth.               |
|                     |         | Auto-generated                  |
|                     |         | Min: 0, Max: 2                  |
| username            | String  | Must be existing user           |
| deviceOnBiRootOrgId | Integer | The Root Org ID of DeviceOn/BI. |
|                     |         | Min: 0                          |

## Tenant Type

```txt
|--------------|
| Root Tenant  |
|--------------|
  |
  |   |-----------|
  |---| Tenant 1  |
  |   |-----------|
  |     |
  |     |   |-------------|
  |     |---| Tenant 1-1  |
  |     |   |-------------|
  |     |
  |     |   |-------------|
  |     |---| Tenant 1-2  |
  |         |-------------|
  |
  |
  |   |-----------|
  |---| Tenant 2  |
      |-----------|
        |
        |   |-------------|
        |---| Tenant 2-1  |
            |-------------|
```

### Root Tenant (Depth 0)

- Only one Root Tenant in the system.
- Auto-generated.
- Tenant Admin can mange sub-tenant (Depth 1).
- No I.App.

### Tenant (Depth 1)

- Tenant Admin can mange sub-tenant (Depth 2).
- Has I.App.

### Tenant (Depth 2)

- Has I.App.

## Stories (Tenant)

### Get available tenants of the current user

```graphql
# Sign-in required.
query {
  me {
    user {
      id
      tenants {
        id
        name
      }
    }
  }
}
```

### Select Tenant

Will set cookie `IFPTenant` and `RootOrgID`(If deviceOnBiRootOrgId exists).

```graphql
# Sign-in required.
mutation {
  setMyTenant(input: { tenantId: "XXX" }) {
    tenant {
      id
      name
    }
  }
}
```

### Get current tenant

Check cookie `IFPTenant`. If it exists, call the following API.

```graphql
# Sign-in required and tenant selected.
query {
  me {
    tenant {
      id
      name
    }
  }
}
```

### Get Acls of the current user in the current tenant

```graphql
# Sign-in required and tenant selected.
query {
  me {
    orgAcls {
      scope
      mode
    }
    dhAcls {
      scope
      mode
    }
    reAcls {
      scope
      mode
    }
  }
}
```

## Stories (Manage Tenant)

### Get sub-tenants of the current tenant

```graphql
# Sign-in required and tenant selected.
query {
  subTenants {
    id
    name
  }
}
```

### Get sub-tenant of the current tenant by tenant ID

```graphql
# Sign-in required and tenant selected.
query {
  tenant(id: "XXX") {
    id
    name
  }
}
```

### Create sub-tenant

If username set, the user will be granted the tenant Admin.

Automatically create Root Org of DeviceOn/BI and set deviceOnBiRootOrgId

```graphql
# Sign-in required and tenant selected.
mutation {
  addTenant(input: { name: "XXX", description: "YYY", username: "a@b.com" }) {
    tenant {
      id
      name
    }
  }
}
```

### Update sub-tenant

If username set, the user will be granted the tenant Admin.

```graphql
# Sign-in required and tenant selected.
mutation {
  updateTenant(
    input: { id: "XXX", name: "XXX", description: "YYY", username: "a@b.com" }
  ) {
    tenant {
      id
      name
    }
  }
}
```

### Remove sub-tenant

```graphql
# Sign-in required and tenant selected.
mutation {
  removeTenant(input: { id: "XXX" }) {
    removedCount
  }
}
```

### Bind DeviceOn/BI to the current tenant

```graphql
# Sign-in required and tenant selected.
mutation {
  bindDeviceOnBiToTenant(input: { id: "XXX", deviceOnBiRootOrgId: 1 }) {
    tenant {
      id
      name
    }
  }
}
```

## Stories (Users)

### Get users in the current tenant

```graphql
# Sign-in required and tenant selected.
query {
  users {
    id
    name
  }
}
```

### Add non-existent user in the current tenant

```graphql
# Sign-in required and tenant selected.
# Unavailable in the EnSaaS.
# Only available in the Edge.
mutation {
  addUser(
    input: {
      name: "XXXX"
      username: "a@b.com"
      password: "XXXX"
      roles: ["Admin"]
      active: true
    }
  ) {
    user {
      id
      username
    }
  }
}
```

### Add existing user in the current tenant

Get user id by username

```graphql
query {
  publicUser(username: "a@b.com") {
    userId
  }
}
```

Use one of the following two

1. `updateUser` mutation

   ```graphql
   # Sign-in required and tenant selected.
   mutation {
     updateUser(input: { id: "XXXX", name: "XXXX", roles: [], active: true }) {
       user {
         id
         username
       }
     }
   }
   ```

2. `setTenantUser` mutation

   ```graphql
   # Sign-in required and tenant selected.
   mutation {
     setTenantUser(
       input: { tenantId: "XXXX", userId: "XXXX", roles: [], active: true }
     ) {
       tenantUser {
         tenantId
         userId
         roles
         active
       }
     }
   }
   ```

### Remove user from the current tenant

roles not set will remove tenant user.

```graphql
# Sign-in required and tenant selected.
mutation {
  setTenantUser(input: { tenantId: "XXXX", userId: "XXXX", active: true }) {
    tenantUser {
      tenantId
      userId
      roles
      active
    }
  }
}
```

## Stories (Group)

### Get groups in the current tenant

```graphql
# Sign-in required and tenant selected.
query {
  groups {
    id
    name
  }
}
```

## Stories (Internal Services)

### Get Tenant ID from request

1. Check HTTP Header: `X-Ifp-Tenant-Id`
2. Check Cookies: `IFPTenant`

### Call ifp-api-hub by Tenant ID

- HTTP Header: `X-Ifp-Tenant-Id`
- HTTP Header: `X-Ifp-Service-Name` (Required)
  - from iApp Service Register
- HTTP Header: `X-Ifp-App-Secret` (Required)
  - from iApp Service Register

### Get root tenant

```graphql
query {
  rootTenant {
    id
    name
  }
}
```

### Get all tenants

```graphql
query {
  allTenants {
    id
    name
  }
}
```

### Get tenants by Ids

```graphql
query {
  tenantsByIds(ids: ["XXX", "YYY"]) {
    id
    name
  }
}
```
